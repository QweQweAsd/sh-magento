<?php

$log_array = array();
require_once '../app/Mage.php';
umask(0);
Mage::app('default');
include 'web_conf.php';

$write = Mage::getSingleton('core/resource')->getConnection('core_write');

$InventoryStatus = $write->fetchAll("SELECT status FROM import_notification_history 
    WHERE import_type='inventoryFeed' or import_type='price' or import_type='catalogOffers' or import_type='productDescription' ");

if ($InventoryStatus[0]['status'] != "inProgress" &&
        $InventoryStatus[1]['status'] != "inProgress" &&
        $InventoryStatus[2]['status'] != "inProgress" && $InventoryStatus[3]['status'] != "inProgress") {

//Mail to be sent for
    //$arrTo[] = array("toEmail" => "amitblues123@gmail.com", "toName" => "imsupport");
    $arrTo[] = array("toEmail" => "mohansaip@kensium.com", "toName" => "Janesh");

//This URL IS USED FOR BATCH PROCESSING
    $CronURL = '../dailyfeed/InventoryUpdateCron.php';

//Semaphore Flag
    $txtFlagFile = '/home/springhill/UploadComplete.txt';

//Directory path where import files are located
    $dir_import = "/home/springhill/";

//Directory Path where Import files to be moved after successful import
    $dir_Archive = "../dailyfeed/archive/";

    $timestamp = date("YmdHis") . "_";

    $txtArchiveInventory = $dir_Archive . $timestamp . "Inventory.txt";


//Directory path for Inventory.txt file
    $txtInventoryFile = $dir_import . "Inventory.txt";

    $write = Mage::getSingleton('core/resource')->getConnection('core_write');

    $InventoryId = $write->fetchOne("SELECT id FROM import_notification_history WHERE import_type='inventory' and status='notStarted'");


// && empty($InventoryId)
//Checking flag file and then proceeding for Inventory Update
    if (file_exists($txtFlagFile) && file_exists($txtInventoryFile) && ((!isset($argv[1]) && !empty($InventoryId)) || (isset($argv[1]) && empty($InventoryId)))) {

        //Saving the lastupdated time in database for 36 hours email notification
        $InventoryDataId = $write->fetchOne("SELECT id FROM import_notification_history WHERE import_type='inventory'");
        if (empty($InventoryDataId)) {
            $write->query("INSERT INTO import_notification_history(import_type,status) VALUES('inventory','inProgress')");
        } else {
            $write->query("UPDATE import_notification_history SET status='inProgress' WHERE id=" . $InventoryDataId);
        }


        //Get the limit to be imported by default 100
        $start = 0;
        $limit = 2600;
        //Get the Start Index for Import
        if (!empty($argv[1]))
            $start = $argv[1];
        else
            $start = 0;


        /*
         * Update Inventory values with Product info
         */
        $connection = Mage::getSingleton('core/resource')
                ->getConnection('core_read');

        $totalResults = $connection->select()
                ->from('Inventory as i', array('count(*) as count'))
                ->join("catalog_product_entity AS cpe", "cpe.sku = i.Item_Num", array());
        $totalResultsCount = $connection->fetchAll($totalResults);

        if (file_exists($txtInventoryFile) && $totalResultsCount[0]['count'] == 0) {
            try {
                $write->query("LOAD DATA LOCAL INFILE '" . $txtInventoryFile . "' INTO TABLE Inventory
FIELDS TERMINATED BY '|' ENCLOSED BY '' 
LINES TERMINATED BY '\n' STARTING BY ''");
                $write->query("DELETE FROM Inventory where Item_Num='Item_Num'");
                $write->query("UPDATE import_notification_history SET start_date=NOW() WHERE id=" . $InventoryDataId);
            } catch (Exception $e) {
                $log_array[0]['Message'] = $e->getMessage();
                $log_array[0]['Trace'] = $e->getTraceAsString();
                $log_array[0]['Info'] = "Something went wrong while importing from Inventory.txt file.";
            }
        }

        $totalResults1 = $connection->select()
                ->from('Inventory as i', array('count(*) as count'))
                ->join("catalog_product_entity AS cpe", "cpe.sku = i.Item_Num", array());
        $totalResultsCount1 = $connection->fetchAll($totalResults1);
//print_r($totalResultsCount1); exit;
        if (count($totalResultsCount1) > 0 && $start < $totalResultsCount1[0]['count']) {

            try {
                $inSelect = "";
                if ($start == 0) {
                    $inSelect = $connection->select()
                            ->from('Inventory as i', array('i.Item_Num', 'i.Unit_Of_Measurement', 'i.Restricted_States', 'i.Botanical'))
                            ->join("catalog_product_entity AS cpe", "cpe.sku = i.Item_Num", array())
                            ->order("i.Item_Num")
                            ->limit($limit);
                } else {
                    $inSelect = $connection->select()
                            ->from('Inventory as i', array('i.Item_Num', 'i.Unit_Of_Measurement', 'i.Restricted_States', 'i.Botanical'))
                            ->join("catalog_product_entity AS cpe", "cpe.sku = i.Item_Num", array())
                            ->order("i.Item_Num")
                            ->limit($start, $limit);
                }
                $inRowsArray = $connection->fetchAll($inSelect);
            } catch (Exception $e) {
                $log_array[1]['Message'] = $e->getMessage();
                $log_array[1]['Trace'] = $e->getTraceAsString();
                $log_array[1]['Info'] = "Something went wrong while importing from Inventory table.";
            }
            $i = 1;

            $error_inc = 2;
            $inc_val = 0;
            $res_cnt = count($inRowsArray);

            foreach ($inRowsArray as $key => $value) {
                $sku = $value['Item_Num'];
                $_Pdetails = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);
                if (!empty($_Pdetails['sku'])) {
                    $product_id = $_Pdetails->getId();
                    try {
                        $prod_details = Mage::getModel('catalog/product');
                        $prod_details->load($product_id);
                        if ($prod_details->getUnitOfMeasure() != $value['Unit_Of_Measurement'])
                            $prod_details->setUnitOfMeasure($value['Unit_Of_Measurement']);
                        if ($prod_details->getRestrictedStates() != $value['Restricted_States'])
                            $prod_details->setRestrictedStates($value['Restricted_States']);
                        if ($prod_details->getBotanicalName() != addslashes($value['Botanical']))
                            $prod_details->setBotanicalName(addslashes($value['Botanical']));

                        $prod_details->save();
                    } catch (Exception $e) {
                        $log_array[$error_inc]['Message'] = $e->getMessage();
                        $log_array[$error_inc]['Trace'] = $e->getTraceAsString();
                        $log_array[$error_inc]['sku'] = $sku;
                        $log_array[$error_inc]['Info'] = "Something went wrong while updating product information.";
                    }
                }
                print $inc_val . "===>" . $sku . "\n";
                $i++;
                $error_inc++;
                $inc_val++;
            }

            $start = $start + $limit;
            exec('php InventoryUpdateCron.php ' . $start . ' ' . $limit);
        } else {
            $error_inc1 = $res_cnt + 2;
            try {
                if (copy($txtInventoryFile, $txtArchiveInventory)) {
                    unlink($txtInventoryFile);
                }
            } catch (Exception $e) {
                $log_array[$error_inc1]['Message'] = $e->getMessage();
                $log_array[$error_inc1]['Trace'] = $e->getTraceAsString();
                $log_array[$error_inc1]['Info'] = "Something went wrong while archive the Inventory file.";
            }

            try {
                if ((count(scandir($dir_import)) == 3)) {
                    unlink($txtFlagFile);
                }
            } catch (Exception $e) {
                $log_array[$error_inc1 + 1]['Message'] = $e->getMessage();
                $log_array[$error_inc1 + 1]['Trace'] = $e->getTraceAsString();
                $log_array[$error_inc1 + 1]['Info'] = "Something went wrong while removing flag file.";
            }
            try {
                $write = Mage::getSingleton('core/resource')->getConnection('core_write');
                $write->delete("Inventory");

                $write->query("UPDATE import_notification_history SET status='notStarted', end_date=NOW() WHERE import_type='inventory' and status='inProgress'");
            } catch (Exception $e) {
                $log_array[$error_inc1 + 2]['Message'] = $e->getMessage();
                $log_array[$error_inc1 + 2]['Trace'] = $e->getTraceAsString();
                $log_array[$error_inc1 + 2]['Info'] = "Something went wrong while deleting the records from Inventory table.";
            }
        }
        //DELETE THE ARCHIVE FILES OLDER THAN 30 Days
        if ($handle = opendir($dir_Archive)) {
            while (false !== ($entry = readdir($handle))) {
                if ($entry != "." && $entry != "..") {
                    $arrFiles = explode("_", $entry);

                    $fileTimeStamp = $arrFiles[0];
                    $date1 = date('YmdHis');
                    $date2 = $fileTimeStamp;

                    $ts1 = strtotime($date1);
                    $ts2 = strtotime($date2);
                    $seconds_diff = $ts1 - $ts2;
                    $days = floor($seconds_diff / 3600 / 24);
                    try {
                        //Delete the files older than 30 days
                        if ($days > 30) {
                            unlink($dir_Archive . $entry);
                        }
                    } catch (Exception $e) {
                        $log_array[$error_inc1 + 3]['Message'] = $e->getMessage();
                        $log_array[$error_inc1 + 3]['Trace'] = $e->getTraceAsString();
                        $log_array[$error_inc1 + 3]['Info'] = "Something went wrong while deleting 30 days old archive files.";
                    }
                }
            }
            closedir($handle);
        }
        //ERROR LOG
        if (count($log_array) > 0) {
            $errorMsg = '';
            foreach ($log_array as $log) {
                $errorMsg .= '
					- - - - - - - -
					ERROR
                                        SUBJECT: ' . $log['Info'] . '
					Message: ' . $log['Message'] . '
					Trace: ' . $log['Trace'] . '
					ProductId: ' . $log['sku'] . '
					Time: ' . date('Y m d H:i:s') . '
					';
                echo "\r\n" . $errorMsg;
            }
            file_put_contents($dir_Archive . '/logs/log_' . time() . '.txt', $errorMsg);
            //Getting admin Information
            $adminInfo = $write->fetchAll("SELECT * from admin_user");
            $userFirstname = $adminInfo[0]['firstname'];
            $userLastname = $adminInfo[0]['lastname'];
            $userEmail = $adminInfo[0]['email'];
            $arrTo[] = array("toEmail" => $userEmail, "toName" => $userFirstname . ' ' . $userLastname);

            $mail = new Zend_Mail();
            $mail->setType(Zend_Mime::MULTIPART_RELATED);
            $mail->setBodyHtml('There were errors updating the product database for Spring Hill. Please review the log files at: ' . $_SERVER['REMOTE_ADDR'] . '/dailyfeed/archive/logs/log_' . time() . '.txt');
            $mail->setFrom($userEmail, $userFirstname . ' ' . $userLastname);
            //SENDING MAIL TO Janesh,imsupport@gardensalive.com and Magento Admin
            foreach ($arrTo as $to) {
                $mail->addTo($to['toEmail'], $to['toName']);
            }
            $mail->setSubject('Error updating SH product database');
            $fileContents = file_get_contents($dir_Archive . '/logs/log_' . time() . '.txt');
            $file = $mail->createAttachment($fileContents);
            $file->filename = "log_" . time() . ".txt";
          /*  try {
                if ($mail->send()) {
                    Mage::getSingleton('core/session')->addSuccess('Log Information has been mailed');
                }
            } catch (Exception $ex) {
                Mage::getSingleton('core/session')->addSuccess('Unable to send mail');
            }*/
        }
    }
} else {
    
}
?>
