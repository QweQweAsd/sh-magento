<?php require_once('../app/Mage.php'); //Path to Magento
umask(0);
Mage::app();

//include 'web_conf.php';

$handle = fopen("ProductDescription.csv", "r");
$count = 0;
$ii=0;
$catArray=array();
//echo "<pre>";
$write = Mage::getSingleton('core/resource')->getConnection('core_write');
while (($data = fgetcsv($handle, 100000, "|")) !== FALSE) {
	//print_r($data);
	if($ii==1250){
		break;
	}
	if($ii<=1200 || $data['1']==''){
		$ii++;
		continue;
	}
	$image ='';
	$product_skus = explode("," , $data['1']);
	foreach($product_skus as $sku){

		echo $sku;
		// get attribute set
		
		

		/// General Mandatory Attribute

		/// To import the SPRG_Product_Categories.csv to get category name on the basis of product_number and cat_nbr		

		$cathandle = fopen("SPRG_Product_Categories.csv", "r");
		$aa=0;
		$cat_nbr='';
		while (($catdata = fgetcsv($cathandle, 100000, "|")) !== FALSE) {
			
			if($aa==0 || $catdata['0']==''){
				$aa++;
				continue;
			}
			if($catdata[0]==$sku){
				//print_r($catdata);
				//echo $catdata[1];
				$cat_nbr[]= $catdata[1];		
			}
			$aa++;

		}

		if(count($cat_nbr) > 0){
			$catArray='';
			foreach($cat_nbr as $cat_value){
				/// To import the SPRG_Categories.csv to get category name on the basis of product_number		
				
				$catnamehandle = fopen("SPRG_Categories.csv", "r");
				$p=0;
				while (($catnamedata = fgetcsv($catnamehandle, 100000, "|")) !== FALSE) {
					
					if($p==0 || $catnamedata['0']==''){
						$p++;
						continue;
					}
					
					if($catnamedata[0]==$cat_value){
						//print_r($catnamedata);
						//echo $cat_value;
						$cat_name = addslashes($catnamedata[1]);	
						echo $cat_name."#####";	
						$cat_ids=$write->fetchOne("SELECT entity_id FROM catalog_category_entity_varchar where attribute_id = '41' and value='".$cat_name."'");	
						$catArray[]  = $cat_ids;
					}
					$p++;

				}
			}

		}
		$CategoryIDs[] =implode(",",$catArray);
		$prodsku[] = $sku;
		$title[] = $cat_name.' Plant'; //If there is no title, please provide the format to be used for default.  E.g. <Product Name> - <category name> plant
		$image ="http://springhillnursery.com/images/250/$sku.jpg";
		
		
		if($sku)
		{
			if(!file_exists("/var/www/springhill/media/import/".$sku.".jpg")){
				@copy( $image  , "/var/www/springhill/media/import/".$sku.".jpg" );
				if(!file_exists($image)){
					$img_val[] ='/noimage.gif';
				}else{
					$img_val[] ='/'.$sku.'.jpg';
				}
			}else{
				$img_val[] ='/'.$sku.'.jpg';
			}
		
		}
	
		$bestsellerindicator[] = 0;
		$lifetimeguarantee[] = 0;
		$webonlyplant[] = 0;
		$isnewplant[] = 0;
		$isonclearance[] = 0;
		
		$store[]="admin";
		$websites[]="base";
		$attribute_set[]="Springhills";
		$type[]="simple";
		$has_options[] = 0;
		$url_key[] = '';
		$url_path[] = '';
		$custom_design[] = '';
		$page_layout[]  = 'No Layout updates';
		$options_container[] = 'Block after info colomn';
		$country_of_manufacture[] = '';
		$msrp_enabled[]   = 'Use config';
		$msrp_display_actual_price_type[] = 'Use config';
		$gift_message_available[]  = 'No';
		$gift_wrapping_available[] = 'No';
		$is_returnable[] = 'Use config';

		$custom_layout_update[]  = '';
		$is_recurring[] = 'No';
		$visibility[]  = 'Catalog, Search';
		$enable_googlecheckout[] = 'Yes';
		$tax_class_id[]  = 'Taxable Goods';     
		$special_from_date[] = '0000-00-00 00:00:00';
		$special_to_date[] = '0000-00-00 00:00:00';
		$news_from_date[]  = '0000-00-00 00:00:00';
		$news_to_date[]  = '0000-00-00 00:00:00';
		$custom_design_from[]  = '';
		$custom_design_to[]    = '';

		$use_config_min_qty[]  ='1';
		$is_qty_decimal[]  = '0';
		
		$store_id[]   = '0';
		$product_type_id[]   = 'simple';
		$product_status_changed[]  = '';
		$product_changed_websites[]  = '';
		$is_in_stock[]  = '1';
		$low_stock_date[]  = '';
		$notify_stock_qty[]  = '';
		$use_config_notify_stock_qty[]  = '1';
		$manage_stock[]  = '0';
		$use_config_manage_stock[] = '0';
		$stock_status_changed_auto[]  = '0';
		$use_config_qty_increments[]  = '1';
		$qty_increments[]  = '0';
		$use_config_enable_qty_inc[]  = '1';
		$enable_qty_increments[]  = '0';
		$stock_status_changed_automatically[]   = '0';
		$use_config_enable_qty_increments[]  = '1';

		$AdditionalSearchWords[] = '' ;
		$LongDescription[] = addslashes($data[59]);
		$MetaKeywords[] = '';
		$MetaDescription[] = '';
		$ShortDescription[] = addslashes($data[59]);
		$weight[] = '1.0';
		$RecommendationURL[] = '';


		/// To import the data from Inv.csv haing same item_num in productdescription.csv Attribute		

		$invhandle = fopen("Inv.csv", "r");
		$jj=0;
		while (($invdata = fgetcsv($invhandle, 100000, "|")) !== FALSE) {

			if($jj==0 || $invdata['0']==''){
				$jj++;
				continue;
			}
			if($invdata[0]==$sku){
				$botanicalname[] = $invdata[1];		
				$RestrictedStates[] = $invdata[3];
			}
			$jj++;

		}
		

		/// To import the data from inventory.csv haing same product_number in productdescription.csv Attribute		

		$inventoryhandle = fopen("Inventory.csv", "r");
		$kk=0;
		while (($inventorydata = fgetcsv($inventoryhandle, 100000, "|")) !== FALSE) {

			if($kk==0 || $inventorydata['0']==''){
				$kk++;
				continue;
			}
			if($inventorydata[0]==$sku){
				$qty[] = $inventorydata[4];		
				$BackOrderDate[] = $inventorydata[11];
				
				$Season[] = $inventorydata[7];
				
				if($inventorydata[11]!= ""){
					$backorders[]  = '1' ;
					$use_config_backorders[] = '0';
				}else{
					$backorders[]  = '0' ;
					$use_config_backorders[] = '1';
				}

				if($inventorydata[3]=='Y'){
					$DiscontinuedObsolete[] ='1';
				}else{
					$DiscontinuedObsolete[] = '0';
				}

				if($inventorydata[9]=='Y'){
					$FutureSeasonIndicator ='1';
				}else{
					$FutureSeasonIndicator = '2';
				}

				if($inventorydata[10]=='y'){
					$FutureSeasonMessage[] = '1';
				}else{
					$FutureSeasonMessage[] = '0';
				}

				if($inventorydata[2]=='Y'){
					$status[] = 'Enabled';
				}else{
					$status[] = 'Disabled';
				}
			}
			$kk++;
		}
		
		/// To import the data from price.csv haing same product_number and offer_code=4 in productdescription.csv Attribute		

		$pricehandle = fopen("Price.csv", "r");
		$ll=0;
		$ProductOffersCode='';
		while (($pricedata = fgetcsv($pricehandle, 100000, "|")) !== FALSE) {

			if($ll==0 || $pricedata['0']==''){
				$ll++;
				continue;
			}
			if($pricedata[0]==$sku && $pricedata[1]!=4 && $pricedata[1]!=''){				
				$ProductOffersCode .= $pricedata[1].",";
			}
			if($pricedata[0]==$sku && $pricedata[1]==4){
				
				$CatalogCode[] = '';		
				$price[] = $pricedata[4];
				$MaxQuantity[] = $pricedata[3];		
				$salePrice[] = $pricedata[6];
				$wholesalePrice[] = '';

				if($pricedata[2]!=''){
					$min_sale_qty[] = $pricedata[2];
					$use_config_min_sale_qty[]  = '0';
				}else{
					$min_sale_qty[] = '1';
					$use_config_min_sale_qty[]  = '1';
				}
				if($pricedata[3]!=''){
					$max_sale_qty[] = $pricedata[3];
					$use_config_max_sale_qty[]  = '0';
				}else{
					$max_sale_qty[] = '1';
					$use_config_max_sale_qty[]  = '1';
				}

			}else{
				$min_sale_qty[] = '1';
				$use_config_min_sale_qty[]  = '1';
				$max_sale_qty[]   = '0';
				$use_config_max_sale_qty[]  = '1';
			}
			$ll++;

		}
		if($ProductOffers!=''){
			$ProductOffers[]=substr($ProductOffersCode,0,-1);
		}
		// Product Description Attribute
		$Usage[] = '';
		$Light[] = '' ;		
		$AdditionalInformation[] = addslashes($data[68]);		
		$BloomTime[] ='';
		$UnitofMeasure[] = 'Each';
		$UniqueCharacteristics[] = addslashes($data[59]);
		$SunExposure[] = '' ;
		$Spread[] = addslashes($data[35]);
		$Spacing[] = addslashes($data[36]);
		$ShippingSchedule[] ='';
		$SEOCopyBlock[] = '';
		$GrowthRate[] =  addslashes($data[58]);
		$Form[] = addslashes($data[64]);
		$FloweringDate[] = addslashes($data[46]);
		$FlowerColor[] =  addslashes($data[41]);
		$FoliageType[] = addslashes($data[37]);
		$DisplayHardinessZone[] = addslashes($data[51]);	
		$RelatedVideoURL[] ='';
		$HardinessZones[]	= 	addslashes($data[51]);
		$foliage[] = addslashes($data[24]);
		$HeightHabit[] = addslashes($data[34]); 
		$breck_name[] = addslashes($data[8]);
		$common_name[] = addslashes($data[9]);
		$family[] = addslashes($data[11]);
		if($data[6]=='TRUE'){
			$PlantingRequirements[] = '1';
		}else{
			$PlantingRequirements[] = '0';
		}		
		if($data[12]=='TRUE'){
			$shade[] = '1';
		}else{
			$shade[] = '0';
		}
		if($data[13]=='TRUE'){
			$partial_shade[] = '1';
		}else{
			$partial_shade[] = '0';
		}
		if($data[14]=='TRUE'){
			$full_sun[] = '1';
		}else{
			$full_sun[] = '0';
		}
		if($data[15]=='TRUE'){
			$borders[] = '1';
		}else{
			$borders[] = '0';
		}
		if($data[17]=='TRUE'){
			$rock_gardens[] = '1';
		}else{
			$rock_gardens[] = '0';
		}
		if($data[18]=='TRUE'){
			$ground_covers[] = '1';
		}else{
			$ground_covers[] = '0';
		}
		if($data[21]=='TRUE'){
			$longlastingingarden[] = '1';
		}else{
			$longlastingingarden[] = '0';
		}
		if($data[23]=='TRUE'){
			$cut_flower[] = '1';
		}else{
			$cut_flower[] = '0';
		}
		if($data[25]=='TRUE'){
			$long_lasting_cut[] = '1';
		}else{
			$long_lasting_cut[] = '0';
		}
		if($data[26]=='TRUE'){
			$goodforforcing[] = '1';
		}else{
			$goodforforcing[] = '0';
		}	
		if($data[27]=='TRUE'){
			$goodfornaturalizing[] = '1';
		}else{
			$goodfornaturalizing[] = '0';
		}	
		if($data[28]=='TRUE'){
			$multipliesannually[] = '1';
		}else{
			$multipliesannually[] = '0';
		}		
		if($data[30]=='TRUE'){
			$DRIEDFLOWERS[] = '1';
		}else{
			$DRIEDFLOWERS[] = '0';
		}
		if($data[31]=='TRUE'){
			$CONTAINER[] = '1';
		}else{
			$CONTAINER[] = '0';
		}
		
		$GoodForOther[] = addslashes($data[32]);
		$WebHeightSearch[] = addslashes($data[33]);
		$FlowerForm[] = addslashes($data[39]);	
		$AverageNumberofBlooms[] = addslashes($data[40]);
		$WebSearchFlowerColor[] = addslashes($data[42]);
		$Specialcareorplantinginstructions[] = addslashes($data[54]);
		$FRAGRANCE[] = addslashes($data[43]);
		$PlantingTime[] = addslashes($data[44]);
		$WebFloweringTimeSearch[] = addslashes($data[45]);
		$DURATION[] = addslashes($data[47]);
		$BLOOMSWITH[] =addslashes( $data[49]);
		$WebSearchZone[] = addslashes($data[50]);
		$TYPEOFFRUIT[] = addslashes($data[52]);
		$Shipped[] = addslashes($data[53]);
		$SOILREQUIREMENTS[] =addslashes( $data[56]);
		$ROOTSYSTEM[] = addslashes($data[57]);
		$twouniqueCharacteristics[] = addslashes($data[60]);
		$Keydifferencefromsimilarbulbs[] = addslashes($data[62]);
		$RESISTANCE[] = addslashes($data[65]);
		$PRUNING[] = addslashes($data[66]);
		$TIMEOFPRUNING[] = addslashes($data[67]);
		$Awards[] = addslashes($data[70]);

		if($data[76]=='TRUE'){
			$Indoorsonly[] = '1';
		}else{
			$Indoorsonly[] = '0';
		}

		if($data[77]=='TRUE'){
			$IndoorsOutdoorsinwarmtemperatures[] = '1';
		}else{
			$IndoorsOutdoorsinwarmtemperatures[] = '0';
		}
	
		if($data[78]=='TRUE'){
			$Canbepermantlyplantedingarden[] = '1';
		}else{
			$Canbepermantlyplantedingarden[] = '0';
		}

		$WINTERREQUIREMENTS[] = addslashes($data[79]);
		$TemperatureRequirements[] = addslashes($data[80]);
		$LightNeededIndoors[] = addslashes($data[81]);
		$WateringRequirements[] = addslashes($data[82]);
		$FertilizationRequirements[] = addslashes($data[83]);
		$SpecialCare[] = addslashes($data[84]);

		if($data[87]=='TRUE'){
			$DeerResistant[] = '1';
		}else{
			$DeerResistant[] = '0';
		}

		if($data[88]=='TRUE'){
			$SeasonShippedFall[] = '1';
		}else{
			$SeasonShippedFall[] = '0';
		}

		if($data[89]=='TRUE'){
			$SeasonShippedSpring[] = '1';
		}else{
			$SeasonShippedSpring[] = '0';
		}
		$count++;
	}

	$ii++;
	
}


$_csv_data ='"store"| "websites"|"attribute_set"|"type"|"category_ids"|"sku"|"has_options"|"name"|"meta_title"|"meta_description"| "image"| "small_image"| "thumbnail"| "url_key"| "url_path"| "custom_design"| "page_layout"| "options_container"| "country_of_manufacture"| "msrp_enabled"| "msrp_display_actual_price_type"| "gift_message_available"| "gift_wrapping_available"| "is_returnable"| "useritemlastchanged"| "price"| "special_price"| "weight"| "msrp"| "gift_wrapping_price"| "status"| "is_recurring"|"visibility"| "enable_googlecheckout"| "tax_class_id"| "special_from_date"| "special_to_date"| "news_from_date"| "news_to_date"|"custom_design_from"| "custom_design_to|description"| "short_description"| "meta_keyword"| "custom_layout_update"| "qty"| "min_qty"| "use_config_min_qty"| "is_qty_decimal"| "backorders"| "use_config_backorders"| "min_sale_qty"| "use_config_min_sale_qty"| "max_sale_qty"| "use_config_max_sale_qty"| "is_in_stock"| "low_stock_date"| "notify_stock_qty"|"use_config_notify_stock_qty"| "manage_stock"| "use_config_manage_stock"| "stock_status_changed_auto"| "use_config_qty_increments|qty_increments"| "use_config_enable_qty_inc"| "enable_qty_increments"| "stock_status_changed_automatically"| "use_config_enable_qty_increments"| "product_name"| "store_id"| "product_type_id"| "product_status_changed"| "product_changed_websites "| "additional_information"| "average_number_blooms"| "awards"| "best_seller_indicator"| "blooms_width|bloom_time"| "borders"| "botanical_name"| "breck_name"| "catalog_code"| "common_name"|"container"| "cut_flowers"| "deer_resistant"| "display_hardiness_zone"| "dried_flowers"| "duration"| "family"| "fertilization_requirements"| "flowering_date"| "flower_color|flower_form"| "foliage"| "foliage_type"| "form"| "fragrance"| "full_sun"| "future_season_indicator"| "future_season_message"| "good_for_forcing"| "good_for_naturalizing"| "good_for_other"| "ground_covers"| "growth_rate"| "hardiness_zones"| "height_habit"| "indoors_only"| "io_in_warm_temp"| "is_web_only_plant"| "lifetime_guarantee"| "is_new_plant"| "light"| "light_needed_indoors"| "long_lasting_cut"| "long_lasting_in_garden"| "multiplies_annually "| "partial_shade"| "permantly_planted_garden"| "planting_requirements"| "planting_time"| "product_offers_code"| "pruning"| "related_video_url"| "resistance"| "restricted_states"| "rock_gardens"| "root_system"| "search_words"| "season"| "season_shipped_fall"| "season_shipped_spring"| "seo_copy_block"| "shade"| "shipped"| "shipping_schedule"| "soil_requirements"| "spacing"| "special_care"| "special_care_instructions "| "spread"| "sun_exposure"| "temperature_requirements"| "time_of_pruning"|"two_unique_characteristic"| "type_of_fruit"| "unique_characteristics"| "unit_of_measure"| "usage"| "watering_requirements"| "web_flowering_time_search"| "web_height_search"| "web_search_flower_color"| "web_search_zone"| "winter_requirement"'. "\n";



//for($i=0;$i<$count;$i++)  
//$count1 = 15;
//$attributeSets = $client->call($session_id, 'product_attribute_set.list');
//$attributeSet = $attributeSets[1];//


for($i=0;$i<$count;$i++)  {
if($price[$i]==0 || $price[$i]=="" ){
	$price[$i]='0';
}

if($status[$i]=="" ){
	$status[$i]='Disabled';
}
$weight[$i]='1.0';

if($CategoryIDs[$i]==''){
	$CategoryIDs[$i]='0';
}

$_csv_data.= '"'.$store[$i].'"| "'.$websites[$i].'"| "'.$attribute_set[$i].'"|"'.$type[$i].'"|"'.$CategoryIDs[$i].'"|"'.$prodsku[$i].'"|"'.$has_options[$i].'"|"'.$title[$i].'"|"'.$meta_title[$i].'"|"'.$meta_description[$i].'"|"'.$img_val[$i].'"|"'.$img_val[$i].'"|"'.$img_val[$i].'"|"'.$url_key[$i].'"|"'.$url_path[$i].'"|"'.$custom_design[$i].'"|"'.$page_layout[$i].'"|"'.$options_container[$i].'"|"'.$country_of_manufacture[$i].'"|"'.$msrp_enabled[$i].'"|"'.$msrp_display_actual_price_type[$i].'"|"'.$gift_message_available[$i].'"|"'.$gift_wrapping_available[$i].'"|"'.$is_returnable[$i].'"|"'.$useritemlastchanged[$i].'"|"'.$price[$i].'"|"'.$special_price[$i].'"|"'.$weight[$i].'"|"'.$msrp[$i].'"|"'.$gift_wrapping_price[$i].'"|"'.$status[$i].'"|"'.$is_recurring[$i].'"|"'.$visibility[$i].'"|"'.$enable_googlecheckout[$i].'"|"'.$tax_class_id[$i].'"|"'.$special_from_date[$i].'"|"'.$special_to_date[$i].'"|"'.$news_from_date[$i].'"|"'.$news_to_date[$i].'"|"'.$custom_design_from[$i].'"|"'.$custom_design_to[$i].'"|"'.$ShortDescription[$i].'"|"'.$ShortDescription[$i].'"|"'.$meta_keyword[$i].'"|"'.$custom_layout_update[$i].'"|"'.$qty[$i].'"|"'.$min_qty[$i].'"|"'.$use_config_min_qty[$i].'"|"'.$is_qty_decimal[$i].'"|"'.$backorders[$i].'"|"'.$use_config_backorders[$i].'"|"'.$min_sale_qty[$i].'"|"'.$use_config_min_sale_qty[$i].'"|"'.$max_sale_qty[$i].'"|"'.$use_config_max_sale_qty[$i].'"|"'.$is_in_stock[$i].'"|"'.$low_stock_date[$i].'"|"'.$notify_stock_qty[$i].'"|"'.$use_config_notify_stock_qty[$i].'"|"'.$manage_stock[$i].'"|"'.$use_config_manage_stock[$i].'"|"'.$stock_status_changed_auto[$i].'"|"'.$use_config_qty_increments[$i].'"|"'.$qty_increments[$i].'"|"'.$use_config_enable_qty_inc[$i].'"|"'.$enable_qty_increments[$i].'"|"'.$stock_status_changed_automatically[$i].'"|"'.$use_config_enable_qty_increments[$i].'"|"'.$product_name[$i].'"|"'.$store_id[$i].'"|"'.$product_type_id[$i].'"|"'.$product_status_changed[$i].'"|"'.$product_changed_websites[$i].'"|"'.$AdditionalInformation[$i].'"|"'.$AverageNumberofBlooms[$i].'"|"'.$Awards[$i].'"|"'.$bestsellerindicator[$i].'"|"'.$BLOOMSWITH[$i].'"|"'.$BloomTime[$i].'"|"'.$borders[$i].'"|"'.$botanicalname[$i].'"|"'.$breck_name[$i].'"|"'.$catalog_code[$i].'"|"'.$common_name[$i].'"|"'.$CONTAINER[$i].'"|"'.$cut_flower[$i].'"|"'.$DeerResistant[$i].'"|"'.$DisplayHardinessZone[$i].'"|"'.$DRIEDFLOWERS[$i].'"|"'.$DURATION[$i].'"|"'.$family[$i].'"|"'.$FertilizationRequirements[$i].'"|"'.$FloweringDate[$i].'"|"'.$FlowerColor[$i].'"|"'.$FlowerForm[$i].'"|"'.$foliage[$i].'"|"'.$FoliageType[$i].'"|"'.$Form[$i].'"|"'.$FRAGRANCE[$i].'"|"'.$full_sun[$i].'"|"'.$FutureSeasonIndicator[$i].'"|"'.$FutureSeasonMessage[$i].'"|"'.$goodforforcing[$i].'"|"'.$goodfornaturalizing[$i].'"|"'.$GoodForOther[$i].'"|"'.$ground_covers[$i].'"|"'.$GrowthRate[$i].'"|"'.$HardinessZones[$i].'"|"'.$HeightHabit[$i].'"|"'.$Indoorsonly[$i].'"|"'.$IndoorsOutdoorsinwarmtemperatures[$i].'"|"0"|"0"|"0"|"'.$Light[$i].'"|"'.$LightNeededIndoors[$i].'"|"'.$long_lasting_cut[$i].'"|"'.$longlastingingarden[$i].'"|"'.$multipliesannually[$i].'"|"'.$partial_shade[$i].'"|"'.$Canbepermantlyplantedingarden[$i].'"|"'.$PlantingRequirements[$i].'"|"'.$PlantingTime[$i].'"|"'.$ProductOffers[$i].'"|"'.$PRUNING[$i].' "|"'.$related_video_url[$i].'"|"'.$RESISTANCE[$i].'"|"'.$RestrictedStates[$i].'"|"'.$rock_gardens[$i].'"|"'.$ROOTSYSTEM[$i].'"|"'.$AdditionalSearchWords[$i].'"|"'.$Season[$i].'"|"'.$SeasonShippedFall[$i].'"|"'.$SeasonShippedSpring[$i].'"|"'.$SEOCopyBlock[$i].'"|"'.$shade[$i].'"|"'.$Shipped[$i].'"|"'.$ShippingSchedule[$i].'"|"'.$SOILREQUIREMENTS[$i].'"|"'.$Spacing[$i].'"|"'.$SpecialCare[$i].'"|"'.$Specialcareorplantinginstructions[$i].'"|"'.$Spread[$i].'"|"'.$SunExposure[$i].'"|"'.$TemperatureRequirements[$i].'"|"'.$TIMEOFPRUNING[$i].'"|"'.$twouniqueCharacteristics[$i].'"|"'.$TYPEOFFRUIT[$i].'"|"'.$UniqueCharacteristics[$i].'"|"Each"|"'.$Usage[$i].'"|"'.$WateringRequirements[$i].'"|"'.$WebFloweringTimeSearch[$i].'"|"'.$WebHeightSearch[$i].'"|"'.$WebSearchFlowerColor[$i].'"|"'.$WebSearchZone[$i].'"|"'.$WINTERREQUIREMENTS[$i].'"' ."\n";




		/*$result = $client->call($session_id, 'catalog_product.create', array('simple', $attributeSet['set_id'], 'product_sku', array(
			'categories' => $CategoryIDs[$i],
			'websites' => array(1),
			'name' => $name[$i],
			'sku' => $prodsku[$i],
			'description' => $ShortDescription[$i],
			'short_description' => $ShortDescription[$i],
			'weight' => '1',
			'status' => $status[$i],
			'visibility' => '4',
			'price' => '100',
			'tax_class_id' => 1,
			'meta_title' =>  $meta_title[$i],
			'meta_keyword' =>  $meta_keyword[$i],
			'meta_description' =>  $meta_description[$i],
		)));*/


}


$fname = '/var/www/springhill/apiclient/SHCSV.csv'; 
if(file_exists($fname))
{
	unlink($fname);
}
$fp = @fopen($fname,'w'); 
@fwrite($fp,$_csv_data); 
@fclose($fp); 
 
header('Content-type: application/csv'); 
header("Content-Disposition: inline; filename=".$fname); 
?>
