<?php
$log_array=array();
require_once '../app/Mage.php';
umask(0);
Mage::app('default');
//Mail to be sent for
$arrTo[] =array("toEmail"=>"imsupport@gardensalive.com","toName"=>"imsupport");
$arrTo[] =array("toEmail"=>"janesh@gardensalive.com","toName"=>"Janesh");
//This URL IS USED FOR BATCH PROCESSING
$CronURL='http://192.168.10.136/springhill/updates/InventoryUpdate.php';
//Semaphore Flag
$txtFlagFile='/var/www/springhill/var/import/products/UploadComplete.txt';
//Directory path where import files are located
$dir_import="/var/www/springhill/var/import/products/";
//Directory Path where Import files to be moved after successful import
$dir_Archive= "/var/www/springhill/updates/archive/";
//Checking flag file and then proceeding for Inventory Update
//if (file_exists($txtFlagFile)) {
	//Files to be used for importing Product Description
	$txtProductDescription	=	$dir_import."ProductDescription.csv";
	//Getting The categories associated with Product
	$txtProductCategories	=	$dir_import."SPRG_Product_Categories.txt";
	//Files used for Inventory,InventoryFeed,Price Update
	$txtInventoryFeed	=	$dir_import."InventoryFeed.txt";
	$txtInventory	=	$dir_import."Inventory.txt";
	$txtPrice	=	$dir_import."Price.txt";
	//get the number of rows in $txtProductDescription;
	$file = file($txtProductDescription);
	$totalNumberRows = count($file);

	//Get the limit to be imported by default 100
	/*$limit=2;
	//Get the Start Index for Import
	if(!empty($_GET['start']))
		$start=$_GET['start'];
	else
		$start=0;
	//Setting up the database for Later use Like inserting the import information to the DB
	$write = Mage::getSingleton('core/resource')->getConnection('core_write');
	//$start value is less than the number of rows in the product description else show message
	if($start < $totalNumberRows){
		//Prepending the timestamp to the file name to get in one order
		$timestamp=date("YmdHis")."_";
		$txtArchiveproductDescription	=	$dir_Archive."/".$timestamp."ProductDescription.txt";
		$txtArchiveProductCategories	=	$dir_Archive."/".$timestamp."SPRG_Product_Categories.txt";
		$txtArchiveInv	=	$dir_Archive."/".$timestamp."InventoryFeed.txt";
		$txtArchiveInventory	=	$dir_Archive."/".$timestamp."Inventory.txt";
		$txtArchivePrice	= $dir_Archive."/".$timestamp."Price.txt";*/
		include 'web_conf.php';
		if (file_exists($txtProductDescription)) {
			$handle = fopen($txtProductDescription, "r");
			//will have updated row count
			$ii=0;
			//to be used for checking with limit value
			$loopCount=0;
			while (($data = fgetcsv($handle, 100000, "|")) !== FALSE) {
				//Catching the exception and sending the error log mail
				try{
					//Skip the first row as it contain header information or continue till $ii count is becomes start count
					if($ii == 0 ||  $ii < $start){
						$ii++;
						continue;
					}
					//Break the loop when limit count is reached
					if($loopCount==$limit){
						break;
					}
					//If sku is not empty and should not consider first row of the flat file and start index should be greater than  $start
					if(!empty($data['1'])) {
						//Exploding into array format if cell contain more than SKU
						$product_skus = explode("," , $data['1']);
						foreach($product_skus as $sku){
							$arrayProduct = array();
							if(file_exists($txtProductCategories)){
								$cathandle = fopen($txtProductCategories, "r");
								$aa=0;
								$cat_ids=array();
								while (($catdata = fgetcsv($cathandle, 100000, "|")) !== FALSE) {
									if($aa==0 || $catdata['0']==''){
										$aa++;
										continue;
									}
									if($catdata[0]==$sku){
										//print_r($catdata);
										//echo $catdata[1];
										$cat_ids[]= $catdata[1];
									}
									$aa++;
								}
								//Storing Category Ids
								$categories=implode(",",$cat_ids);
								$categories = array('categories'	=>	array($categories));
								$arrayProduct = array_merge($arrayProduct, $categories);
							}
							$image ="http://springhillnursery.com/images/250/$sku.jpg";
							if($sku)
							{
								if(!file_exists("/var/www/springhill/media/import/".$sku.".jpg")){
									@copy( $image  , "/var/www/springhill/media/import/".$sku.".jpg" );
									if(!file_exists($image)){
										$image ='/noimage.gif';
									}else{
										$image ='/'.$sku.'.jpg';
									}
								}else{
									$image ='/'.$sku.'.jpg';
								}
							}
							$arrImage= array("image" => $image);
							$arrayProduct = array_merge($arrayProduct, $arrImage);
							//stock_data
							$arrstock_data=array();
							// 						$best_seller_indicator = 0;
							// 						$lifetime_guarantee = 0;
							// 						$is_web_only_plant = 0;
							// 						$is_new_plant = 0;
							// 						$is_on_clearance = 0;

							// 						$store="admin";
							// 						$websites="base";
							// 						$has_options = 0;
							// // 						//No Layout updates
							// 						$page_layout  = 'No Layout updates';
							// // 						//Block after info colomn
							// 						$options_container = 'container2';
							// // 						//Manufacturer's Suggested Retail Price
							// 						$msrp='';
							// // 						//Use config
							// 						$msrp_enabled   = '2';
							// // 						//Use config
							// 						$msrp_display_actual_price_type = '4';
							// // 						$gift_message_available  = 'No';
							// // 						$gift_wrapping_available = 'No';
							// 						$is_returnable = 'Use config';
							// // 						$custom_layout_update  = '';
							// // 						//No
							// 						$is_recurring = '2';
							// 						$visibility  = 'Catalog, Search';
							// 						$enable_googlecheckout = 1;
							// // 						//Taxable Goods
							// 						$tax_class_id  = 'Taxable Goods';
							// 						$special_from_date = '0000-00-00 00:00:00';
							// 						// 				$special_price='';
							// 						$special_to_date = '0000-00-00 00:00:00';
							// 						$news_from_date  = '0000-00-00 00:00:00';
							// 						$news_to_date  = '0000-00-00 00:00:00';
							// 						$custom_design_from  = '';
							// 						$custom_design_to   = '';

							// 						$use_config_min_qty[]  ='1';
							// 						$is_qty_decimal[]  = '0';

							// 						$store_id[]   = '0';
							// 						$product_type_id[]   = 'simple';
							// 						$product_status_changed[]  = '';
							// 						$product_changed_websites[]  = '';

							// 						$is_in_stock  = '1';
							// 						$arris_in_stock =array("is_in_stock " =>$is_in_stock);
							// 						$arrstock_data = array_merge($arrstock_data, $arris_in_stock);

							// 						$low_stock_date[]  = '';

							// 						$notify_stock_qty  = '';
							// 						$arrnotify_stock_qty =array("notify_stock_qty" =>$notify_stock_qty);
							// 						$arrstock_data = array_merge($arrstock_data, $arrnotify_stock_qty);

							// 						$use_config_notify_stock_qty  = '1';
							// 						$arruse_config_notify_stock_qty =array("use_config_notify_stock_qty" =>$use_config_notify_stock_qty);
							// 						$arrstock_data = array_merge($arrstock_data, $arruse_config_notify_stock_qty);


							// 						$arrmanage_stock =array("manage_stock " =>0);
							// 						$arrstock_data = array_merge($arrstock_data, $arrmanage_stock);


							// 						$arruse_config_manage_stock =array("use_config_manage_stock " =>0);
							// 						$arrstock_data = array_merge($arrstock_data, $arruse_config_manage_stock);

							// 						$arrstock_data = array_merge($arrstock_data, $arrMinSaleQty);

							// 						$stock_status_changed_auto[]  = '0';
							// 						$use_config_qty_increments[]  = '1';
							// 						$qty_increments[]  = '0';
							// 						$use_config_enable_qty_inc[]  = '1';
							// 						$enable_qty_increments[]  = '0';
							// 						$stock_status_changed_automatically[]   = '0';
							// 						$use_config_enable_qty_increments[]  = '1';
							$store="admin";
							$websites="base";
							$has_options = 0;
							// 						//No Layout updates
							$page_layout  = 'No Layout updates';
							// 						//Block after info colomn
							$options_container = 'container2';
							// 						//Manufacturer's Suggested Retail Price
							$msrp='';
							// 						//Use config
							$msrp_enabled   = '2';
							// 						//Use config
							$msrp_display_actual_price_type = '4';
							// 						$gift_message_available  = 'No';
							// 						$gift_wrapping_available = 'No';
							$is_returnable = 'Use config';
							// 						$custom_layout_update  = '';
							// 						//No
							$is_recurring = '2';
							$visibility  = 'Catalog, Search';
							$enable_googlecheckout = 1;
							//Taxable Goods
							$tax_class_id  = 'Taxable Goods';
							$meta_keyword = '';
							$meta_title="";
							$weight = '1.0';
							//Product description
							$recommendation_url = '';

							$createArray = array(
									"store"=>$store,
									"websites"=>$websites,
									"has_options"=>$has_options,
									"page_layout"=>$page_layout,
									"msrp"=>$msrp,
									"msrp_enabled"=>$msrp_enabled,
									"msrp_display_actual_price_type"=>msrp_display_actual_price_type,
									"is_returnable"=>$is_returnable,
									"is_recurring"=>$is_recurring,
									"visibility"=>$visibility,
									"enable_googlecheckout"=>$enable_googlecheckout,
									"tax_class_id"=>$tax_class_id,
									"weight"=>$weight,
									"recommendation_url"=>$recommendation_url,
									"meta_keyword"=>$meta_keyword,
									"meta_title"=>$meta_title
							);

							//@todoBuilding Product array to be updated or created
							if($data[6]=='TRUE'){
								$arrPlant = array('planting_requirements'	=>	1);
								$arrayProduct = array_merge($arrayProduct, $arrPlant);
							}elseif($data[6]=='FALSE'){
								$arrPlant = array('planting_requirements'	=>	0);
								$arrayProduct = array_merge($arrayProduct, $arrPlant);
							}
							if($data[12]=='TRUE'){
								$shade = array('shade'	=>	1);
								$arrayProduct = array_merge($arrayProduct, $arrShade);
							}elseif($data[12]=='FALSE'){
								$shade = array('shade'	=>	0);
								$arrayProduct = array_merge($arrayProduct, $shade);
							}
							if($data[13]=='TRUE'){
								$partial_shade = array('partial_shade'	=>	1);
								$arrayProduct = array_merge($arrayProduct, $partial_shade);
							}elseif($data[13]=='FALSE'){
								$partial_shade = array('partial_shade'	=>	0);
								$arrayProduct = array_merge($arrayProduct, $partial_shade);
							}
							if($data[14]=='TRUE'){
								$full_sun = array('full_sun'	=>	1);
								$arrayProduct = array_merge($arrayProduct, $full_sun);
							}elseif($data[14]=='FALSE'){
								$full_sun = array('full_sun'	=>	0);
								$arrayProduct = array_merge($arrayProduct, $full_sun);
							}
							if($data[15]=='TRUE'){
								$borders = array('borders'	=>	1);
								$arrayProduct = array_merge($arrayProduct, $borders);
							}elseif($data[15]=='FALSE'){
								$borders = array('borders'	=>	0);
								$arrayProduct = array_merge($arrayProduct, $borders);
							}
							if($data[17]=='TRUE'){
								$rock_gardens = array('rock_gardens'	=>	1);
								$arrayProduct = array_merge($arrayProduct, $rock_gardens);
							}elseif($data[17]=='FALSE'){
								$rock_gardens = array('rock_gardens'	=>	0);
								$arrayProduct = array_merge($arrayProduct, $rock_gardens);
							}
							if($data[18]=='TRUE'){
								$ground_covers = array('ground_covers'	=>	1);
								$arrayProduct = array_merge($arrayProduct, $ground_covers);
							}elseif($data[18]=='FALSE'){
								$ground_covers = array('ground_covers'	=>	0);
								$arrayProduct = array_merge($arrayProduct, $ground_covers);
							}

							if($data[21]=='TRUE'){
								$long_lasting_in_garden = array('long_lasting_in_garden'	=>	1);
								$arrayProduct = array_merge($arrayProduct, $long_lasting_in_garden);
							}elseif($data[21]=='FALSE'){
								$long_lasting_in_garden = array('long_lasting_in_garden'	=>	0);
								$arrayProduct = array_merge($arrayProduct, $long_lasting_in_garden);
							}
							if($data[23]=='TRUE'){
								$cut_flowers = array('cut_flowers'	=>	1);
								$arrayProduct = array_merge($arrayProduct, $cut_flowers);
							}elseif($data[23]=='FALSE'){
								$cut_flowers = array('cut_flowers'	=>	0);
								$arrayProduct = array_merge($arrayProduct, $cut_flowers);
							}
							if($data[25]=='TRUE'){
								$long_lasting_cut = array('long_lasting_cut'	=>	1);
								$arrayProduct = array_merge($arrayProduct, $long_lasting_cut);
							}elseif($data[25]=='FALSE'){
								$long_lasting_cut = array('long_lasting_cut'	=>	0);
								$arrayProduct = array_merge($arrayProduct, $long_lasting_cut);
							}
							if($data[26]=='TRUE'){
								$good_for_forcing = array('good_for_forcing'	=>	1);
								$arrayProduct = array_merge($arrayProduct, $good_for_forcing);
							}elseif($data[26]=='FALSE'){
								$good_for_forcing = array('good_for_forcing'	=>	0);
								$arrayProduct = array_merge($arrayProduct, $good_for_forcing);
							}
							if($data[27]=='TRUE'){
								$good_for_naturalizing = array('good_for_naturalizing'	=>	1);
								$arrayProduct = array_merge($arrayProduct, $good_for_naturalizing);
							}elseif($data[27]=='FALSE'){
								$good_for_naturalizing = array('good_for_naturalizing'	=>	0);
								$arrayProduct = array_merge($arrayProduct, $good_for_naturalizing);
							}
							if($data[28]=='TRUE'){
								$multiplies_annually = array('multiplies_annually'	=>	1);
								$arrayProduct = array_merge($arrayProduct, $multiplies_annually);
							}elseif($data[28]=='FALSE'){
								$multiplies_annually = array('multiplies_annually'	=>	0);
								$arrayProduct = array_merge($arrayProduct, $multiplies_annually);
							}
							if($data[30]=='TRUE'){
								$dried_flowers = array('dried_flowers'	=>	1);
								$arrayProduct = array_merge($arrayProduct, $dried_flowers);
							}elseif($data[30]=='FALSE'){
								$dried_flowers = array('dried_flowers'	=>	0);
								$arrayProduct = array_merge($arrayProduct, $dried_flowers);
							}
							if($data[31]=='TRUE'){
								$container = array('container'	=>	1);
								$arrayProduct = array_merge($arrayProduct, $container);
							}elseif($data[31]=='FALSE'){
								$container = array('container'	=>	0);
								$arrayProduct = array_merge($arrayProduct, $container);
							}
							if($data[76]=='TRUE'){
								$indoors_only = array('indoors_only'	=>	1);
								$arrayProduct = array_merge($arrayProduct, $indoors_only);
							}elseif($data[76]=='FALSE'){
								$indoors_only = array('indoors_only'	=>	0);
								$arrayProduct = array_merge($arrayProduct, $indoors_only);
							}

							if($data[77]=='TRUE'){
								$io_in_warm_temp = array('io_in_warm_temp'	=>	1);
								$arrayProduct = array_merge($arrayProduct, $io_in_warm_temp);
							}elseif($data[77]=='FALSE'){
								$io_in_warm_temp = array('io_in_warm_temp'	=>	0);
								$arrayProduct = array_merge($arrayProduct, $io_in_warm_temp);
							}

							if($data[78]=='TRUE'){
								$permantly_planted_garden = array('permantly_planted_garden'	=>	1);
								$arrayProduct = array_merge($arrayProduct, $permantly_planted_garden);
							}elseif($data[78]=='FALSE'){
								$permantly_planted_garden = array('permantly_planted_garden'	=>	0);
								$arrayProduct = array_merge($arrayProduct, $permantly_planted_garden);
							}

							if($data[87]=='TRUE'){
								$deer_resistant = '1';
								$deer_resistant = array('deer_resistant'	=>	1);
								$arrayProduct = array_merge($arrayProduct, $deer_resistant);
							}elseif($data[87]=='FALSE'){
								$deer_resistant = '0';
								$deer_resistant = array('deer_resistant'	=>	0);
								$arrayProduct = array_merge($arrayProduct, $deer_resistant);
							}

							if($data[88]=='TRUE'){
								$season_shipped_fall = '1';
								$season_shipped_fall = array('season_shipped_fall'	=>	1);
								$arrayProduct = array_merge($arrayProduct, $season_shipped_fall);
							}elseif($data[88]=='FALSE'){
								$season_shipped_fall = '0';
								$season_shipped_fall = array('season_shipped_fall'	=>	0);
								$arrayProduct = array_merge($arrayProduct, $season_shipped_fall);
							}

							if($data[89]=='TRUE'){
								$season_shipped_spring = array('season_shipped_spring'	=>	1);
								$arrayProduct = array_merge($arrayProduct, $season_shipped_spring);
							}elseif($data[89]=='FALSE'){
								$season_shipped_spring = array('season_shipped_spring'	=>	0);
								$arrayProduct = array_merge($arrayProduct, $season_shipped_spring);
							}

							$breck_name = addslashes($data[8]);
							$common_name = addslashes($data[9]);
							$family = addslashes($data[11]);
							$good_for_other = addslashes($data[32]);
							$web_height_search = addslashes($data[33]);
							$height_habit = addslashes($data[34]);
							$spread = addslashes($data[35]);
							$spacing = addslashes($data[36]);
							$foliage_type = addslashes($data[37]);
							$flower_form = addslashes($data[39]);
							$average_number_blooms = addslashes($data[40]);
							$flower_color =  addslashes($data[41]);
							$web_search_flower_color = addslashes($data[42]);
							$fragrance = addslashes($data[43]);
							$planting_time = addslashes($data[44]);
							$web_flowering_time_search = addslashes($data[45]);
							$flowering_date = addslashes($data[46]);
							$duration = addslashes($data[47]);
							$blooms_width =addslashes( $data[49]);
							$web_search_zone = addslashes($data[50]);
							$display_hardiness_zone = addslashes($data[51]);
							$hardiness_zones	= 	addslashes($data[51]);
							$type_of_fruit = addslashes($data[52]);
							$shipped = addslashes($data[53]);
							$special_care_instructions = addslashes($data[54]);
							$soil_requirements =addslashes( $data[56]);
							$root_system = addslashes($data[57]);
							$growth_rate =  addslashes($data[58]);
							$description = addslashes($data[59]);
							$short_description= addslashes($data[59]);
							$unique_characteristics = addslashes($data[59]);
							$two_unique_characteristic = addslashes($data[60]);
							$key_difference_similar_bulbs = addslashes($data[62]);
							$form = addslashes($data[64]);
							$resistance = addslashes($data[65]);
							$pruning = addslashes($data[66]);
							$time_of_pruning = addslashes($data[67]);
							$additional_information = addslashes($data[68]);
							$awards = addslashes($data[70]);
							$winter_requirement = addslashes($data[79]);
							$temperature_requirements = addslashes($data[80]);
							$light_needed_indoors = addslashes($data[81]);
							$watering_requirements = addslashes($data[82]);
							$fertilization_requirements = addslashes($data[83]);
							$special_care = addslashes($data[84]);
							//Misspelled Additional Search Words
							$search_words ="";

							$txtArrayProduct = array(
									"breck_name"=>$breck_name,
									"common_name"=>$common_name,
									"family"=>$family,
									"good_for_other"=>$good_for_other,
									"web_height_search"=>$web_height_search,
									"height_habit"=>$height_habit,
									"spread"=>$spread,
									"spacing"=>$spacing,
									"foliage_type"=>$foliage_type,
									"flower_form"=>$flower_form,
									"average_number_blooms"=>$average_number_blooms,
									"flower_color"=>$flower_color,
									"web_search_flower_color"=>$web_search_flower_color,
									"fragrance"=>$fragrance,
									"planting_time"=>$planting_time,
									"web_flowering_time_search"=>$web_flowering_time_search,
									"flowering_date"=>$flowering_date,
									"duration"=>$duration,
									"blooms_width"=>$blooms_width,
									"web_search_zone"=>$web_search_zone,
									"display_hardiness_zone"=>$display_hardiness_zone,
									"hardiness_zones"=>$hardiness_zones,
									"type_of_fruit"=>$type_of_fruit,
									"shipped"=>$shipped,
									"special_care_instructions"=>$special_care_instructions,
									"soil_requirements"=>$soil_requirements,
									"root_system"=>$root_system,
									"growth_rate"=>$growth_rate,
									"description"=>$description,
									"short_description"=>$short_description,
									"unique_characteristics"=>$unique_characteristics,
									"two_unique_characteristic"=>$two_unique_characteristic,
									"key_difference_similar_bulbs"=>$key_difference_similar_bulbs,
									"form"=>$form,
									"resistance"=>$resistance,
									"pruning"=>$pruning,
									"time_of_pruning"=>$time_of_pruning,
									"additional_information"=>$additional_information,
									"awards"=>$awards,
									"winter_requirement"=>$winter_requirement,
									"temperature_requirements"=>$temperature_requirements,
									"light_needed_indoors"=>$light_needed_indoors,
									"watering_requirements"=>$watering_requirements,
									"fertilization_requirements"=>$fertilization_requirements,
									"special_care"=>$special_care,
									"search_words"=>$search_words
							);
							//Merge array with main array
							$arrayProduct = array_merge($arrayProduct, $txtArrayProduct);
							//For the name of the product
							if(!empty($breck_name)) {
								$name =$breck_name;
							}else if(!empty($common_name)) {
								$name =$common_name;//$start value is less than the number of rows in the product description else show messageommon_name;
							}
							else if(!empty($botanical_name) ) {
								$name =$botanical_name;
							}
							//add SKU,status to the arrayProduct array
							$arrName = array("name" => $name);
							$arrayProduct = array_merge($arrayProduct, $arrName);


							//Check if sku is already exists or not if exists Update else create
							$_Pdetails =Mage::getModel('catalog/product')->loadByAttribute('sku',$sku);
							if($_Pdetails){
								$productid= $_Pdetails->getId();
								$result = $client->call($session_id, 'catalog_product.update', array($productid, $arrayProduct));
								if($result){
									echo 'updated##########';//.$sku.'<br/>';
								}else{
									echo 'failed';//.$sku.'<br/>';
								}
							}else{
								//add SKU,status to the arrayProduct array
								$arrSku = array("sku" => $sku,"status"=>"1");
								$arrayProduct = array_merge($arrayProduct, $arrSku);
								//Adding default attributes
								$arrayProduct = array_merge($arrayProduct, $createArray);
								// get attribute set
								$attributeSets = $client->call($session_id, 'product_attribute_set.list');
								$attributeSet = $attributeSets[1];//
								$result = $client->call($session_id, 'catalog_product.create', array('simple', $attributeSet['set_id'], 'product_sku',$arrayProduct));
								if(is_numeric($result)){
									echo 'created'.$result.'<br/>';
								}else{
									echo 'create failed'.$sku.'<br/>';
								}
							}
						}
						$arrayInvPrice = array();
						//Importing Inventory Feed From the File having same SKU as in ProductDescription.txt
						if (file_exists($txtInventoryFeed)) {
							$invhandle = fopen($txtInventoryFeed, "r");
							$jj=0;
							while (($invdata = fgetcsv($invhandle, 100000, "|")) !== FALSE) {
								if($jj==0 || $invdata['0']==''){
									$jj++;
									continue;
								}
								if($invdata[0]==$sku){
									//,"recommended_items"=>$invdata[4]
									//
									$arrInvFeed =array(
											"botanical_name" =>$invdata[1],
											"restricted_states"=>$invdata[3],
											"unit_of_measure" => $invdata[2]
											);
									$arrayInvPrice = array_merge($arrayInvPrice, $arrInvFeed);
									// 								$botanical_name = $invdata[1];
									// 								$restricted_states = $invdata[3];
								}
								$jj++;
							}
						}
						//Importing Inventory From the File having same SKU as in ProductDescription.txt
						if (file_exists($txtInventory)) {
							$inventoryhandle = fopen($txtInventory, "r");
							$kk=0;
							while (($inventorydata = fgetcsv($inventoryhandle, 100000, "|")) !== FALSE) {

								if($kk==0 || $inventorydata['0']=='' || $inventorydata[0]!=$sku){
									$kk++;
									continue;
								}
								if($inventorydata[0]==$sku){
									if(!empty($inventorydata[4])){
										$arrqty =array("qty" =>$inventorydata[4]);
										$arrstock_data = array_merge($arrstock_data, $arrqty);
									}
									if(!empty($inventorydata[4])){
										$arrqty =array("qty" =>$inventorydata[4]);
										$arrstock_data = array_merge($arrstock_data, $arrqty);
									}
									$BackOrderDate[] = $inventorydata[11];

									$arrSeason =array("season" => $inventorydata[7]);
									$arrayInvPrice = array_merge($arrayInvPrice, $arrSeason);

									if($inventorydata[11]!= ""){
										$arrBackOrders =array("backorders" =>1,
												"use_config_backorders"=>0);
										$arrstock_data = array_merge($arrstock_data, $arrBackOrders);
									}else{
										$arrBackOrders =array("backorders" =>0,
												"use_config_backorders"=>1);
										$arrstock_data = array_merge($arrstock_data, $arrBackOrders);
									}

									if($inventorydata[3]=='Y'){
										$discontinued =array("discontinued" =>1);
										$arrayInvPrice = array_merge($arrayInvPrice, $discontinued);
									}elseif($inventorydata[3]=='N'){
										$discontinued =array("discontinued" =>0);
										$arrayInvPrice = array_merge($arrayInvPrice, $discontinued);
									}

// 									if($inventorydata[9]=='Y'){
										$future_season_indicator =array("future_season_indicator" =>$inventorydata[9]);
										$arrayInvPrice = array_merge($arrayInvPrice, $future_season_indicator);
// 									}elseif($inventorydata[9]=='N'){
// 										$future_season_indicator =array("future_season_indicator" =>0);
// 										$arrayInvPrice = array_merge($arrayInvPrice, $future_season_indicator);
// 									}

// 									if($inventorydata[10]=='1'){
										$future_season_message =array("future_season_message" =>$inventorydata[10]);
										$arrayInvPrice = array_merge($arrayInvPrice, $future_season_message);
// 									}else{
// 										//if($inventorydata[10]=='N'){
// 										$future_season_message =array("future_season_message" =>0);
// 										$arrayInvPrice = array_merge($arrayInvPrice, $future_season_message);
// 									}

									if($inventorydata[2]=='Y'){
										$status =array("status" =>1);
										$arrayInvPrice = array_merge($arrayInvPrice, $status);
									}elseif($inventorydata[2]=='N'){
										$status =array("status" =>2);
										$arrayInvPrice = array_merge($arrayInvPrice, $status);
									}
									
// 									recommended_items
									//Break the loop if sku is found in the file
									break;
								}
								$kk++;
							}
						}
						/// To import the data from Price.txt haing same product_number and offer_code=4 in productdescription_test.csv Attribute
							
						if (file_exists($txtPrice)) {
							$pricehandle = fopen($txtPrice, "r");
							$ll=0;
							$ProductOffersCode='';
							while (($pricedata = fgetcsv($pricehandle, 100000, "|")) !== FALSE) {

								if($ll==0 || $pricedata['0']==''){
									$ll++;
									continue;
								}
								if($pricedata[0]==$sku && $pricedata[1]!=4 && $pricedata[1]!=''){
									$ProductOffersCode .= $pricedata[1].",";
								}
								if($pricedata[0]==$sku && $pricedata[1]==4){

									$CatalogCode[] = '';
									$price = $pricedata[4];
									$MaxQuantity[] = $pricedata[3];
									$salePrice[] = $pricedata[6];
									$wholesalePrice[] = '';
									
									//Merge arrInvPrice and Price
									$arrPrice=array("price" =>$pricedata[4]);
									$arrayInvPrice = array_merge($arrayInvPrice, $arrPrice);
									
									if($pricedata[2]!=''){
										$arrMinSaleQty =array("min_sale_qty" =>$pricedata[2],
												"use_config_min_sale_qty "=>0);
										$arrstock_data = array_merge($arrstock_data, $arrMinSaleQty);
									}else{
										$arrMinSaleQty =array("min_sale_qty" =>1,
												"use_config_min_sale_qty "=>1);
										$arrstock_data = array_merge($arrstock_data, $arrMinSaleQty);
									}
									if($pricedata[3]!=''){
										$arrMaxSaleQty =array("max_sale_qty" =>$pricedata[3],
												"use_config_max_sale_qty"=>0);
										$arrstock_data = array_merge($arrstock_data, $arrMaxSaleQty);
									}else{
										$arrMaxSaleQty =array("max_sale_qty" =>1,
												"use_config_max_sale_qty"=>1);
										$arrstock_data = array_merge($arrstock_data, $arrMaxSaleQty);
									}

								}
								else{
									$min_sale_qty[] = '1';
									$use_config_min_sale_qty[]  = '1';
									$max_sale_qty[]   = '0';
									$use_config_max_sale_qty[]  = '1';
									$arrBackOrders =array("backorders" =>0,
											"use_config_backorders"=>1);
									$arrstock_data = array_merge($arrstock_data, $arrBackOrders);
								}
								$ll++;

							}
						}
						if($ProductOffers!=''){
							$product_offers_code =array("product_offers_code" =>substr($ProductOffersCode,0,-1));
							$arrayInvPrice = array_merge($arrayInvPrice, $product_offers_code);
						}
							
						//Merge arrInvPrice and stock Data
						$stockData=array("stock_data" =>$arrstock_data);
						$arrayInvPrice = array_merge($arrayInvPrice, $stockData);
						//Check if sku is already exists or not if exists Update else create
						$_Pdetails =Mage::getModel('catalog/product')->loadByAttribute('sku',$sku);
						if($_Pdetails){
							$productid= $_Pdetails->getId();
							$result = $client->call($session_id, 'catalog_product.update', array($productid, $arrayInvPrice));
							if($result){
								echo 'updated Inventory';//.$sku.'<br/>';
							}else{
								echo 'failed';//.$sku.'<br/>';
							}
						}

						$ii++;
					}
				}catch (Exception $e){
					$log_array[]['Message']=$e->getMessage();
					$log_array[]['Trace']=$e->getTraceAsString();
					$log_array[]['sku']=$sku;
				}

			}
			/*if($ii==$totalNumberRows){
				if (copy($txtProductDescription,$txtArchiveproductDescription)) {
					unlink($txtProductDescription);
				}
				if (copy($txtInventoryFeed,$txtArchiveInv)) {
					unlink($txtInventoryFeed);
				}
				if (copy($txtInventory,$txtArchiveInventory)) {
					unlink($txtInventory);
				}
				if (copy($txtPrice,$txtArchivePrice)) {
					unlink($txtPrice);
				}
				if (copy($txtProductCategories,$txtArchiveProductCategories)) {
					unlink($txtProductCategories);
				}
				chmod($txtFlagFile, 0755);
				unlink ($txtFlagFile);
				//Saving the lastupdated time in database for 36 hours email notification
				$InventoryId=$write->fetchOne("SELECT import_notification_id FROM import_notification_history WHERE import_type='inventory'");
				if(empty($InventoryId)){
					$write->query("INSERT INTO import_notification_history(import_date,import_type) VALUES('".date("Y-m-d H:i:s")."','inventory')");
				}else{
					$write->query("UPDATE import_notification_history SET import_date='".date("Y-m-d H:i:s")."', email_notification='No' WHERE import_notification_id=".$InventoryId);
				}

			}else{
				usleep(2000000);
				$start =$start+$limit;
				echo '<meta http-equiv="refresh" content="2; url='.$CronURL.'?start='.$start.'">';
			}*/
				
			/*//ERROR LOG
			if(count($log_array)> 0){
				$errorMsg='';
				foreach ($log_array as $log){
					$errorMsg .= '
					- - - - - - - -
					ERROR
					Message: '.$log['Message'].'
					Trace: '.$log['Trace'].'
					ProductId: '.$log['sku'].'
					Time: '.date('Y m d H:i:s').'
					';
					echo "\r\n".$errorMsg;
				}
				file_put_contents($dir_Archive.'/log.txt', $errorMsg);
				//Getting admin Information
				$adminInfo =$write->fetchAll("SELECT * from  admin_user");
				$userFirstname =$adminInfo[0]['firstname'];
				$userLastname =$adminInfo[0]['lastname'];
				$userEmail=$adminInfo[0]['email'];
				$arrTo[]=array("toEmail"=>$userEmail,"toName"=>$userFirstname.' '.$userLastname);
					
				$mail = new Zend_Mail();
				$mail->setType(Zend_Mime::MULTIPART_RELATED);
				$mail->setBodyHtml('Please see attached log file');
				$mail->setFrom($userEmail, $userFirstname.' '.$userLastname);
				//SENDING MAIL TO Janesh,imsupport@gardensalive.com and Magento Admin
				foreach($arrTo as $to){
					$mail->addTo($to['toEmail'], $to['toName']);
				}
				$mail->setSubject('Import Error Log');
				$fileContents = file_get_contents($dir_Archive.'/log.txt');
				$file = $mail->createAttachment($fileContents);
				$file->filename = "log.txt";
				try {
					if($mail->send())
					{
						Mage::getSingleton('core/session')->addSuccess('Log Information has been mailed');
					}
				}
				catch(Exception $ex) {
					Mage::getSingleton('core/session')->addSuccess('Unable to send mail');
						
				}
			}*/
			/*//DELETE THE ARCHIVE FILES OLDER THAN 30 Days
			if ($handle = opendir($dir_Archive)) {
				while (false !== ($entry = readdir($handle))) {
					if ($entry != "." && $entry != "..") {
						$arrFiles =explode("_",$entry);
						$fileTimeStamp=$arrFiles[0];
						$date1 = date('YmdHis');
						$date2 = $fileTimeStamp;
						$ts1 = strtotime($date1);
						$ts2 = strtotime($date2);
						$seconds_diff = $ts1 - $ts2;
						$days = floor($seconds_diff/3600/24);
						//Delete the files older than 30 days
						if($days > 30){
							unlink($dir_Archive.$entry);
						}
					}
				}
				closedir($handle);
			}*/
		}
	/*}else{
		echo "Start values exceeds the total number of rows.Please check ";
	}*/
/* }else{
	echo "Sorry Semaphore flag is not found";
} */

