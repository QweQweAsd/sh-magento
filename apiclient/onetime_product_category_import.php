<?php
require_once '../app/Mage.php';
umask(0);
Mage::app('default');
//include 'web_conf.php';
$write = Mage::getSingleton('core/resource')->getConnection('core_write');

$products = Mage::getModel('catalog/product')
			->getCollection()
			->addAttributeToSelect('*')
			//->addAttributeToFilter('status', array('value' => 1))
			->addAttributeToSort('entity_id','desc')
			->load();
echo count($products)."\n";
$i=0;
$exception ="";
foreach ($products  as $product) {
	$catStr='';
    $sku = $product->getSku();
	$productId = $product->getId();
	//echo $productId."\n";
    //if($productId<7501) continue;
	$_product = Mage::getModel('catalog/product')->load($productId);
	$countsku = explode("_",$sku);
	if(count($countsku)>1)  continue; 
	$skuId = $sku."_VSPMAAC";
	//$offerproductId = Mage::getModel('catalog/product')->getIdBySku("$skuId");
	//$_offerproduct = Mage::getModel('catalog/product')->load($offerproductId);
	if (!empty($sku) && ($sku != "product_sku") && $productId>2640) {
	    try {

	        $cat_names = $write->fetchAll("SELECT spc.Parent_Cat_Name,spc.Sub_Cat_Name  FROM Category_new AS spc where spc.Product_Number=" . $sku);
	        $cat_ids = array();
			
			foreach($cat_names  as  $cat_name){
				$categoryName ='';
				if($cat_name['Sub_Cat_Name']==''){
					$categoryName = $cat_name['Parent_Cat_Name'];
				}else{
					$categoryName = $cat_name['Sub_Cat_Name'];
				}


				if($categoryName=='Sun Perennial Flowers'){
					$categoryName ='Sun Perennial';
				}
				if($categoryName=='Shrubs & Hedges'){
					$categoryName ='Shrubs';
				}
				if($categoryName=='Ornamental Grasses'){
					$categoryName ='Grasses';
				}
				if($cat_name['name']=='Drought Tolerant Plants'){
					$categoryName ='Drought Tolerant';
				}

				$categoryName = addslashes($categoryName );

            	$cat_ids[] = $write->fetchOne("SELECT entity_id FROM catalog_category_entity_varchar where attribute_id = '41' and value='" . $categoryName . 
"'");
				$catStr .= $categoryName.',';
			}

            $categories = array('categories' => array_unique($cat_ids));
            try {
                //$result = $client->call($session_id, 'catalog_product.update', array($productId, $categories));
						
				$_product->setCategoryIds($categories);				
				$_product->save();				
                echo 'Updated: ' . $i++."---". $_product->getId()."------".$sku . '---------' . $catStr . "\n";
				
            } catch (Exception $e) {
               $exception .= 'Exception 1: ' . $i++ . "---" . $sku . '---------' . $catStr ."\n";
            }
		
	    } catch (Exception $e) {
	        $exception .='Exception 2: ' .  $i++ ."---". $sku . '---------' . $sku . "\n";
	    }
	}
}
print $exception;
?>
