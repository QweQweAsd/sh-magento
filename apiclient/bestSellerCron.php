<?php

require_once '/var/www/html/springhill/app/Mage.php';
umask(0);
Mage::app('default');
$today = time();
$last = $today - (60*60*24*10);
$current = $today + (60*60*24*1);
$from = date("Y-m-d", $last);
$to = date("Y-m-d", $current);
$write = Mage::getSingleton('core/resource')->getConnection('core_write');
$_productCollection = Mage::getResourceModel('reports/product_collection')
    ->addAttributeToSelect('entity_id','ordered_qty','price')
    ->addOrderedQty($from,$to)
    ->setVisibility(Mage::getSingleton('catalog/product_visibility')->getVisibleInCatalogIds())
    ->setOrder('ordered_qty', 'desc');
    //->setPageSize(1);
   //echo $_productCollection->printLogQuery(true);exit;
$i=1;
   foreach ($_productCollection as $product){
       //,SUM( sfoi.qty_ordered ) AS `ordered_qty`
       $_product = Mage::getModel('catalog/product')->load($product->getId());
       $ProdSku = $_product->getSku();
       //check for configurable product
       $offerSku = $ProdSku . '_VSPMAAC';
       $duplicate_productid = $write->fetchOne("SELECT entity_id from catalog_product_entity where sku = '".$offerSku."' ");
       $offerQuantity= 0;
       $offerPrice =0;
       if (!empty($duplicate_productid)) {
            $Offerquery ="SELECT SUM( sfoi.base_row_total ) AS `ordered_price`,SUM( sfoi.qty_ordered ) AS `ordered_qty`
            FROM `sales_flat_order_item` sfoi
            WHERE sfoi.product_id ='".$duplicate_productid."' AND (sfoi.created_at BETWEEN '".$from."' AND '".$to."') GROUP BY sfoi.product_id";
           //echo $duplicate_productid;
           $offerProducts = $write->fetchAll($Offerquery);
		   if(!empty($offerProducts[0]['ordered_qty']))
           		echo $offerQuantity = $offerProducts[0]['ordered_qty'];
		   if(!empty($offerProducts[0]['ordered_price']))
           		echo $offerPrice =  $offerProducts[0]['ordered_price'];
       }
        $query ="SELECT SUM( sfoi.base_row_total ) AS `ordered_price`
        FROM `sales_flat_order_item` sfoi
        WHERE sfoi.product_id ='".$product->getId()."' AND (sfoi.created_at BETWEEN '".$from."' AND '".$to."')";

        $baseRowTotal = $write->fetchOne($query);
        if(empty($baseRowTotal)|| $baseRowTotal< 0){
            $baseRowTotal =0;
        }
        

		$totalOrderedQty =  $product->getOrderedQty();
        if(empty($totalOrderedQty)|| $totalOrderedQty< 0){
            $totalOrderedQty =0;
        }

        /**Final Qty and sale price*/
            $totalQty = $totalOrderedQty+$offerQuantity;
            $totalPrice = $baseRowTotal+$offerPrice;
        /***/

        $_product->setLastTendaysSalePrice($totalPrice);
        $_product->setLastTendaysSaleQuantity($totalQty);
        $_product->save();
        echo $i++."=====ProductSKU=====>".$product->getSku()."====Ordered_Qty=====>".$totalQty.'===Total Price===>'.$totalPrice;
        echo "\n";
   }

?>

