<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category    sphill
 * @package     sphill_Customization
 * @copyright   Copyright (c) 2009 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */


/**
 * Cart PDF model
 *
 * @category   sphill
 * @package    sphill_Customization
 * @author     Magento Core Team <core@magentocommerce.com>
 */

require_once ('lib/Zend/Pdf/Color/Rgb.php');
require_once ('lib/Zend/Pdf/Color/GrayScale.php');

class Sphill_Customization_Model_Cart_Pdf extends Mage_Sales_Model_Order_Pdf_Abstract
{
    public function getPdf()
    {
        $this->_beforeGetPdf();

        $pdf = new Zend_Pdf();
        $style = new Zend_Pdf_Style();
        $this->_setFontBold($style, 10);

        $page = $pdf->newPage(Zend_Pdf_Page::SIZE_A4);
        $pdf->pages[] = $page;

        $y = 780;

        /* Add image */
        $this->insertLogo($page);
        $y -= 10;

        $page->setFillColor(new Zend_Pdf_Color_RGB(0.4, 0.4, 0.4));
        $this->_setFontBold($page, 14);
        $page->drawText(Mage::helper('sales')->__('Shopping Cart'), 35, $y+15, 'UTF-8');
        $this->_setFontRegular($page, 8);


        /* Add table */
        $page->setFillColor(new Zend_Pdf_Color_RGB(0.93, 0.92, 0.92));
        $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.5));
        $page->setLineWidth(0.5);

        $page->drawRectangle(25, $y, 570, $y -15);
        $y -=10;

        /* Add table head */
        $page->setFillColor(new Zend_Pdf_Color_RGB(0.4, 0.4, 0.4));
        $page->drawText(Mage::helper('sales')->__('Items'), 35, $y, 'UTF-8');
        $page->drawText(Mage::helper('sales')->__('Price'), 380, $y, 'UTF-8');
        $page->drawText(Mage::helper('sales')->__('Qty'), 430, $y, 'UTF-8');
        $page->drawText(Mage::helper('sales')->__('Subtotal'), 535, $y, 'UTF-8');

        $y -=15;

        $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));

        /* Add body */
        foreach ($this->getAllItems() as $item) {
            $y -=10;
            /* @var $item sphill_Sales_Model_Quote_Item */

            $_incl = Mage::helper('checkout')->getSubtotalInclTax($item);

            $page->setFillColor(new Zend_Pdf_Color_RGB(0.4, 0.4, 0.4));

            if ($item->getHasChildren()) {
                $this->_setFontBold($page);
            }
            if ($item->getParentItemId() > 0) {
                $prodNameXpos = 40;
            } else {
                $prodNameXpos = 35;
            }
            $page->drawText(strip_tags(htmlspecialchars_decode(stripslashes($item->getProduct()->getName()))), $prodNameXpos, $y, 'UTF-8');

            if ($item->getGiftNumber()) {
                $unitPrice = 0;
            } else {
                $unitPrice = $item->getCalculationPrice();
            }
 
            if ($unitPrice > 0 || !$item->getHasChildren()) {
                $page->drawText($this->formatPrice($unitPrice), 380, $y, 'UTF-8');
            }

            $page->drawText($item->getQty()*1, 430, $y, 'UTF-8');
           $totalPrice = $item->getRowTotal();
         // $totalPrice = $_incl - $item->getWeeeTaxRowDisposition();
            if ($totalPrice > 0 || !$item->getHasChildren()) {
                $page->drawText($this->formatPrice($totalPrice), 535, $y, 'UTF-8');
            }

            if ($item->getHasChildren()) {
                $this->_setFontRegular($page);
            }

            $prodNo = trim($item->getProduct()->getProductNo());
            if (strlen($prodNo)) {
                $y -= 7;
                $this->_setFontItalic($page);
                $page->drawText(Mage::helper('sphill_customization')->__('Item #').$prodNo, $prodNameXpos, $y, 'UTF-8');
                $this->_setFontRegular($page);

            }
        }

        $y -= 20;
        /* Add totals */
        $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.5));
        $page->setLineWidth(0.5);
        $page->drawLine(25, $y, 570, $y);
        $y -= 15;

        $totals = $this->getTotals();

        $page->setFillColor(new Zend_Pdf_Color_RGB(0.4, 0.4, 0.4));
        $this->_setFontBold($page, 8);
        if (isset($totals['subtotal'])) {
            $page->drawText(Mage::helper('sales')->__('Subtotal'), 454, $y, 'UTF-8');
            $page->drawText($this->formatPrice($totals['subtotal']->getValue()), 534, $y, 'UTF-8');
        }

            
        if (isset($totals['discount'])) {
$y -=15;
$this->_setFontBold($page, 8);
            $page->drawText(Mage::helper('sales')->__('Discount'), 454, $y, 'UTF-8');
            $page->drawText($this->formatPrice($totals['discount']->getValue()), 534, $y, 'UTF-8');
        }

           
        if (isset($totals['shipping'])) {
 $y -=15;
$this->_setFontBold($page, 8);
            $page->drawText(Mage::helper('sales')->__('Shipping & Handling'), 454, $y, 'UTF-8');
            $page->drawText($this->formatPrice($totals['shipping']->getValue()), 534, $y, 'UTF-8');
        }

 
        if (isset($totals['tax'])) {
 $y -=15;
$this->_setFontBold($page, 8);
            $page->drawText(Mage::helper('tax')->__('Tax'), 454, $y, 'UTF-8');
            $page->drawText($this->formatPrice($totals['tax']->getValue()), 534, $y, 'UTF-8');
        } 

        
        if (isset($totals['grand_total'])) {
$y -= 15;
        $this->_setFontBold($page, 8);
            $page->drawText(Mage::helper('sales')->__('Grand Total'), 454, $y, 'UTF-8');
            $page->drawText($this->formatPrice($totals['grand_total']->getValue()), 534, $y, 'UTF-8');
        }





        $this->_afterGetPdf();
        return $pdf;
    }

    public function getAllItems()
    {
        $cart = Mage::getSingleton('checkout/cart');
        /* @var $cart Mage_Checkout_Model_Cart */

    	return $cart->getQuote()->getAllItems();
    }

    public function getTotals()
    {
    	$cart = Mage::getSingleton('checkout/cart');
        /* @var $cart Mage_Checkout_Model_Cart */

    	return $cart->getQuote()->getTotals();
    }

    public function formatPrice($price)
    {
        $priceHtml = Mage::helper('checkout')->formatPrice($price);
        return strip_tags($priceHtml);
    }


}
