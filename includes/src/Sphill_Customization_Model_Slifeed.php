<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category   Sphill
 * @package    Sphill_Customization
 * @copyright  Copyright (c) 2009 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

class Sphill_Customization_Model_Slifeed extends Varien_Object
{
    protected $_delimiter = '|';
    protected $_enclosure = '"';
    protected $_includeRootInPath;
    protected $_justUniqueProducts = true;
    protected $_categoriesCollection;
    protected $_categoriesTree;
    protected $_categoryModel;
    protected $_rootNode;
    protected $_processedProducts = array();
    protected $_familyLeaderFlag = 0;
    protected $_currentProductCount = 0;

    public function _construct()
    {
        $feedFileCA = $this->getFilenameCA();
        if ( !$feedFileCA ) {
            $feedFileCA = Mage::getBaseDir('var') . DS . 'export' . DS . 'sphill_ca_{{date}}.txt';
        }
        $feedFileSLI = $this->getFilenameSLI();
        if ( !$feedFileSLI ) {
            $feedFileSLI = Mage::getBaseDir('var') . DS . 'export' . DS . 'sphill_sli_{{date}}.txt';
        }
        $feedDir = dirname($feedFileCA);
        if ( !is_dir($feedDir)) {
            mkdir($feedDir, 0777);
        }
        $feedFileCA = str_replace('{{date}}', Mage::getSingleton('core/date')->date('Y-m-d'), $feedFileCA);
        $feedFileSLI = str_replace('{{date}}', Mage::getSingleton('core/date')->date('Y-m-d'), $feedFileSLI);
        $this->setFilenameCA($feedFileCA);
        $this->setFilenameSLI($feedFileSLI);
    }

    public function generateFeeds()
    {
        /**
         * Saving current store and store from front end
         */
        $cronStore = Mage::app()->getStore();
        $frontStore = Mage::app()->getDefaultStoreView();

        try {
            //setting frontend store as current
            Mage::app()->setCurrentStore($frontStore);

            $this->_currentProductCount = 0;

            $filenameSLI = $this->getFilenameSLI();
            $dataStreamSLI = fopen($filenameSLI, 'w') or Mage::throwException('Can\'t open file for SLI data.');

            $filenameCA = $this->getFilenameCA();
            $dataStreamCA = fopen($filenameCA, 'w') or Mage::throwException('Can\'t open file for CA data.');

            $this->writeProductsDataToCsvStreams($dataStreamSLI, $dataStreamCA);

            fclose($dataStreamSLI);
            @chmod($filenameSLI, 0777);

            fclose($dataStreamCA);
            @chmod($filenameCA, 0777);

            //reverting current store to cron store
            Mage::app()->setCurrentStore($cronStore);
        } catch (Exception $e) {
            Mage::logException($e);
            //in case of exceptions revert store and throw exception to next warrior
            Mage::app()->setCurrentStore($cronStore);
            throw $e;
        }

        return array($filenameSLI, $filenameCA);
    }

/*
    public function generateSliFeed()
    {
        $filename = $this->getFilenameSLI();
        $this->_currentProductCount = 0;
        if ( $dataStream = fopen($filename, 'w') ) {
            echo $filename . "<br />";
            $this->writeProductsDataToCsvStream($dataStream, 'SLI');
            fclose($dataStream);
            @chmod($filename, 0777);
        }
        return $filename;
    }

    public function generateCAFeed()
    {
        $filename = $this->getFilenameCA();
        $this->_currentProductCount = 0;
        if ( $dataStream = fopen($filename, 'w') ) {
            echo $filename . "<br />";
            $this->writeProductsDataToCsvStream($dataStream, 'CA');
            fclose($dataStream);
            @chmod($filename, 0777);
        }
        return $filename;
    }

    public function writeProductsDataToCsvStream($stream, $feedType='CA', $delimiter='|', $enclosure='"')
    {
        if ( is_resource($stream) ) {
            return $this->_writeProductsDataToCsvStream($stream, $feedType, $delimiter, $enclosure);
        }
    }
*/

    public function writeProductsDataToCsvStreams($streamSLI, $streamCA, $delimiter='|', $enclosure='"')
    {
        if ( is_resource($streamSLI) && is_resource($streamCA)) {
            return $this->_writeProductsDataToCsvStreams($streamSLI, $streamCA, $delimiter, $enclosure);
        }
    }

/*
    public function _writeProductsDataToCsvStream($stream, $feedType='CA', $delimiter='|', $enclosure='"')
    {
        $this->_delimiter = $delimiter;
        $this->_enclosure = $enclosure;
        $this->_initCategoriesTree();

        $this->_rootNode  = $this->_categoriesTree->getNodeById(
                Mage::app()->getDefaultStoreView()->getRootCategoryId()
            );
        $this->_writeCategoryDataToCsvStream($stream, $feedType, $this->_rootNode);
    }
*/

    public function _writeProductsDataToCsvStreams($streamSLI, $streamCA, $delimiter='|', $enclosure='"')
    {
        $this->_delimiter = $delimiter;
        $this->_enclosure = $enclosure;
        $this->_initCategoriesTree();

        $store = Mage::app()->getDefaultStoreView();

        $productCollection = Mage::getResourceModel('catalog/product_collection')
            ->setStore($store)
            ->setStoreId($store->getId())
            ->addAttributeToSelect(
                    array('name',
                        'visibility',
                        'short_description',
                        'description',
                        'image',
                        'small_image',
                        'thumbnail',
                        'product_no',
                        'special_comments',
                        'price',
                        'search_terms',
                        'url_key',
                        'special_price',
                        'special_from_date',
                        'special_to_date',
                        'description',
                        'short_description',
                        'image_label',
                        'thumbnail_label',
                        'small_image_label',
                        'tax_class_id')
                )
            ->addAttributeToFilter('status', 1)
            ->joinField('qty',
                'cataloginventory/stock_item',
                'qty',
                'product_id=entity_id',
                '{{table}}.stock_id=1',
                'left');

        $pageSize = 100;
        $totalPages = ceil($productCollection->getSize()/$pageSize);
        $curPage = 0;

        while ($curPage <= $totalPages) {
            set_time_limit(600);
            $_collection = clone $productCollection;
            $_collection->setPage($curPage, $pageSize);
            foreach ( $_collection as $product ) {
                if ( !$this->_justUniqueProducts || !in_array($product->getId(), $this->_processedProducts) ) {
                    try {
                        $productDataSLI = $productDataCA = $this->_getProductData($product);
                        if (in_array($product->getVisibility(), array(3,4))) {
                            unset($productDataSLI[5]);
                            fwrite($streamSLI, implode($this->_delimiter, $productDataSLI)."\n");
                        }
                        unset($productDataCA[4]);
                        fwrite($streamCA, implode($this->_delimiter, $productDataCA)."\n");
                    } catch (Exception $e) {
                        Mage::logException($e);
                    }
                }
                $this->_processedProducts[] = $product->getId();
            }

            $curPage++;
            unset($_collection);
        }
        /**
         * unset all junk
         */
        unset($product);
        unset($productDataSLI);
        unset($productDataCA);
        unset($productCollection);
        unset($pageSize);
        unset($totalPages);
        unset($curPage);
    }

    protected function _getProductData($product)
    {
        /**
         * Fixed core issue. Store will be not set automatically when we set store filter on collection.
         */
        $store = Mage::app()->getDefaultStoreView();
        $product->setStore($store)
            ->setStoreId($store->getId());

        try {
            $imageThumbUrl = (string) Mage::helper('catalog/image')->init($product, 'thumbnail')->resize(75, 75);
        } catch (Exception $e) {
            Mage::logException($e);
            $imageThumbUrl = '';
        }
        try {
            $imageLargeUrl = (string) Mage::helper('catalog/image')->init($product, 'image');
        } catch (Exception $e) {
            Mage::logException($e);
            $imageLargeUrl = '';
        }

        return array(
                $product->getName(),
                $product->getProductUrl(false),
                str_replace(array("\n", "\r"), "", $product->getShortDescription()),
                str_replace(array("\n", "\r"), "", $product->getDescription()),
                $imageThumbUrl,
                $imageLargeUrl,
                $product->getProductNo(),
                $this->_familyLeaderFlag,
                str_replace(array("\n", "\r"), "", $product->getSearchTerms()),
                $this->_getCategoryPath($product->getCategoryIds()),
                str_replace(array("\n", "\r"), "", $product->getSpecialComments()),
                number_format($product->getFinalPrice(), 2),
                number_format($product->getQty()),
            );
    }

    protected function _getCategoryPath($categoryId)
    {
        $_categoryPath = '';
        if (!empty($categoryId)) {
            $_categoryPaths = array();

            if (!$this->_categoryModel) {
                $this->_categoryModel = Mage::getSingleton('catalog/category');
            }
            $this->_categoryModel->setData(array());

            if (is_array($categoryId)) {
                foreach ($categoryId as $key => $id) {
                    if ($id != '-') {
                        $this->_categoryModel->load($id);
                        if ($this->_categoryModel->getId() == $id) {
                            break;
                        }
                    }
                }
            } else {
                $this->_categoryModel->load($categoryId);
            }

            if ( !$this->_categoryModel->hasPathByName() ) {
                if ($_path = $this->_categoryModel->getPath()) {
                    foreach (explode('/', $_path) as $_parentId) {
                        if ( ($_parentId != 1) && ($_parentId != 2) ) {
                            $this->_categoryModel->setData(array());
                            $this->_categoryModel->load($_parentId);
                            $_categoryPaths[] = $this->_categoryModel->getName();
                        }
                    }
                } else {
                    $_categoryPaths[] = $this->_categoryModel->getName();
                }
            }
            $_categoryPath = implode(' > ', $_categoryPaths);
        }
        return $_categoryPath;
    }

    protected function _includeInPath($category)
    {
        return $category->getId() != $this->_rootNode->getId() || $this->_includeRootInPath;
    }

    protected function _initCategoriesTree()
    {
        if (!$this->_categoriesCollection) {
            $this->_categoriesTree = Mage::getResourceModel('catalog/category_tree');
            $this->_categoriesTree->load();
            $categoriesTable         = Mage::getSingleton('core/resource')->getTableName('catalog/category');
            $categoriesProductsTable = Mage::getSingleton('core/resource')->getTableName('catalog/category_product');
            $this->_categoriesTree->getCollection()
                ->getSelect()->joinLeft(array('_category_product' => $categoriesProductsTable),
                    'e.entity_id=_category_product.category_id',
                    array(
                        'product_count' => new Zend_Db_Expr('COUNT(_category_product.product_id)'),
                    ))
                ->group('e.entity_id');

            // 5-th parameter for load only active categories
            $this->_categoriesTree->addCollectionData(null, false, array(), true, true);
            $this->_categoriesCollection = $this->_categoriesTree->getCollection();
        }
    }

    /**
     * Prepare Product's thumbs in cache folder
     *
     */
    public function generateProductThumbs($width=175, $height=125)
    {
        /**
         * Saving current store and store from front end
         */
        $cronStore = Mage::app()->getStore();
        $frontStore = Mage::app()->getDefaultStoreView();

        try {
            //setting frontend store as current
            Mage::app()->setCurrentStore($frontStore);

            //$this->_initCategoriesTree();

            $productCollection = Mage::getResourceModel('catalog/product_collection')
                ->setStore($frontStore)
                ->setStoreId($frontStore->getId())
                ->addAttributeToSelect(array('image','small_image','thumbnail'))
                ->addAttributeToFilter('status', 1);

            $pageSize = 100;
            $totalPages = ceil($productCollection->getSize()/$pageSize);
            $curPage = 0;

            while ($curPage <= $totalPages) {
                $_collection = clone $productCollection;
                $_collection->setPage($curPage, $pageSize);
                foreach ( $_collection as $product ) {
                    if ( !$this->_justUniqueProducts || !in_array($product->getId(), $this->_processedProducts) ) {
                        $product->setStore($frontStore)
                            ->setStoreId($frontStore->getId());
                        (string) Mage::helper('catalog/image')->init($product, 'small_image')->resize($width, $height);
                    }
                    $this->_processedProducts[] = $product->getId();
                }

                $curPage++;
                unset($_collection);
            }

            /**
             * unset all junk
             */
            unset($product);
            unset($productCollection);
            unset($pageSize);
            unset($totalPages);
            unset($curPage);

            //reverting current store to cron store
            Mage::app()->setCurrentStore($cronStore);
        } catch (Exception $e) {
            //in case of exceptions revert store and throw exception to next warrior
            Mage::app()->setCurrentStore($cronStore);
            throw $e;
        }
    }
}
