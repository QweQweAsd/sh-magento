<?php
class Magestore_Banner_Block_Adminhtml_Client extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_client';
    $this->_blockGroup = 'banner';
    $this->_headerText = Mage::helper('banner')->__('Client Manager');
    $this->_addButtonLabel = Mage::helper('banner')->__('Add Client');
    parent::__construct();
  }
}