<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category    Sphill
 * @package     Sphill_Catalog
 * @copyright   Copyright (c) 2009 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

class Sphill_Customization_Model_Observer
{

    const XML_PATH_SLI_FTP_SERVER = 'slifeed/ftp/server';
    const XML_PATH_SLI_FTP_PORT  = 'slifeed/ftp/port';
    const XML_PATH_SLI_FTP_USER  = 'slifeed/ftp/user';
    const XML_PATH_SLI_FTP_PASSWORD = 'slifeed/ftp/password';
    const XML_PATH_SLI_FTP_SSL = 'slifeed/ftp/ssl';
    const XML_PATH_SLI_FTP_PASV = 'slifeed/ftp/pasv';
    const CRON_PRODUCT_EXPORT_JOB_CODE = 'generate_product_export_file_id_';

    /**
     * Cron job for SLI generation
     *
     */
    public function generateSliFeed()
    {
        $filenames = (array) Mage::getSingleton('sphill_customization/slifeed')->generateFeeds();
        foreach ($filenames as $filename) {
            try {
                $this->_uploadSliFeed($filename);
            } catch (Exception $e) {
                Mage::logException($e);
            }
        }
    }

    protected function _uploadSliFeed($filename)
    {
        $server = Mage::getStoreConfig(self::XML_PATH_SLI_FTP_SERVER);
        $user = Mage::getStoreConfig(self::XML_PATH_SLI_FTP_USER);

        if ($server && $user) {
            $fp = fopen($filename, 'r');

            $sslFlag = Mage::getStoreConfig(self::XML_PATH_SLI_FTP_SSL);
            $password = Mage::getStoreConfig(self::XML_PATH_SLI_FTP_PASSWORD);

            $port = Mage::getStoreConfig(self::XML_PATH_SLI_FTP_PORT);
            if (!$port) {
                $port = 21;
            }

            if ($sslFlag) {
                $conn = ftp_ssl_connect($server, $port);
            } else {
                $conn = ftp_connect($server, $port);
            }
            if(!$conn)
            {
                throw new Exception('SLI Feed FTP UPload - can not connect to server '.$server.':'.$port);
            }
            $result = ftp_login($conn, $user, $password);

            if (Mage::getStoreConfig(self::XML_PATH_SLI_FTP_PASV)) {
                ftp_pasv($conn, true);
            } else {
                ftp_pasv($conn, false);
            }

            if (!$result) {
                throw new Exception('SLI Feed FTP UPload - Login failure');
            }

            if (!ftp_fput($conn, basename($filename), $fp, FTP_ASCII)) {
                throw new Exception('There was a problem while uploading ' . $filename);
            }

            ftp_close($conn);
            fclose($fp);
        }
    }

    public function productExport()
    {
        if (func_num_args() > 0) {
            $schedule = func_get_arg(0);

            $jobCode = $schedule->getJobCode();

            $profileId = str_replace(self::CRON_PRODUCT_EXPORT_JOB_CODE, '', $jobCode);

            if ($profileId) {
                $profile = Mage::getModel('dataflow/profile');
                /** $profile Mage_Dataflow_Model_Profile */

                $profile->load($profileId);

                if (!$profile->getId()) {
                    echo "The profile you are trying to save no longer exists\n";
                    return false;
                }

                $profile->run();
                //foreach ($profile->getExceptions() as $e) {
                //    echo "Level: {$e->getLevel()}<br />\n";
                //    echo "Message: {$e->getMessage()}<br />\n";
                //}
                $profile->unsetData();
                unset($profile);
            }
        }
    }

    /**
     * Prepare Product's thumbs
     *
     */
    public function generateProductThumbs()
    {
        $width  = 175;
        $height = 125;
        Mage::getSingleton('sphill_customization/slifeed')->generateProductThumbs($width, $height);
    }

    /**
     * Remove old files from var/export magento directory
     */
    public function clearVarExportDirectory()
    {
        Mage::helper('sphill_customization')->clearDirectory(BP . DS . 'var' . DS . 'export', "-1 month");
        return true;
    }

    /**
     * Remove old files from var/export magento directory
     */
    public function clearVarTmpDirectory()
    {
        Mage::helper('sphill_customization')->clearDirectory(BP . DS . 'var' . DS . 'tmp', "-1 month");
        return true;
    }
}
