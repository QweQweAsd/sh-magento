<?php
	
class Springhills_Freecatalog_Block_Adminhtml_Freecatalog_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
		public function __construct()
		{

				parent::__construct();
				$this->_objectId = "freecatalog_id";
				$this->_blockGroup = "freecatalog";
				$this->_controller = "adminhtml_freecatalog";
				$this->_updateButton("save", "label", Mage::helper("freecatalog")->__("Save Item"));
				$this->_updateButton("delete", "label", Mage::helper("freecatalog")->__("Delete Item"));

				$this->_addButton("saveandcontinue", array(
					"label"     => Mage::helper("freecatalog")->__("Save And Continue Edit"),
					"onclick"   => "saveAndContinueEdit()",
					"class"     => "save",
				), -100);



				$this->_formScripts[] = "

							function saveAndContinueEdit(){
								editForm.submit($('edit_form').action+'back/edit/');
							}
						";
		}

		public function getHeaderText()
		{
				if( Mage::registry("freecatalog_data") && Mage::registry("freecatalog_data")->getId() ){

				    return Mage::helper("freecatalog")->__("Edit Catalog Request", $this->getFirstname());

				} 
				else{

				     return Mage::helper("freecatalog")->__("Add Item");

				}
		}
}
