<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category    Sphill
 * @package     Sphill_Customization
 * @copyright   Copyright (c) 2009 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

require_once 'app/code/core/Mage/Checkout/controllers/CartController.php';

class Sphill_Customization_CartController extends Mage_Checkout_CartController
{

    public function printAction()
    {
        Mage::setIsDeveloperMode(true);

        $cart = $this->_getCart();
        if ($cart->getQuote()->getItemsCount() < 1) {
            Mage::getSingleton('checkout/session')->addError('Cart is empty, nothing to print.');
            $this->_redirect('checkout/cart/');
        }

        $pdf = Mage::getModel('sphill_customization/cart_pdf')->getPdf();
        return $this->_prepareDownloadResponse('current_cart.pdf', $pdf->render(), 'application/pdf');
    }

    protected function _prepareDownloadResponse($fileName, $content, $contentType = 'application/octet-stream', $contentLength = null)
    {
        $this->getResponse()
            ->setHttpResponseCode(200)
            ->setHeader('Pragma', 'public', true)
            ->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true)
            ->setHeader('Content-type', $contentType, true)
            ->setHeader('Content-Length', is_null($contentLength) ? strlen($content) : $contentLength)
            ->setHeader('Content-Disposition', 'attachment; filename=' . $fileName)
            ->setHeader('Last-Modified', date('r'));
        if (!is_null($content)) {
            $this->getResponse()->setBody($content);
        }
        return $this;
    }


    public function emailAction()
    {
        $cart = $this->_getCart();
        if ($cart->getQuote()->getItemsCount() < 1) {
            Mage::getSingleton('checkout/session')->addError('Cart is empty, nothing to send.');
            $this->_redirect('checkout/cart/');
        }

        Mage::register('backUrl', $this->_getRefererUrl(), true);

        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('catalog/session');
        $this->getLayout()->getBlock('head')->setTitle($this->__('Email Cart'));
        $this->renderLayout();
    }

    public function emailPostAction()
    {
        if (!$this->_validateFormKey()) {
            return $this->_redirect('checkout/cart/email');
        }

        $emails = explode(',', $this->getRequest()->getPost('emails'));
        $message= nl2br(htmlspecialchars((string) $this->getRequest()->getPost('message')));
        $error  = false;
        if (empty($emails)) {
            $error = $this->__('Email address can\'t be empty.');
        }
        else {
            foreach ($emails as $index => $email) {
                $email = trim($email);
                if (!Zend_Validate::is($email, 'EmailAddress')) {
                    $error = $this->__('You input not valid email address.');
                    break;
                }
                $emails[$index] = $email;
            }
        }
        if ($error) {
            Mage::getSingleton('customer/session')->addError($error);
            Mage::getSingleton('wishlist/session')->setSharingForm($this->getRequest()->getPost());
            $this->_redirect('checkout/cart/email');
            return;
        }

        $translate = Mage::getSingleton('core/translate');
        /* @var $translate Mage_Core_Model_Translate */
        $translate->setTranslateInline(false);

        try {
            $customer = Mage::getSingleton('customer/session')->getCustomer();

            $items = $this->getLayout()->createBlock('sphill_customization/cart_items')
                ->toHtml();

            $totalsBlock = $this->getLayout()->createBlock('sphill_customization/cart_totals');
            $totalsHtml = $totalsBlock->renderTotals();
            $totalsHtml .= $totalsBlock->renderTotals('footer');

            if (!$totalsHtml)

            $emails = array_unique($emails);
             $emailModel = Mage::getModel('core/email_template');

            $quoteId = Mage::getSingleton('checkout/session')->getQuoteId();
            $quoteId = base64_encode($quoteId);
            $cartLink = Mage::getUrl('checkout/cart/view').'?quote='.$quoteId;

//print_r($emails);
            foreach($emails as $email) {
// Send
//mail($email, 'My Subject', 'test test');
/*
echo $customer;
echo $items;
echo $totalsHtml.$message.$cartLink;*/
               $emailModel->sendTransactional(
                    Mage::getStoreConfig('cart/email/email_template'),
                    Mage::getStoreConfig('cart/email/email_identity'),
                    $email,
                    null,
                    array(
                        'customer'      => $customer,
                        'items'         => &$items,
                        'totals'         => &$totalsHtml,
                        'message'       => $message,
                        'viewlink'       => $cartLink,
                    ));
//die('ssssss');
            }

            $translate->setTranslateInline(true);

            Mage::getSingleton('checkout/session')->addSuccess(
                $this->__('Your Shopping Cart was successfully shared')
            );
            $this->_redirect('checkout/cart/');
        }
        catch (Exception $e) {
            $translate->setTranslateInline(true);

            Mage::getSingleton('customer/session')->addError($e->getMessage());
            Mage::getSingleton('wishlist/session')->setSharingForm($this->getRequest()->getPost());
            $this->_redirect('checkout/cart/email');
        }
    }

    public function viewAction() {
        $quoteId = $this->getRequest()->getParam('quote');
        if ($quoteId) {
            $quoteId = base64_decode($quoteId);
            Mage::getSingleton('checkout/session')->setQuoteId($quoteId);
        }
        $this->_redirect('checkout/cart/');
        return $this;
    }

}
