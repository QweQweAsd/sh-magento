<?php

class Magestore_Banner_Model_Category extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('banner/category');
    }
	
	public function getOptions()
	{
		$options = array();
		$categoryCollection = $this->getCollection();
		
		foreach($categoryCollection as $category)
		{
			$option = array();
			$option['label'] = $category->getName();
			$option['value'] = $category->getId();
			$options[] = $option;
		}
		
		return $options;
	}
}