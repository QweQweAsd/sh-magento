<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category    Tem
 * @package     Tem_AbandonedCart
 * @copyright   Copyright (c) 2009 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

class Tem_AbandonedCart_Model_Observer
{
    /**
     * Remove item from cart, send request to remove item from TEM
     */
    public function removeItem($observer)
    {
        $tem = Mage::getModel('tem_abandonedcart/tem');
        if ($tem->isEnabled()){
            $quoteItem = $observer->getEvent()->getQuoteItem();
            $tem->sendRemoveItem($quoteItem);
        }
    }

    /**
     * Send to TEM all items in cart
     */
    public function sendAllItems($observer)
    {
        $tem = Mage::getModel('tem_abandonedcart/tem');
        if ($tem->isEnabled()){
            $tem->sendAllItems();
        }
    }

    /**
     * Set state to ViewCart
     */
    public function setStateViewCart($observer)
    {
        $tem = Mage::getModel('tem_abandonedcart/tem');
        if ($tem->isEnabled()){
            $tem->sendState('ViewCart', true)->sendAllItems();
        }
    }

    /**
     * Set state to Welcome
     */
    public function setStateWelcome($observer)
    {
        $tem = Mage::getModel('tem_abandonedcart/tem');
        if ($tem->isEnabled()){
            $tem->sendState('Welcome');
        }
    }

    /**
     * Set state to Billing
     */
    public function setStateBilling($observer)
    {
        $tem = Mage::getModel('tem_abandonedcart/tem');
        if ($tem->isEnabled()){
            $tem->sendState('Billing', true)->sendAllItems();
        }
    }

    /**
     * Set state to Shipping
     */
    public function setStateShipping($observer)
    {
        $tem = Mage::getModel('tem_abandonedcart/tem');
        if ($tem->isEnabled()){
            $tem->sendState('Shipping');
        }
    }

    /**
     * Set state to ShippingMethod
     */
    public function setStateShippingMethod($observer)
    {
        $tem = Mage::getModel('tem_abandonedcart/tem');
        if ($tem->isEnabled()){
            $tem->sendState('ShippingMethod');
        }
    }


    /**
     * Set state to Payment
     */
    public function setStatePayment($observer)
    {
        $tem = Mage::getModel('tem_abandonedcart/tem');
        if ($tem->isEnabled()){
            $tem->sendState('Payment');
        }
    }

    /**
     * Set state to Review
     */
    public function setStateReview($observer)
    {
        $tem = Mage::getModel('tem_abandonedcart/tem');
        if ($tem->isEnabled()){
            $tem->sendState('Review');
        }
    }

    /**
     * Set state to OrderComplete
     */
    public function setStateOrderComplete($observer)
    {
        $order = $observer->getEvent()->getOrder();
        $tem = Mage::getModel('tem_abandonedcart/tem');
        if ($tem->isEnabled() && $order->getId()){
            $tem->setOrder($order)->sendState('OrderComplete');
        }
    }

    public function quoteMergeAfter($observer)
    {
        $quote = $observer->getEvent()->getQuote();
        Mage::getModel('tem_abandonedcart/tem')->quoteMergeAfter($quote);
    }
}