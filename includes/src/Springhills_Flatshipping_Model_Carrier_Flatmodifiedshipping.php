<?php
class Springhills_Flatshipping_Model_Carrier_Flatmodifiedshipping  extends Mage_Shipping_Model_Carrier_Abstract 
{
    /* Use group alias */
    protected $_code = 'flatmodifiedshipping';
	
 
    public function collectRates(Mage_Shipping_Model_Rate_Request $request)
    {

        // skip if not enabled
        if (!Mage::getStoreConfig('carriers/'.$this->_code.'/active'))
           return false;
        
        $result = Mage::getModel('shipping/rate_result');
		$write = Mage::getSingleton('core/resource')->getConnection('core_write');	
	
		$quoteid=Mage::getSingleton("checkout/session")->getQuote()->getId(); // Current Quote Id
		$sales = Mage::getModel('sales/quote')->load($quoteid);
		$cartBaseSubtotal = $sales->getBaseSubtotal(); // Cart Subtotal

		// Need this to check whether to use offer_code of 4 or other offer code
		$coupon_code = $write->fetchOne("select coupon_code from sales_flat_quote  where entity_id='".$quoteid."' ");

		if(!empty($coupon_code) ){
			// getting the rule id will be used for fetching offer_code
			$rule_id = $write->fetchOne("select rule_id from salesrule_coupon  where code='".$coupon_code."' ");
			$offer_code = $write->fetchOne("select offer_code from salesrule  where rule_id='".$rule_id."' ");

			// Getting Shipping Amount on the basis of order base total and offer code
			$shipping_amount = $write->fetchOne("select Shipping from ShippingTables  where Order_Low <= ".$cartBaseSubtotal." AND Order_High >= ".$cartBaseSubtotal."  and Offer_Code='".$offer_code."' ");
			
			$method = Mage::getModel('shipping/rate_result_method');
			$method->setCarrier($this->_code);
			$method->setCarrierTitle(Mage::getStoreConfig('carriers/'.$this->_code.'/title'));
			/* Use method name */
			$method->setMethod('flatmodifiedshipping');
			$method->setMethodTitle('Flat Shipping');
			$method->setCost(0);
			$method->setPrice($shipping_amount);
			$result->append($method);
			return $result;	

		}else{

			// 
			$shipping_amount = $write->fetchOne("select Shipping from ShippingTables  where Order_Low <= ".$cartBaseSubtotal." AND Order_High >= ".$cartBaseSubtotal."  and Offer_Code='4' ");
			
			$method = Mage::getModel('shipping/rate_result_method');
			$method->setCarrier($this->_code);
			$method->setCarrierTitle(Mage::getStoreConfig('carriers/'.$this->_code.'/title'));
			/* Use method name */
			$method->setMethod('flatmodifiedshipping');
			$method->setMethodTitle('Flat Shipping');
			$method->setCost(0);
			$method->setPrice($shipping_amount);
			$result->append($method);
			return $result;	
		}
	
    }
	public function getAllowedMethods()
    {
        return array('flatmodifiedshipping'=>$this->getConfigData('name'));
    }
}
