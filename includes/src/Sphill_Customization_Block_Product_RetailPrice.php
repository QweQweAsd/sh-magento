<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category    Sphill
 * @package     Sphill_Customization
 * @copyright   Copyright (c) 2009 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

class Sphill_Customization_Block_Product_RetailPrice extends Mage_Core_Block_Template
{
    protected function _toHtml()
    {
        if ($this->_getData('sku')) {
            $_product = Mage::getModel('catalog/product')->loadByAttribute('sku', $this->_getData('sku'));
            if ($_product) {
                $_retailPrice = $_product->getData('suggested_retail_price');
                if (!is_null($_retailPrice)) {
                    $_prefix = $this->_getData('prefix') ? $this->_getData('prefix') : '';
                    $_sufix  = $this->_getData('sufix') ? $this->_getData('sufix') : '';
                    return $_prefix . Mage::app()->getStore()->formatPrice($_retailPrice) . $_sufix;
                }
            }
        }
        return '';
    }
}
