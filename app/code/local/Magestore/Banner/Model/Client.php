<?php

class Magestore_Banner_Model_Client extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('banner/client');
    }
	
	public function getOptions()
	{
		$options = array();
		$clientCollection = $this->getCollection();
		
		foreach($clientCollection as $client)
		{
			$option = array();
			$option['label'] = $client->getName();
			$option['value'] = $client->getId();
			$options[] = $option;
		}
		
		return $options;
	}
}