<?php
class Magestore_Banner_Block_Default extends Mage_Core_Block_Template
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
    
     public function getBanner()     
     { 
        /*if (!$this->hasData('banner')) {
		            $this->setData('banner', Mage::registry('banner'));
		        }
		        return $this->getData('banner');*/
    }
	
	public function setBannerBlock($alias)
	{
		$this->setData("alias",$alias);		
	}
	
	public function getBlockData()
	{	
		/*if(!Mage::helper('magenotification')->checkLicenseKey('Banner')){
			return array('block'=>array(),'banners'=>array());
		}*/
		if (!$this->hasData('block_data')){ 
			$alias = $this->getData("alias");
			
			$banner_block = Mage::getSingleton("banner/block")->load($alias,'alias');
			$block_data = $banner_block->getData();
			$banners = Mage::getResourceModel("banner/banner")->getListBannerOfBlock($block_data);
			
			$result = array();
			$result['block'] = $block_data;
			$result['banners'] = $banners;
			
			$this->setData('block_data',$result);
		}
		return $this->getData('block_data');	
	}
	
	
	public function generateBannerCode($target,$banner)
	{
		$html = '';
		if (trim($banner['custombannercode']))
		{
			$html = $banner['custombannercode'];
		}
		else
		{
			$image_path = Mage::getBaseDir('media') . DS . 'banners' . DS . $banner['image'];
						
			if(file_exists($image_path))
			{
				$link = $this->getUrl('banner/index/click',array('id'=>$banner['id']));
				
				$width = '';
				$height = '';
				if(isset($banner['width']) && $banner['width'] > 0)
				{
					$width = ' width="' . $banner['width']. 'px" ';
				}
				
				if(isset($banner['height']) && $banner['height'] > 0)
				{
					$height = ' height="' . $banner['height'] . 'px" ';
				}
				
				$image = '<img src="'. Mage::getBaseUrl('media') . 'banners/' . $banner['image'] . '" '. $width . $height .' alt="'. Mage::helper('banner')->__('Banner').'" style="padding-bottom:10px" />';
				
				// 	Text Banner Conditions Start

				$width=$banner['width'];
				$height=$banner['height']; 
				$text=$banner['image_text'];
				$link_cond=$banner['link_cond'];
				$twidth=$banner['text_width'];//"300";
				$theight=$banner['text_height']; //"20";
				$fontsize=$banner['font_size']."px";
				$font_weight=$banner['font_weight'];
				$font_color="#".$banner['color'];
				$font_family="arial";
				$image_align=$banner['image_position'];
				$cursor='';
				if($link_cond==2){
				$cursor = "cursor:pointer; ";
				}
				$default_style=$cursor."font-weight:".$font_weight."; font-size:".$fontsize."; color: ".$font_color."; font-family: ".$font_family."!important; ";
				$text_length="height:".$theight."px;width:".$twidth."px;";
				$bottom_left=$height-$theight;
				$bottom_right=$width-$twidth;
				$bottom_center=($width/2)-($twidth/2);

				$center=($height/2)-($theight/2);
				$left=$width-$twidth;
				$banner_center=$height-$theight;
				$style='';
				// Top Left
				if($image_align=="1"){
					$style=$default_style."position: relative; left: 20px; top: ".$theight."px;".$text_length;
				}

				// Top Right
				if($image_align=="2"){
					$style=$default_style."position: relative; left: 20px; top: ".$theight."px;".$text_length." left:".$bottom_right."px;";
				}

				// Top Center
				if($image_align=="3"){ 
					$style=$default_style."position: relative; left: 20px; top: ".$theight."px;".$text_length." left:".$bottom_center."px";
				}

				// Left
				if($image_align=="4"){
					$style=$default_style."position: relative;  top: ".$center."px;".$text_length;
				}

				// Right
				if($image_align=="5"){
					$style=$default_style."position: relative;  top: ".$center."px;".$text_length." left:".$left."px";
				}	

				// Center
				if($image_align=="6"){
					$style=$default_style."position: relative;  top: ".$center."px;".$text_length." left:".$bottom_center."px";
				}

				// Bottom Left 
				if($image_align=="7"){
					$style=$default_style."position: relative; left:".($twidth/4)."px; top: ".$bottom_left."px;".$text_length;
				}

				// Bottom Right
				if($image_align=="8"){
					$style=$default_style."position: relative;  top: ".$bottom_left."px;".$text_length." left:".($bottom_right+$theight)."px";
				}
		
				// Bottom Center
				if($image_align=="9"){ 
					$style=$default_style."position: relative;  top: ".$bottom_left."px;".$text_length." left:".$bottom_center."px";
				}

				

				
							

				// Text Banner Conditions End 
					
				if($banner['clickurl'])
				{
					$a = '';
					switch ($target)
					{
						// cases are slightly different
						case 0:
							// open in a new window
							 $_cursor="";
							if($link_cond==2)  $_cursor='style="cursor:default;"' ;
							//$a = '<a href="'. $link .'" target="_blank" '.$_cursor.'>';
							if($link_cond==1){
							 	$linkStr = $link; 
								$targetStr  = '';
							}else{ 
								$linkStr =  'javascript:void(0);';
								$targetStr  = '';
							}
							$a = '<a href="'.$linkStr.'" '.$targetStr.' '.$_cursor.'  >';
							break;
							break;

						case 2:
							// open in a popup window
							$a = "<a href=\"javascript:void window.open('". $link ."', '', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=780,height=550'); return false\">";
							break;

						default:	// formerly case 2
							// open in parent window
							$a = '<a href="'. $link .'">';
							break;
					}
					 if($link_cond==2){ $onclick= 'onClick="window.open(\''.$link.'\')"';}
					$html.="<div style='margin-top:10px;".$style."' ".$onclick."  >".$text."</div>";

					$html .= $a . $image . '</a>';
				}
				else
				{
					$html .= $image;
				}
			}
		
		}
		$html = $html.'<div style="margin-top:5px;"></div>';
		return $html;		
	}
}
