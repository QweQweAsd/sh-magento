<?php

class Magestore_Banner_Block_Adminhtml_Customerattribute_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

  public function __construct()
  {
      parent::__construct();
      $this->setId('customerattribute_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('banner')->__('Attribute Information'));
  }

  protected function _beforeToHtml()
  {
      $this->addTab('form_section', array(
          'label'     => Mage::helper('banner')->__('Attribute Information'),
          'title'     => Mage::helper('banner')->__('Attribute Information'),
          'content'   => $this->getLayout()->createBlock('banner/adminhtml_customerattribute_edit_tab_form')->toHtml(),
      ));
     
      return parent::_beforeToHtml();
  }
}
