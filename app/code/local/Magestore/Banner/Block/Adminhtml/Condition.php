<?php
class Magestore_Banner_Block_Adminhtml_Condition extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_condition';
    $this->_blockGroup = 'banner';
    $this->_headerText = Mage::helper('banner')->__('Condition Manager');
    $this->_addButtonLabel = Mage::helper('banner')->__('Add Condition Information');
	$id     = $this->getRequest()->getParam('banner_id');

	$this->_addButton('button_id', array(
            'label'     => Mage::helper('banner')->__('Add Condition'),
            'onclick'   => 'setLocation(\'' . $this->getUrl('banner/adminhtml_condition/new/banner_id/'.$id) . '\')',
            'class'     => 'go'
        ), 0, 100, 'header', 'header');

	$this->_addButton('back', array(
            'label'   => Mage::helper('adminhtml')->__('Back to Banner'),
            'onclick' => 'setLocation(\'' . Mage::helper('adminhtml')->getUrl('banner/adminhtml_banner/') . '\')',
            'class'   => 'back',
            'level'   => 1
    	));

	
    parent::__construct();

	
	
	$this->removeButton('add'); 
	
  }
}
