<?php
$installer = $this;

$installer->startSetup();

$installer->run("

ALTER TABLE {$this->getTable('bannerblock')}
	ADD `block_position` varchar(255) ;
	
ALTER TABLE {$this->getTable('bannerclient')}
	ADD `customer_id` int(10) unsigned NULL ;
");

$installer->endSetup(); 