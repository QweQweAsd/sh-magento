<?php
$installer = $this;
$installer->startSetup();
$installer->run("CREATE TABLE IF NOT EXISTS `freecatalog` (
  `freecatalog_id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(60) NOT NULL,
  `lastname` varchar(60) NOT NULL,
  `company` varchar(60) NOT NULL,
  `address1` text NOT NULL,
  `address2` text NOT NULL,
  `city` int(60) NOT NULL,
  `state` varchar(60) NOT NULL,
  `zipcode` varchar(20) NOT NULL,
  `country` varchar(60) NOT NULL,
  `email` varchar(60) NOT NULL,
  `phone` int(20) NOT NULL,
  `mobile` int(20) NOT NULL,
  PRIMARY KEY (`freecatalog_id`)
);
   
		");
//demo 
Mage::getModel('core/url_rewrite')->setId(null);
//demo 
$installer->endSetup();
	 
