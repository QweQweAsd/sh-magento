<?php
class Springhills_Freecatalog_Adminhtml_FreecatalogbackendController extends Mage_Adminhtml_Controller_Action
{
	public function indexAction()
    {
       $this->loadLayout();
	   $this->_title($this->__("Free Catalog"));
	   $this->renderLayout();
    }
}
