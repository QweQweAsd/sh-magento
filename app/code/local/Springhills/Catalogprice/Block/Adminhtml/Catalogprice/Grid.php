<?php

class Springhills_Catalogprice_Block_Adminhtml_Catalogprice_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
				parent::__construct();
				$this->setId("catalogpriceGrid");
				$this->setDefaultSort("catalogprice_id");
				$this->setDefaultDir("DESC");
				$this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection()
		{
				$collection = Mage::getModel("catalogprice/catalogprice")->getCollection();
				$this->setCollection($collection);
				return parent::_prepareCollection();
		}
		protected function _prepareColumns()
		{
				$this->addColumn("catalogprice_id", array(
				"header" => Mage::helper("catalogprice")->__("ID"),
				"align" =>"right",
				"width" => "50px",
				"index" => "catalogprice_id",
				));
				$this->addColumn("Sku", array(
				"header" => Mage::helper("catalogprice")->__("Product Number"),
				"align" =>"left",
				"index" => "Product_Number",
				));

				$this->addColumn("Offer_Code", array(
				"header" => Mage::helper("catalogprice")->__("Offer Code"),
				"align" =>"left",
				"index" => "Offer_Code",
				));

				$this->addColumn("Qty_Low", array(
				"header" => Mage::helper("catalogprice")->__("Qty Low"),
				"align" =>"left",
				"index" => "Qty_Low",
				));


				$this->addColumn("Qty_High", array(
				"header" => Mage::helper("catalogprice")->__("Qty High"),
				"align" =>"left",
				"index" => "Qty_High",
				));
                $this->addColumn("Price", array(
                    "header" => Mage::helper("catalogprice")->__("Price"),
                    "align" =>"left",
                    "index" => "Price",
                ));
                $this->addColumn("Sale_Price", array(
                    "header" => Mage::helper("catalogprice")->__("Sale Price"),
                    "align" =>"left",
                    "index" => "Sale_Price",
                ));

				return parent::_prepareColumns();
		}

		public function getRowUrl($row)
		{
			   return $this->getUrl("*/*/edit", array("id" => $row->getId()));
		}

}
