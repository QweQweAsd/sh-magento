<?php

class Springhills_Myorder_IndexController extends Mage_Core_Controller_Front_Action
{
         public function indexAction()
    	{
		$coreSession    = Mage::getSingleton('core/session');
		$arrySession    = $coreSession->getData();
		// if customer is set in the session then view 
		if( !empty($arrySession['cust_number'])){
			$this->loadLayout();   
			$this->getLayout()->getBlock("head")->setTitle($this->__("My Order"));
			$breadcrumbs = $this->getLayout()->getBlock("breadcrumbs");
			$breadcrumbs->addCrumb("home", array(
					"label" => $this->__("Home"),
					"title" => $this->__("Home"),
					"link"  => Mage::getBaseUrl()
			));
			$breadcrumbs->addCrumb("MY Order", array(
					"label" => $this->__("My Order"),
					"title" => $this->__("My Order")
			));
			$this->renderLayout();
		}else{ 
			$coreSession->addError('You are not authenticated to view this page');
			$this->_redirectReferer();
		}
		
	}

	public function orderAction()
        {
		$coreSession    = Mage::getSingleton('core/session');
		$arrySession    = $coreSession->getData();
		// if customer is set in the session then view 
		if( !empty($arrySession['cust_number'])){
			$this->loadLayout();   
			$orderId =$this->getRequest()->getParam('orderId',false);	
			$this->getLayout()->getBlock("head")->setTitle($this->__("Order : #".$orderId));		
			$breadcrumbs = $this->getLayout()->getBlock("breadcrumbs");
			$breadcrumbs->addCrumb("home", array(
					"label" => $this->__("Home"),
					"title" => $this->__("Home"),
					"link"  => Mage::getBaseUrl()
			));
			$breadcrumbs->addCrumb("MY Order", array(
					"label" => $this->__("Order Detail"),
					"title" => $this->__("Order Detail")
			));
			$this->renderLayout(); 
		}else{
			$coreSession->addError('You are not authenticated to view this page');
			$this->_redirectReferer();
		}
	}
}

?>
