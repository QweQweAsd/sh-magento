<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition License
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magentocommerce.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Page
 * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://www.magentocommerce.com/license/enterprise-edition
 */

/**
 * Top menu block
 *
 * @category    Mage
 * @package     Mage_Page
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Springhills_Page_Block_Html_Topmenu extends Mage_Page_Block_Html_Topmenu
{


    /**
     * Recursively generates top menu html from data that is specified in $menuTree
     *
     * @param Varien_Data_Tree_Node $menuTree
     * @param string $childrenWrapClass
     * @return string
     */
    protected function _getHtml(Varien_Data_Tree_Node $menuTree, $childrenWrapClass)
    {
        $html = '';

        $children = $menuTree->getChildren();
        $parentLevel = $menuTree->getLevel();
        $childLevel = is_null($parentLevel) ? 0 : $parentLevel + 1;

        $counter = 1;
        $childrenCount = $children->count();

        $parentPositionClass = $menuTree->getPositionClass();
        $itemPositionClassPrefix = $parentPositionClass ? $parentPositionClass . '-' : 'nav-';

        foreach ($children as $child) {

            $child->setLevel($childLevel);
            $child->setIsFirst($counter == 1);
            $child->setIsLast($counter == $childrenCount);
            $child->setPositionClass($itemPositionClassPrefix . $counter);

            $outermostClassCode = '';
            $outermostClass = $menuTree->getOutermostClass();

            if ($childLevel == 0 && $outermostClass) {
                $outermostClassCode = ' class="' . $outermostClass . '" ';
                $child->setClass($outermostClass);
            }
			$catArr= explode("-",$child->getId());
			$_category = Mage::getModel('catalog/category')->load($catArr['2']);

			if($_category->getIsFeatured()){
		        $html .= '<li ' . $this->_getRenderedMenuItemAttributes($child) . '>';
		        $html .= '<a href="' . $child->getUrl() . '" ' . $outermostClassCode . '>'."<img src='".$_category->getImageUrl()."' title='".$this->escapeHtml($child->getName()) ."' border='0' />".'<span>'.$this->escapeHtml($child->getName()) . '</span></a>';

		        if ($child->hasChildren()) {
		            if (!empty($childrenWrapClass)) {
		                $html .= '<div class="' . $childrenWrapClass . '">';
		            }
		            $html .= '<ul class="level' . $childLevel . '">';
		            $html .= $this->_getHtml($child, $childrenWrapClass);
		            $html .= '</ul>';

		            if (!empty($childrenWrapClass)) {
		                $html .= '</div>';
		            }
		        }
			
            	$html .= '</li>';
			}else{
				$html .= '</li>';
			}

            $counter++;
        }

        return $html;
    }

}
