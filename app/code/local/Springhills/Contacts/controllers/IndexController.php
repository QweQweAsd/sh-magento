<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category    Curvecommerce
 * @package     Curvecommerce_Contacts
 * @copyright   Copyright (c) 2011 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

require_once 'app/code/core/Mage/Contacts/controllers/IndexController.php';

class Springhills_Contacts_IndexController extends Mage_Contacts_IndexController
{
    public function postAction()
    { 

        $post = $this->getRequest()->getPost();
		
		$session_id=session_id();
		$_SESSION[$session_id]['formdata']=array();
		
        if ($post) {
            $translate = Mage::getSingleton('core/translate');
            /* @var $translate Mage_Core_Model_Translate */
            $translate->setTranslateInline(false);
            try {

			//if($post['captcha_code']==$post['randnum']) {
            		//	$translate->setTranslateInline(true);
		    //} else {
   				//Mage::getSingleton('customer/session')->addError(Mage::helper('contacts')->__('The security code is Incorrect, Please try again.'));
				//$this->_redirect('*/*/');	
				//array_push($_SESSION[$session_id]['formdata'],$post);
			//return;
			//die('The code you entered was incorrect.  Go back and try again.');
			//}
                $postObject = new Varien_Object();
                $postObject->setData($post);

                $error = false;

                if (!Zend_Validate::is(trim($post['name']) , 'NotEmpty')) {
                    $error = true;
                }

                if (!Zend_Validate::is(trim($post['comment']) , 'NotEmpty')) {
                    $error = true;
                }

                if (!Zend_Validate::is(trim($post['email']), 'EmailAddress')) {
                    $error = true;
                }

                if (Zend_Validate::is(trim($post['hideit']), 'NotEmpty')) {
                    $error = true;
                }

                if ($error) {
                    throw new Exception();
                }
                $mailTemplate = Mage::getModel('core/email_template');
                /* @var $mailTemplate Mage_Core_Model_Email_Template */
                //Newsletter Redback Call
                $write = Mage::getSingleton("core/resource")->getConnection("core_write");
                $count = $write->fetchOne("SELECT count(*) as cnt from  newsletter_subscriber where subscriber_email='".$post['email']."' ");
                if($count==0){
                    //Mage::getModel('newsletter/subscriber')->subscribe($post['email']);
                }
                //Newletter Call
		        $sender = array('name' => $post['name'],
                    'email' => $post['email']);
                $recepientName = Mage::getStoreConfig('trans_email/ident_general/name');

                $mailTemplate->setDesignConfig(array('area' => 'frontend'))
                    ->setReplyTo($post['email'])
                    ->sendTransactional(
                        Mage::getStoreConfig(self::XML_PATH_EMAIL_TEMPLATE),
                        $sender,
                        Mage::getStoreConfig(self::XML_PATH_EMAIL_RECIPIENT),
                        $recepientName,
                        array('data' => $postObject)
                    );

                if (!$mailTemplate->getSentSuccess()) {
                    throw new Exception();
                }

                $translate->setTranslateInline(true);
                Mage::getSingleton('customer/session')->addSuccess(Mage::helper('contacts')->__('Your inquiry was submitted and will be responded as soon as possible. Thank you for contacting us.'));
                $this->_redirect('*/*/');

                return;
            } catch (Exception $e) {
                $translate->setTranslateInline(true);

                Mage::getSingleton('customer/session')->addError(Mage::helper('contacts')->__('Unable to submit your request. Please, try again later'));
                $this->_redirect('*/*/');
                return;
            }

        } else {
            $this->_redirect('*/*/');
        }
    }

}
