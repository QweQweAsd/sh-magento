<?php
/**
* Example View block
*
* @codepool   Local
* @category   Fido
* @package    Springhills_Cache
* @module     Example
*/
class Springhills_Cache_Block_Productprice extends Mage_Core_Block_Template
{
    private $message;
    private $att;
	private $productId;

    
    public function getPriceBlock() {
		$message ='';
		$array = $this->getCacheKeyInfo();
		$keyCode = Mage::getSingleton("core/session")->getData('offerCode');
		$prdId = $array['product_id'];
		if (empty($prdId))
			$prdId = $this->productId;

		$_product = Mage::getModel('catalog/product')->load($prdId);
		$pprice = $_product->getFinalPrice();
		$uom = $_product->getUnitOfMeasure();
		$prodSku = $_product->getSku();

		$offerresource = Mage::getResourceSingleton('checkout/cart');
		$message = $offerresource->getOfferPriceBlock("$prodSku", "$keyCode","$uom" );	

		return $message;
    }

	

 	public function setProductId($id) {
        $this->productId = $id;
    }

    protected function _toHtml() {
        $html = parent::_toHtml();
        return $html;
    }

	public function getCacheKeyInfo() {
		$info = parent::getCacheKeyInfo();

		if (Mage::registry('current_product'))
		    $info['product_id'] = Mage::registry('current_product')->getId();

		return $info;
	}

}
