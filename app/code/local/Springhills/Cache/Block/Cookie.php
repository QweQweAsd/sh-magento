<?php
/**
 * Example View block
 *
 * @codepool   Local
 * @category   Fido
 * @package    Springhills_Cache
 * @module     Example
 */
class Springhills_Cache_Block_Cookie extends Mage_Core_Block_Template
{
    private $message;
    private $att;
    private $productId;
    private $actionName;
    private $routeName;
    private $moduleName;
    private $controllerName;
    private $pageId;

    protected function createMessage($msg) {
        $this->message = $msg;
    }

    public function receiveMessage() {

        $moduleName = Mage::app()->getRequest()->getModuleName();
        $actionName = Mage::app()->getRequest()->getActionName();
        $routeName = Mage::app()->getRequest()->getRouteName();
        $controllerName = Mage::app()->getRequest()->getControllerName();
        $pageId = Mage::getSingleton('cms/page')->getIdentifier();

        if($routeName == 'enterprise_pagecache'){
            $action = $this->actionName ;
            $module = $this->moduleName ;
            $route  = $this->routeName ;
            $controller  = $this->controllerName ;
            $pageId= $this->pageId;
        }else{
            $action = $actionName;
            $module = $moduleName;
            $route  = $routeName;
            $controller  = $controllerName;
        }


        //echo $action . "====". $module . "====" .$controller. "=====".$route."====".$pageId ;
        $script='';
        $requestType='';
        $cookiedata=  Mage::getSingleton('core/session')->getData("cookie_type");
        $pageType=  Mage::getSingleton('core/session')->getData("pageType");
        $sessionEmail= Mage::getSingleton('core/session')->getData("email_id");

        $freecatlogFirstName = Mage::getSingleton('core/session')->getData("freecatlogFirstName");
        $keyCode=  Mage::getSingleton('core/session')->getData("offerCode");
        $keyCode=  Mage::getSingleton('core/session')->getData("offerCode");
        $custZip = Mage::getModel('core/session')->getData('cust_zip');
        $emailStr = 'email no-offer';
        $requestType = Mage::getSingleton('core/session')->getData("requestType");
        $requestEmail = Mage::getSingleton('core/session')->getData("requestEmail");
        if(!empty($keyCode)){
            $emailStr = 'email offer';
        }

        if($requestType!=null && $requestEmail!=null){

                $script ='
            <script type="text/javascript">
                // <![CDATA[
                    UCX.P("583B0677");
                    function UCX_Queue(accessKey, cookieId) {
                        var email = "'.$requestEmail.'";
                        var zip = "'.$custZip.'";
                        var inputForm = "'.$requestType .':'.$pageType.':'.$cookiedata.'";
                        var keycode = "'.$keyCode.'";
                        var list = "";
                        UCX.LogContact(accessKey,cookieId, email, zip ,inputForm, keycode, list);
                    }
                // ]]>
            </script>';

        }

        if($pageId == 'emailsignup-thankyou' && !empty($sessionEmail)){

            if($pageType=='plantfinder:search'){
                $script ='
                <script type="text/javascript">
                    // <![CDATA[
                        UCX.P("583B0677");
                        function UCX_Queue(accessKey, cookieId) {
                            var email = "'.$sessionEmail.'";
                            var zip = "'.$custZip.'";
                            var inputForm = "'.$pageType.'";
                            var keycode = "'.$keyCode.'";
                            var list = "";
                            UCX.LogContact(accessKey,cookieId, email, zip ,inputForm, keycode, list);
                        }
                    // ]]>
                </script>';
            }else{
                $script ='
                <script type="text/javascript">
                    // <![CDATA[
                        UCX.P("583B0677");
                        function UCX_Queue(accessKey, cookieId) {
                            var email = "'.$sessionEmail.'";
                            var zip = "'.$custZip.'";
                            var inputForm = "'.$emailStr.':'.$pageType.':'.$cookiedata.'";
                            var keycode = "'.$keyCode.'";
                            var list = "";
                            UCX.LogContact(accessKey,cookieId, email, zip ,inputForm, keycode, list);
                        }
                    // ]]>
                </script>';
            }
        }

        if($pageId == 'freecatalog-thankyou' ){
            $ffisrtname = Mage::getSingleton('core/session')->getData('firstname');
            $flastname = Mage::getSingleton('core/session')->getData('lastname');
            $femail = Mage::getSingleton('core/session')->getData('email_id');
            $faddress1 = Mage::getSingleton('core/session')->getData("Address");
            $faddress2 = Mage::getSingleton('core/session')->getData("Address2");
            $fcity = Mage::getSingleton('core/session')->getData("City");
            $fstate = Mage::getSingleton('core/session')->getData("State");
            $fzipcode = Mage::getSingleton('core/session')->getData("ZipCode");
            //$keyCode=  Mage::getSingleton('core/session')->getData("offerCode");
            $addrInfo = array(
                'firstname' => $ffisrtname,
                'lastname' => $flastname,
                'email' => $femail,
                'address' => $faddress1,
                'address1' => $faddress2,
                'city' => $fcity,
                'state' => $fstate,
                'zipcode' => $fzipcode);

            if(!empty($femail)){
            $script ='
            <script type="text/javascript">
                // <![CDATA[
                var ucxAccess = "583B0677";
                var inputForm = "'.$emailStr.':catalog request:center";
                var keycode = "'.$keyCode.'";
                var list = "";
                UCX.LogContact(ucxAccess, cookieId, "'.$femail.'" ,"'.$fzipcode.'",inputForm, keycode, list);
                // ]]>
            </script>';
            }
        }

        Mage::getSingleton('core/session')->setData('firstname','');
        Mage::getSingleton('core/session')->setData('lastname','');
        Mage::getSingleton('core/session')->setData('email_id','');
        Mage::getSingleton('core/session')->setData("Address",'');
        Mage::getSingleton('core/session')->setData("Address2",'');
        Mage::getSingleton('core/session')->setData("City",'');
        Mage::getSingleton('core/session')->setData("State",'');
        Mage::getSingleton('core/session')->setData("ZipCode",'');
        Mage::getSingleton('core/session')->setData("requestType",'');
        Mage::getSingleton('core/session')->setData("requestEmail",'');
        return $script;


    }

    public function setCookieId($id){
        if($id != ''){
            $this->cookieId = $id;
        }
    }

    public function setParams($moduleName,$actionName,$routeName,$controllerName,$catId,$pageId){
        if($routeName != 'enterprise_pagecache'){
            $this->actionName = $actionName;
            $this->moduleName = $moduleName;
            $this->routeName = $routeName;
            $this->controllerName = $controllerName;
            $this->catId = $catId;
            $this->pageId = $pageId;
        }
    }

    protected function _toHtml() {
        $html = parent::_toHtml();
        return $html;
    }

    public function getCacheKeyInfo() {
        $info = parent::getCacheKeyInfo();

        $moduleName = Mage::app()->getRequest()->getModuleName();
        $actionName = Mage::app()->getRequest()->getActionName();
        $routeName = Mage::app()->getRequest()->getRouteName();
        $controllerName = Mage::app()->getRequest()->getControllerName();
        $pageId = Mage::getSingleton('cms/page')->getIdentifier();
        if($routeName!='enterprise_pagecache'){
            $info['routeName'] = $routeName;
            $info['actionName'] = $actionName;
            $info['moduleName'] = $moduleName;
            $info['controllerName'] = $controllerName;
            $info['pageId'] = $pageId;

        }

        return $info;
    }

}
