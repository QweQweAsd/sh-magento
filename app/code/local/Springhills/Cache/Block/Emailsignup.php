<?php
/**
 * Example View block
 *
 * @codepool   Local
 * @category   Fido
 * @package    Springhills_Cache
 * @module     Example
 */
class Springhills_Cache_Block_Emailsignup extends Mage_Core_Block_Template
{
    private $message;
    private $att;
    private $actionName;
    private $routeName;
    private $moduleName;
    private $controllerName;
    private $pageId;

    public function getEmailSignupBlock() {
        $message = '';

        $moduleName = Mage::app()->getRequest()->getModuleName();
        $actionName = Mage::app()->getRequest()->getActionName();
        $routeName = Mage::app()->getRequest()->getRouteName();
        $controllerName = Mage::app()->getRequest()->getControllerName();
        $pageId = Mage::getSingleton('cms/page')->getIdentifier();

        if($routeName == 'enterprise_pagecache'){
            $action = $this->actionName ;
            $module = $this->moduleName ;
            $route  = $this->routeName ;
            $controller  = $this->controllerName ;
            $pageId= $this->pageId;
        }else{
            $action = $actionName;
            $module = $moduleName;
            $route  = $routeName;
            $controller  = $controllerName;
        }

        //echo $action . "====". $module . "====" .$controller. "=====".$route."====".$pageId ;
        if($controller=='index' && $route=='cms'){
            $pageType = 'homepage';
        }else if($controller=='category'){
            $pageType = 'category';
        }else if($controller=='product'){
            if($route =='review')
                $pageType = 'product review';
            else
                $pageType = 'product';

            $pageType = 'product';
        }else if($controller=='catalogsearch'){
            $pageType = 'search';
        }else if($controller=='cart'){
            $pageType = 'cart';
        }else{
            $pageType = 'article';
        }


        $bannerofferCode = Mage::getSingleton('core/session')->getData("offerCode");
        $cookieEmailId = Mage::getModel('core/cookie')->get('email_id');
        $message = '<form action="'.Mage::getBaseUrl().'newsletter/subscriber/new/" method="post" id="newsletter-validate-detail3">
		<div class="content" style="margin-top:-10px;">';
        if($cookieEmailId==null ){

            $message .= '
		<h3 class="coupons-heading">Don\'t miss Out! Sign up for our Email Deal Alerts.</h3>'.Mage::app()->getLayout()->createBlock('cms/block')->setBlockId('emailimage')->toHtml().
        '<div class="SignUpBenefits">

			<ul  style="font-size:15px;" >
			  <li>Get an Instant $25 off your $50 or more order</li>
			  <li>FREE Shipping &amp; Handling on order of $25 or more</li>
			  <li>Save Up to 75% Off your purchases</li>
			  <li>Save $50 off your $100 or more order</li>
			  <li>Save $100 off your $200 or more order</li>
			</ul> 
		</div>
	
		<div class="YesSignMeUp">  <span class="Yes">Yes! Please sign me up.</span>
			<div><span class="Sign-Up-Now">
				<input type="text" autocomplete="off" name="email" id="newsletter3" value="Enter Your Email Address" class="input-text required-entry validate-email" onfocus="if (this.value == \'Enter Your Email Address\') { this.value = \'\'; }" onblur="if (this.value == \'\') { this.value = \'Enter Your Email Address\'; }"></span>
				<input type="hidden"  name="signup-type" value="center">
				 <input type="hidden"  name="page_types" value="'.$pageType.'">
				<input type="image"  src="'.Mage::getBaseUrl().'skin/frontend/enterprise/springhills/images/sign-me-up.png" value="Go">
			</div>              
		</div>
	    <div class="clear"></div>
		<div style=" position:relative">
		<div class="emailmask"></div>
		<div class="coupons-banner">
			<img width="562" height="159" border="0" src="'.Mage::getBaseUrl().'skin/frontend/enterprise/springhills/images/coupons-banner.jpg" usemap="#Map">
			<map name="Map" id="Map">
			<area shape="rect" coords="31,86,263,115" href="#">
			</map>
		
		</div>              
	
		<div class="Coupon-Block">
			<div class="coupon"><a href="#"><img width="266" height="158" border="0" src="'.Mage::getBaseUrl().'skin/frontend/enterprise/springhills/images/free-shipping.jpg"></a>
			</div>
		    <div class="coupon"><a href="#"><img width="266" height="158" border="0" src="'.Mage::getBaseUrl().'skin/frontend/enterprise/springhills/images/75off.jpg"></a>
			</div>
		    <div class="coupon"><a href="#"><img width="266" height="158" border="0" src="'.Mage::getBaseUrl().'skin/frontend/enterprise/springhills/images/save50.jpg"></a>
			</div>
			<div class="coupon"><a href="#"><img width="266" height="158" border="0" src="'.Mage::getBaseUrl().'skin/frontend/enterprise/springhills/images/save100.jpg"></a>
			</div>
		</div> 
		</div>';

        }else{
           // echo 'else';
            echo Mage::app()->getLayout()->createBlock('cms/block')->setBlockId('emailsignupblock')->toHtml();
        }

        $message .= '
		</div>
		<div class="clear"></div>
		</form>';

        return $message;
    }

    public function setParams($moduleName,$actionName,$routeName,$controllerName){
        if($routeName != 'enterprise_pagecache'){
            $this->actionName = $actionName;
            $this->moduleName = $moduleName;
            $this->routeName = $routeName;
            $this->controllerName = $controllerName;
        }
    }


    protected function _toHtml() {
        $html = parent::_toHtml();
        return $html;
    }

    public function getCacheKeyInfo() {
        $info = parent::getCacheKeyInfo();

        $moduleName = Mage::app()->getRequest()->getModuleName();
        $actionName = Mage::app()->getRequest()->getActionName();
        $routeName = Mage::app()->getRequest()->getRouteName();
        $controllerName = Mage::app()->getRequest()->getControllerName();
        $pageId = Mage::getSingleton('cms/page')->getIdentifier();
        if($routeName!='enterprise_pagecache'){
            $info['routeName'] = $routeName;
            $info['actionName'] = $actionName;
            $info['moduleName'] = $moduleName;
            $info['controllerName'] = $controllerName;
            $info['pageId'] = $pageId;

        }

        return $info;
    }


}
