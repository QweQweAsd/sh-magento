<?php
/**
* Example View block
*
* @codepool   Local
* @category   Fido
* @package    Springhills_Cache
* @module     Example
*/
class Springhills_Cache_Block_Cart extends Mage_Core_Block_Template
{
    private $message;
    private $att;
	private $actionName;
	private $routeName;
	private $moduleName;
	private $controllerName;

    
    public function headerCart() {
        $array = $this->getCacheKeyInfo();
		$custId = $array['customer_id'];
		$message ='';
		$session = Mage::getSingleton("core/session");
		$arrySession=$session->getData();
		$moduleName = Mage::app()->getRequest()->getModuleName();
		$actionName = Mage::app()->getRequest()->getActionName();
		$routeName = Mage::app()->getRequest()->getRouteName();
		$controllerName = Mage::app()->getRequest()->getControllerName();

		if($routeName == 'enterprise_pagecache'){
			$action = $this->actionName ; 
			$module = $this->moduleName ;
			$route  = $this->routeName ;
			$controller  = $this->controllerName ;
		}else{
			$action = $actionName;
			$module = $moduleName;
			$route  = $routeName;
			$controller  = $controllerName;
		}

		if($controller !='onepage'){
			if (!empty($arrySession['cust_first_name'])){
				$message ='<div class="access cms-links"><ul class="headlogin" style="width:335px !important;">     
						<li>Hello, '.ucwords($arrySession["cust_first_name"]).'</li>
						<li><a href="'.$this->getUrl("checkout/onepage/logout").'">Logout</a></li>
						<li><a href="'.$this->getUrl("myorder").'" title="'.$this->__("View Orders").'">View / Track Orders</a></li> 				 
					</ul></div>';
			} else {

					$message = '<div class="access cms-links"><ul class="headlogin" style="width:305px !important;margin-right:-30px;"> 
								<li style="margin-left: 7%;"><a href="'.$this->getUrl("customer/account/login/").'" title="'.$this->__("View Orders").'">View / Track Orders</a></li>
							</ul></div>';  

			}
		}
		return $message;
    }

	

 	public function setParams($moduleName,$actionName,$routeName,$controllerName){
		if($routeName != 'enterprise_pagecache'){
			$this->actionName = $actionName;
			$this->moduleName = $moduleName;
			$this->routeName = $routeName;
			$this->controllerName = $controllerName;
		}
	}

    protected function _toHtml() {
        $html = parent::_toHtml();
        return $html;
    }

	public function getCacheKeyInfo() {
		$info = parent::getCacheKeyInfo();
		
		$moduleName = Mage::app()->getRequest()->getModuleName();
		$actionName = Mage::app()->getRequest()->getActionName();
		$routeName = Mage::app()->getRequest()->getRouteName();
		$controllerName = Mage::app()->getRequest()->getControllerName();
		if($routeName!='enterprise_pagecache'){
			$info['routeName'] = $routeName;
			$info['actionName'] = $actionName;
			$info['moduleName'] = $moduleName;
			$info['controllerName'] = $controllerName;
		}

		return $info;
	}

}
