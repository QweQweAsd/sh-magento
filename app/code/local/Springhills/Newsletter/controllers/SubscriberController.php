<?php
require_once 'app/code/core/Mage/Newsletter/controllers/SubscriberController.php';
class Springhills_Newsletter_SubscriberController extends Mage_Newsletter_SubscriberController
{
    /**
     * New subscription action
     */
    public function newAction()
    {

        if ($this->getRequest()->isPost() && $this->getRequest()->getPost('email')) {

            $session = Mage::getSingleton('core/session');
            $defaultKeycode = Mage::getSingleton('core/session')->getData('offerCode');
            $customerSession = Mage::getSingleton('customer/session');
            $email = (string)$this->getRequest()->getPost('email');
            try {
                if (!Zend_Validate::is($email, 'EmailAddress')) {
                    Mage::throwException($this->__('Please enter a valid email address.'));
                }

                if (Mage::getStoreConfig(Mage_Newsletter_Model_Subscriber::XML_PATH_ALLOW_GUEST_SUBSCRIBE_FLAG) != 1 &&
                    !$customerSession->isLoggedIn()
                ) {
                    Mage::throwException($this->__('Sorry, but administrator denied subscription for guests. Please <a href="%s">register</a>.', Mage::helper('customer')->getRegisterUrl()));
                }

                $resource = Mage::getResourceSingleton('newsletter/subscriber');
                $existenceCount = $resource->checkSubscriberEmail($email);
                $signupType = (string)$this->getRequest()->getPost('signup-type');
                $pageType = (string)$this->getRequest()->getPost('page_types');

                Mage::getSingleton('core/session')->setData("pageType",$pageType);
                if($signupType=="left"){
                    Mage::getSingleton('core/session')->setData("cookie_type","left");
                }else if($signupType=="bottom"){
                    Mage::getSingleton('core/session')->setData("cookie_type","bottom");

                }else if($signupType=="center"){
                    Mage::getSingleton('core/session')->setData("cookie_type","center");
                }

                if ($existenceCount > 0 && empty($defaultKeycode) && 0 ) {
                    Mage::getModel('core/cookie')->set('email_id', $email);
                    Mage::getSingleton('core/session')->setData("email_id",$email);
                    $session->addSuccess($this->__('Thank you for your subscription.'));
                    $this->_redirect('emailsignup-thankyou');
                    return;
                    //Mage::throwException($this->__('This email address is already subscribed.'));
                } else {
                    $status = Mage::getModel('newsletter/subscriber')->subscribe($email);
                    if ($this->getRequest()->getPost('name')) {
                        $subscriber = Mage::getModel('newsletter/subscriber')->loadByEmail($email);
                        $name = (string)$this->getRequest()->getPost('name');

                        $namearr = explode(' ', $name);
                        if (sizeof($namearr) > 0) {
                            $j = 0;
                            $lastname = '';
                            foreach ($namearr as $subscribername=>$val) {
                                //print_r($subscribername);
                                if ($j == 0)
                                    $firstname = $namearr[0];
                                else
                                    $lastname .= $namearr[$j]." ";
                                $j++;
                            }
                        }

                        $subscriber->setSubscriberFirstname($firstname);
                        $subscriber->setSubscriberLastname($lastname);
                        $subscriber->save();
                    }
                    //fetch the subscriber_id
                    $subscriberId = $resource->getSubscriberId($email);
                    // set cookie for email subscription
                    //echo $email;
                    Mage::getModel('core/cookie')->set('email_id', $email);
                    Mage::getSingleton('core/session')->setData("email_id",$email);
                    // Transactional Email Template's ID Use template id of template created for newsletter
                    $templateId = 3;
                    // Set sender information
                    $senderName = Mage::getStoreConfig('trans_email/ident_custom1/name');
                    $senderEmail = Mage::getStoreConfig('trans_email/ident_custom1/email');
                    // $senderName ="Amit";
                    // $senderEmail = "amits@kensium.com";
                    $sender = array('name' => $senderName,
                        'email' => $senderEmail);
                    // Set recepient information
                    $recepientEmail = $email;
                    $recepientName = $email;
                    // Get Store ID
                    $storeId = Mage::app()->getStore()->getId();
                    // Set variables that can be used in email template
                    $vars = array('customerEmail' => $email);
                    $translate = Mage::getSingleton('core/translate');
                    // Send Transactional Email
                    Mage::getModel('core/email_template')
                        ->sendTransactional($templateId, $sender, $recepientEmail, $recepientName, $vars, $storeId);
                    $translate->setTranslateInline(true);
                    //echo Mage::getModel('core/cookie')->get('email_id'); die();
                    if ($status == Mage_Newsletter_Model_Subscriber::STATUS_NOT_ACTIVE) {
                        $session->addSuccess($this->__('Confirmation request has been sent.'));
                    } else {
                        $session->addSuccess($this->__('Thank you for your subscription.'));
                        Mage::getSingleton('core/session')->setFormData(false);
                        Mage::getModel('core/cookie')->set('emailflag', "yes");
                        $this->_redirect('emailsignup-thankyou');

                        return;
                    }

                }
            } catch (Mage_Core_Exception $e) {
                $session->addException($e, $this->__('There was a problem with the subscription: %s', $e->getMessage()));
            }
            catch (Exception $e) {
                $session->addException($e, $this->__('There was a problem with the subscription.'));
            }
        }
        $this->_redirectReferer();
    }


}
