<?php

class Springhills_Solr_Model_Adapter_HttpStream extends Enterprise_Search_Model_Adapter_HttpStream {

    
    /**
     *
     * @var type Increase limit to 9999
     */
     protected $_defaultQueryParams = array(
        'offset' => 0,   
        'limit' => 9999,
        'sort_by' => array(array('score' => 'desc')),
        'store_id' => null,
        'locale_code' => null,
        'fields' => array(),
        'solr_params' => array(),
        'ignore_handler' => false,
        'filters' => array()
    );
            
    protected function prepareSearchConditions($query) {
        $query = str_replace(' ', ' AND ', str_replace(' AND ', ' ', $query));

        return parent::prepareSearchConditions($query);
    }

}
