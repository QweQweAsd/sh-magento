<?php

class Springhills_Productshipping_Adminhtml_ProductshippingController extends Mage_Adminhtml_Controller_Action
{
		protected function _initAction()
		{
				$this->loadLayout()->_setActiveMenu("productshipping/productshipping")->_addBreadcrumb(Mage::helper("adminhtml")->__("Productshipping  Manager"),Mage::helper("adminhtml")->__("Productshipping Manager"));
				return $this;
		}
		public function indexAction() 
		{
				$this->_initAction();
				$this->renderLayout();
		}
		public function editAction()
		{
				$brandsId = $this->getRequest()->getParam("id");
				$brandsModel = Mage::getModel("productshipping/productshipping")->load($brandsId);
				if ($brandsModel->getId() || $brandsId == 0) {
					Mage::register("productshipping_data", $brandsModel);
					$this->loadLayout();
					$this->_setActiveMenu("productshipping/productshipping");
					$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Productshipping Manager"), Mage::helper("adminhtml")->__("Productshipping Manager"));
					$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Productshipping Description"), Mage::helper("adminhtml")->__("Productshipping Description"));
					$this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
					$this->_addContent($this->getLayout()->createBlock("productshipping/adminhtml_productshipping_edit"))->_addLeft($this->getLayout()->createBlock("productshipping/adminhtml_productshipping_edit_tabs"));
					$this->renderLayout();
				} 
				else {
					Mage::getSingleton("adminhtml/session")->addError(Mage::helper("productshipping")->__("Item does not exist."));
					$this->_redirect("*/*/");
				}
		}

		public function newAction()
		{

        $id   = $this->getRequest()->getParam("id");
		$model  = Mage::getModel("productshipping/productshipping")->load($id);

		$data = Mage::getSingleton("adminhtml/session")->getFormData(true);
		if (!empty($data)) {
			$model->setData($data);
		}

		Mage::register("productshipping_data", $model);

		$this->loadLayout();
		$this->_setActiveMenu("productshipping/productshipping");

		$this->getLayout()->getBlock("head")->setCanLoadExtJs(true);

		$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Productshipping Manager"), Mage::helper("adminhtml")->__("Productshipping Manager"));
		$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Productshipping Description"), Mage::helper("adminhtml")->__("Productshipping Description"));


		$this->_addContent($this->getLayout()->createBlock("productshipping/adminhtml_productshipping_edit"))->_addLeft($this->getLayout()->createBlock("productshipping/adminhtml_productshipping_edit_tabs"));

		$this->renderLayout();

		       // $this->_forward("edit");
		}
		public function saveAction()
		{

			$post_data=$this->getRequest()->getPost();


				if ($post_data) {

					try {
			
						$post_data['updated_at']=date('Y-m-d H:i:s');
						$brandsModel = Mage::getModel("productshipping/productshipping")
						->addData($post_data)
						->setId($this->getRequest()->getParam("id"))
						->save();

						Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Productshipping was successfully saved"));
						Mage::getSingleton("adminhtml/session")->setProductshippingData(false);

						if ($this->getRequest()->getParam("back")) {
							$this->_redirect("*/*/edit", array("id" => $brandsModel->getId()));
							return;
						}
						$this->_redirect("*/*/");
						return;
					} 
					catch (Exception $e) {
						Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
						Mage::getSingleton("adminhtml/session")->setProductshippingData($this->getRequest()->getPost());
						$this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
					return;
					}

				}
				$this->_redirect("*/*/");
		}



		public function deleteAction()
		{
				if( $this->getRequest()->getParam("id") > 0 ) {
					try {
						$brandsModel = Mage::getModel("productshipping/productshipping");
						$brandsModel->setId($this->getRequest()->getParam("id"))->delete();
						Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item was successfully deleted"));
						$this->_redirect("*/*/");
					} 
					catch (Exception $e) {
						Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
						$this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
					}
				}
				$this->_redirect("*/*/");
		}
}
