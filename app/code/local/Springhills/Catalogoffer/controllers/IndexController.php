<?php
require_once 'redback/RedbackServices.php';
class Springhills_Catalogoffer_IndexController extends Mage_Core_Controller_Front_Action{
    public function IndexAction() {
      
	  $this->loadLayout();   
	  $this->getLayout()->getBlock("head")->setTitle($this->__("Free Catalog"));
	  $breadcrumbs = $this->getLayout()->getBlock("breadcrumbs");
      $breadcrumbs->addCrumb("home", array(
                "label" => $this->__("Home"),
                "title" => $this->__("Home"),
                "link"  => Mage::getBaseUrl()
	  ));
      $breadcrumbs->addCrumb("Free Catalog ", array(
                "label" => $this->__("Free Catalog "),
                "title" => $this->__("Free Catalog ")
	   ));
      $this->renderLayout(); 
	  
    }
     
	public function postAction()
    { 
        $post = $this->getRequest()->getPost();
        if ($post) {
            $translate = Mage::getSingleton('core/translate');
            /* @var $translate Mage_Core_Model_Translate */
            $translate->setTranslateInline(false);
            try {
                $postObject = new Varien_Object();
                $postObject->setData($post);
                $error = false;
                if (!Zend_Validate::is(trim($post['name']) , 'NotEmpty')) {
                    $error = true;
                }
                if (!Zend_Validate::is(trim($post['email']), 'EmailAddress')) {
                    $error = true;
                }
                if (Zend_Validate::is(trim($post['hideit']), 'NotEmpty')) {
                    $error = true;
                }
		$write =Mage::getSingleton('core/resource')->getConnection('core_write');
		$regionInfo = $write->FetchAll("SELECT * FROM directory_country_region WHERE default_name= '".$post['state']."' and country_id='US'");
		//REDBACK API CALL TO SEND THE EMAIL ID
			       $objRedBackApi = new RedbackServices();
				  // This is an archaic parameter list
				$data =array(
					'Title'	=>	'11',
					'CustNumber'=>'',
					'CustFirstName'=>trim($post['firstname']),
					'CustLastName'=>trim($post['lastname']),
					'CustCompany'=>trim($post['company']),
					'CustAddr1'=>trim($post['address1']),
					'CustAddr2'=>trim($post['address2']),
					'CustCity'=>trim($post['city']),
					'CustState'=>trim($regionInfo[0]['code']),
					'CustZip'=>trim($post['zipcode']),
					'CustEmail'=>trim($post['email'])	,
					'CustEmIP'=>'',
					'CustPhone'=>trim($post['phone']),
					'PromoCode'=>'',
					'ContestCode'=>'',
					'OptOutFlag'=>'',
					'CatErrCode'=>'',
					'CatErrMsg'=>'',
					'Bonus'=>'',
					'debug'=>'',
					'debug_id'=>'',
					'BirthMonth'=>'',
					'BirthDay'=>'',
					'ConfirmEmail'=>'',
					'IPaddress'=>$_SERVER["REMOTE_ADDR"],
					'TimeStamp'=>''
				 );
				
				$redbackStatus = $objRedBackApi->catalogofferService($data);
				//If Errorcode is empty or Redback dint return false 
				$iStatus=0;	               	
				if($redbackStatus)
	       				$iStatus=1;

				/* Saving values in database */
				$data['firstname']=trim($post['firstname']);
				$data['lastname']=trim($post['lastname']);
				$data['company']=trim($post['company']);
				$data['address1']=trim($post['address1']);
				$data['address2']=trim($post['address2']);
				$data['city']=trim($post['city']);
				$data['state']=trim($post['state']);
				$data['zipcode']=trim($post['zipcode']);
				$data['country']=trim($post['country']);
				$data['email']=trim($post['email']);
				$data['phone']=trim($post['phone']);
				$data['mobile']=trim($post['mobil']);
				$data['created_at']=date('Y-m-d H:i:s');
				$data['updated_at']=date('Y-m-d H:i:s');
				$data['redback_status']=$iStatus;
				$data['redback_ip']=$_SERVER["REMOTE_ADDR"];
				$model = Mage::getModel('catalogoffer/catalogoffer');
				$model->setData($data);
				$model->save();
			
				/* Sending email */
				$senderFName = $data['firstname'];
				$senderEmail = $data['email'];

				$reFName = Mage::getStoreConfig('trans_email/ident_general/name'); //fetch sender name Admin
				$reEmail = Mage::getStoreConfig('trans_email/ident_general/email'); //fetch sender email Admin

				$message = '<div style="text-align:left; font-family:arial; font-size:11px;width:500px;" >';
				$message .= 'Name:'.$data['firstname']." ". $data['lastname']."<br><br>";
				$message .= 'Email:'.$data['email']."<br><br>";
				$message .= 'Company Name:'.$data['company']."<br><br>";
				$message .= 'Zip:'.$data['zipcode']."<br><br>";
				$message .= 'Address:'.$data['address1']." ".$data['address2']."<br><br>";
				$message .= 'City:'.$data['city']."<br><br>";
				$message .= 'State:'.$data['state']."<br><br>";				
				$message .= 'Country:'.$data['country']."<br><br>";
				if($data['phone']!=''){
					$message .= 'Phone:'.$data['phone']."<br><br>";
				}
				if($data['mobile']!=''){
					$message .= 'Mobile:'.$data['mobile']."<br><br>";
				}
				$message .= '</div>';



				$message = '';
				$message = '<table style="border-left: 1px solid rgb(175, 197, 168); border-right: 1px solid rgb(175, 197, 168); border-top: 1px solid rgb(175, 197, 168);" align="center" width="602" cellpadding="0" cellspacing="0">
<tbody><tr>
<td><a href="http://springhillnursery.com/?sid=417232&amp;utm_source=trigger-emails&amp;utm_medium=email&amp;utm_campaign=catalog-series1" target="_blank"><font style="font-size:14px;" face="Arial, Helvetica, sans-serif" size="3"><img style="display: block;" src="http://209.35.60.250/skin/frontend/enterprise/springhills/images/emaillogo.gif" alt="Spring Hill Nurseries" height="80" width="600" border="0"></font></a></td>
</tr>
</tbody></table>

<table style="background: url(&quot;images/header-bkgd.gif&quot;) no-repeat scroll 0% 0% rgb(51, 102, 51); border-left: 1px solid rgb(175, 197, 168); border-right: 1px solid rgb(175, 197, 168);" align="center" width="602" bgcolor="#336633" border="0" cellpadding="10" cellspacing="0">
<tbody><tr>
<td align="center" width="602"><font style="font-size:16px;" color="#ffffcc" face="Arial,Helvetica,sans-serif" size="5"><strong>Thank You For Requesting The Spring Hill Catalog!</strong></font></td>
</tr>
</tbody></table>


<table style="border-left: 1px solid rgb(175, 197, 168); border-right: 1px solid rgb(175, 197, 168); border-bottom: 1px solid rgb(175, 197, 168);" "="" width="602" cellpadding="0" cellspacing="0" align="center" >
<tbody><tr>
<td>
<div style="border:1px solid #003366;background-color:#F0F0E4;padding:10px;margin:10px;" align="center">
<strong><img src="http://209.35.60.250/skin/frontend/enterprise/springhills/images/catalog.jpg" alt="Spring Hill Catalog" align="right" hspace="10" vspace="5" border="0">

</strong>
<p style="margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;" align="left"><strong><font style="font-size:14px;" color="#336633" face="Arial, Helvetica, sans-serif" size="3">Watch for your catalog to arrive in your mailbox in the weeks ahead. In the meantime, we\'d like to invite you to 
  <a style="text-decoration: underline; color: rgb(0, 0, 128);" href="http://springhillnursery.com/?sid=417232&utm_source=trigger-emails&utm_medium=email&utm_campaign=catalog-series1">shop our website
  </a> where you\'ll find everything that\'s available in our catalog - 
PLUS EVEN MORE! But you don\'t have to wait for your catalog to arrive, 
to thank you for your interest in Spring Hill Nurseries we would like to
 invite you to SAVE $25.00 off any order of $50 or more worth of 
merchandise right now!</font></strong><font style="font-size:14px;" color="#336633" face="Arial, Helvetica, sans-serif" size="3"></font></p>

<p style="margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><font style="font-size:20px;" face="Arial, Helvetica, sans-serif" size="4"><a style="text-decoration: underline; color: rgb(0, 0, 128);" href="http://springhillnursery.com/?sid=417232&utm_source=trigger-emails&utm_medium=email&utm_campaign=catalog-series1"><strong>&nbsp;SHOP NOW and SAVE $25.00</strong></a></font></p>
</div>


<div style="padding:2px;margin:10px;font-size:10px;color:#000080;font-weight:800;background-color:#bedcfa;border:2px solid #000080;" align="center">
<p style="margin-top: 0; margin-right: 0; margin-bottom: 0px; margin-left: 0;"><font style="font-size:12px;" class="fontMobile" face="Arial, Helvetica, sans-serif" size="2">
<a style="color: rgb(0, 0, 128); font-weight: 800; text-decoration: none;" href="http://springhillnursery.com/sun-perennial.html?sid=417232&amp;utm_source=trigger-emails&amp;utm_medium=email&amp;utm_campaign=catalog-series1">Sun Perennials
</a>&nbsp;&nbsp;|&nbsp;&nbsp;
<a style="color: rgb(0, 0, 128); font-weight: 800; text-decoration: none;" href="http://springhillnursery.com/ground-cover.html?sid=417232&amp;utm_source=trigger-emails&amp;utm_medium=email&amp;utm_campaign=catalog-series1">Ground Covers
</a>&nbsp;&nbsp;|&nbsp;&nbsp;
<a style="color: rgb(0, 0, 128); font-weight: 800; text-decoration: none;" href="http://springhillnursery.com/shrubs.html?sid=417232&amp;utm_source=trigger-emails&amp;utm_medium=email&amp;utm_campaign=catalog-series1">Shrubs
</a>&nbsp;&nbsp;|&nbsp;&nbsp;
<a style="color: rgb(0, 0, 128); font-weight: 800; text-decoration: none;" href="http://springhillnursery.com/roses.html?sid=417232&amp;utm_source=trigger-emails&amp;utm_medium=email&amp;utm_campaign=catalog-series1">Roses
</a>&nbsp;&nbsp;|&nbsp;&nbsp;
<a style="color: rgb(0, 0, 128); font-weight: 800; text-decoration: none;" href="http://springhillnursery.com/grasses.html?sid=417232&amp;utm_source=trigger-emails&amp;utm_medium=email&amp;utm_campaign=catalog-series1">Grasses
</a>&nbsp;&nbsp;|&nbsp;&nbsp;
<a style="color: rgb(0, 0, 128); font-weight: 800; text-decoration: none;" href="http://springhillnursery.com/flowering-trees.html?sid=417232&amp;utm_source=trigger-emails&amp;utm_medium=email&amp;utm_campaign=catalog-series1">Trees
</a>&nbsp;&nbsp;|&nbsp;&nbsp;
<a style="color: rgb(0, 0, 128); font-weight: 800; text-decoration: none;" href="http://springhillnursery.com/flowering-vines.html?sid=417232&amp;utm_source=trigger-emails&amp;utm_medium=email&amp;utm_campaign=catalog-series1">Vines</a></font></p>
</div>

<p style="margin-top: 0; margin-right: 0; margin-bottom: 0px; margin-left: 0;" align="center"><font style="font-size:20px;" color="#336633" face="Arial, Helvetica, sans-serif" size="4"><strong>Browse Our Most Popular Categories Online Today!</strong></font></p>


<table align="center" width="560" border="0" cellpadding="5" cellspacing="0">  
<tbody><tr>
    
<td align="center">
<a href="http://springhillnursery.com/sun-perennial.html?sid=417232&amp;utm_source=trigger-emails&amp;utm_medium=email&amp;utm_campaign=catalog-series1"><img src="http://209.35.60.250/skin/frontend/enterprise/springhills/images/Perennials.jpg" alt="Perennials" style="width: 250px;" border="0"></a>

<p style="margin-top: 5px; margin-right: 0; margin-bottom: 5px; margin-left: 0;" align="center"><strong><font style="font-size:14px;" color="#336633" face="Arial, Helvetica, sans-serif" size="3"><a style="color: rgb(51, 102, 51);" href="http://springhillnursery.com/sun-perennial.html?sid=417232&amp;utm_source=trigger-emails&amp;utm_medium=email&amp;utm_campaign=catalog-series1">Sun Perennials</a></font></strong></p>
</td>
    
<td align="center">
<a href="http://springhillnursery.com/ground-cover.html?sid=417232&amp;utm_source=trigger-emails&amp;utm_medium=email&amp;utm_campaign=catalog-series1"><img src="http://209.35.60.250/skin/frontend/enterprise/springhills/images/Ground-Covers.jpg" alt="Ground Covers" style="width: 250px;" border="0"></a>

<p style="margin-top: 5px; margin-right: 0; margin-bottom: 5px; margin-left: 0;" align="center"><strong><font style="font-size:14px;" color="#336633" face="Arial, Helvetica, sans-serif" size="3"><a style="color: rgb(51, 102, 51);" href="http://springhillnursery.com/ground-cover.html?sid=417232&amp;utm_source=trigger-emails&amp;utm_medium=email&amp;utm_campaign=catalog-series1">Ground Covers</a></font></strong></p>
</td>
      
</tr>
  
<tr>
    
<td align="center" valign="top">
<a href="http://springhillnursery.com/flowering-vines.html?sid=417232&amp;utm_source=trigger-emails&amp;utm_medium=email&amp;utm_campaign=catalog-series1"><img src="http://209.35.60.250/skin/frontend/enterprise/springhills/images/Vines.jpg" alt="Vines" style="width: 250px;" border="0"></a>

<p style="margin-top: 5px; margin-right: 0; margin-bottom: 5px; margin-left: 0;" align="center"><strong><font style="font-size:14px;" color="#336633" face="Arial, Helvetica, sans-serif" size="3"><a style="color: rgb(51, 102, 51);" href="http://springhillnursery.com/flowering-vines.html?sid=417232&amp;utm_source=trigger-emails&amp;utm_medium=email&amp;utm_campaign=catalog-series1">Vines</a></font></strong></p>
</td>
    
<td align="center">
<a href="http://springhillnursery.com/shrubs.html?sid=417232&amp;utm_source=trigger-emails&amp;utm_medium=email&amp;utm_campaign=catalog-series1">
<img src="http://209.35.60.250/skin/frontend/enterprise/springhills/images/Shrubs.jpg" alt="Shrubs" style="width: 250px;" border="0"></a>
 

<p style="margin-top: 5px; margin-right: 0; margin-bottom: 5px; margin-left: 0;" align="center"><strong><font style="font-size:14px;" color="#336633" face="Arial, Helvetica, sans-serif" size="3"><a style="color: rgb(51, 102, 51);" href="http://springhillnursery.com/shrubs.html?sid=417232&amp;utm_source=trigger-emails&amp;utm_medium=email&amp;utm_campaign=catalog-series1">Shrubs</a></font></strong></p>

</td>    
</tr>  
</tbody></table>

<p style="margin-top: 0; margin-right: 0; margin-bottom: 5px; margin-left: 0;" align="center"><strong><font style="font-size:20px;" color="#336633" face="Arial, Helvetica, sans-serif" size="4"><a style="color: rgb(0, 0, 128);" href="http://springhillnursery.com/?sid=417232&amp;utm_source=trigger-emails&amp;utm_medium=email&amp;utm_campaign=catalog-series1">SHOP NOW and SAVE $25.00&nbsp;»
</a></font></strong></p>

</td>
</tr>
</tbody></table>



<table width="602" cellpadding="10" cellspacing="0" align="center" >
<tbody><tr>
<td>
<p style="margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><font style="font-size:11px;" class="fontMobile" face="Arial, Helvetica, sans-serif" size="1">Copyright ©2012 Spring Hill Nurseries LLC. All Rights Reserved.<br>
110 West Elm Street | Tipp City | OH | 45371-1699</font></p>
<p style="margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><font style="font-size:11px;" class="fontMobile" face="Arial, Helvetica, sans-serif" size="1"><strong>Subscription Information:</strong><br>
To ensure you receive Spring Hill offers to your inbox, please add 
offers@springhillnursery.com to your safe senders list, address book or 
contacts. You received this Spring Hill email offer because you signed 
up at our website, purchased from us, or inquired to receive more 
information from Spring Hill.&nbsp; <a href="http://springhillnursery.com/privacy-policy?sid=417232&amp;utm_source=trigger-emails&amp;utm_medium=email&amp;utm_campaign=catalog-series1" style="color: rgb(0, 0, 128);">Privacy Policy</a></font></p>
<p style="margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><font style="font-size:11px;" class="fontMobile" face="Arial, Helvetica, sans-serif" size="1">You are subscribed as '.$data['email'].'<strong></strong>.</font></p>

<p style="margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><font style="font-size:11px;" class="fontMobile" face="Arial, Helvetica, sans-serif" size="1"><strong>Offer Information:</strong><br>
To take advantage of the above specials, follow any link provided within
 this email. These specials may not be combined with any other offers, 
and do not apply to previous orders. To order without clicking through 
this email, please enter the offer keycode below in the keycode box 
within your shopping cart at our website. Or mention the offer keycode 
to your customer service representative when you Order by Phone 
513-354-1509.</font></p>
<p style="margin-top: 0; margin-right: 0; margin-bottom: 0px; margin-left: 0;"><font style="font-size:11px;" class="fontMobile" face="Arial, Helvetica, sans-serif" size="1"><strong>This Email Offer Expires in 7 Days.<br>
<span style="color:#cc0000;">Offer Keycode: 417232</span></strong></font></p>
</td>
</tr>
</tbody></table>';


		

				$email_body = $message ;
				$useremail_body = $email_body;
				/*$mail = new Zend_Mail();
				$mail->setType(Zend_Mime::MULTIPART_RELATED);
				$mail->setBodyHtml($email_body);
				$mail->setFrom($senderEmail, $senderFName);
				$mail->addTo($reEmail, $reFName);
				$mail->setSubject('Spring Hills Catalog Request');*/
				 
				$usermail = new Zend_Mail();
				$usermail->setType(Zend_Mime::MULTIPART_RELATED);
				$usermail->setBodyHtml($useremail_body);
				$usermail->setFrom($reEmail, $reFName);
				$usermail->addTo($senderEmail, $senderFName);
				$usermail->setSubject('Spring Hills Catalog Request Offers');

				try {
					Mage::getSingleton('core/session')->addSuccess('Your catalog request was submitted successfully.');
					if(!empty($data['email'])){
						if($usermail->send())
						{ 							
							// set cookie for email subscription
							Mage::getModel('core/cookie')->set('email_id', $post['email']);
							Mage::getModel('core/cookie')->set('firstname', $post['firstname']);
							Mage::getModel('core/cookie')->set('lastname', $post['lastname']);
						}
					}
				}
				catch(Exception $ex) {
				        Mage::getSingleton('core/session')->addSuccess('Unable to submit your request. Please, try again later');
				
				} 
				Mage::getSingleton('core/session')->setFormData(false);
				$this->_redirect('catalogoffer-thankyou');
				return;
	
            } catch (Exception $e) {
                $translate->setTranslateInline(true);
                //Mage::getSingleton('customer/session')->addError(Mage::helper('contacts')->__('Unable to submit your request. Please, try again later'));
				//Mage::getSingleton('core/session')->addSuccess('Unable to submit your request. Please, try again later');
                $this->_redirect('*/*/');
                return;
            }

        } else {
            $this->_redirect('*/*/');
        }
    }
}
