<?php
class Springhills_Catalogoffer_Block_Adminhtml_Catalogoffer_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
		public function __construct()
		{
				parent::__construct();
				$this->setId("catalogoffer_tabs");
				$this->setDestElementId("edit_form");
				$this->setTitle(Mage::helper("catalogoffer")->__("Offer Information"));
		}
		protected function _beforeToHtml()
		{
				$this->addTab("form_section", array(
				"label" => Mage::helper("catalogoffer")->__("Offer Information"),
				"title" => Mage::helper("catalogoffer")->__("Offer Information"),
				"content" => $this->getLayout()->createBlock("catalogoffer/adminhtml_catalogoffer_edit_tab_form")->toHtml(),
				));
				return parent::_beforeToHtml();
		}

}
