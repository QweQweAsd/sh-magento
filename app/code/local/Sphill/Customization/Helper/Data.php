<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category    Sphill
 * @package     Sphill_Customization
 * @copyright   Copyright (c) 2009 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

class Sphill_Customization_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Remove old files from directory
     *
     * $timeInterval is argument for function strtotime ("-1 day", "-1 month")
     */
    public function clearDirectory($path, $timeInterval)
    {
        $fileDir = trim($path, DS);
        if ($handle = opendir($fileDir)) {
            while (false !== ($file = readdir($handle))) {
                $filePath = $fileDir . DS . $file;
                if ($file != '.' && $file != '..' && filemtime($filePath) < strtotime($timeInterval)) {
                    if (is_file($filePath)) unlink($filePath);
                }
            }
            closedir($handle);
        }

        return true;
    }
    
}
