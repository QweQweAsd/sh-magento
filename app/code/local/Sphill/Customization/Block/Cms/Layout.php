<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category    Sphill
 * @package     Sphill_Customization
 * @copyright   Copyright (c) 2009 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

class Sphill_Customization_Block_Cms_Layout extends Mage_Core_Block_Template
{
    protected function _prepareLayout()
    {
        if ($rootBlock = $this->getLayout()->getBlock('root')) {
            if ($rootBlock->getTemplate() == 'page/short_cms.phtml' ) {
                $this->_killBlock('refer_friend');
                $this->_killBlock('callout_quality');
                $this->_killBlock('testimonials');
                $this->_killBlock('top5');
				$this->_killBlock('right.poll');
				$this->_killBlock('verisign');
            }
        }
        return parent::_prepareLayout();
    }

    /**
     * Remove Block from layout and also from his parent
     *
     * @param unknown_type $blockName
     */
    protected function _killBlock($blockName)
    {
        if ($this->getLayout()->getBlock($blockName)) {
            $this->getLayout()->getBlock($blockName)
                    ->getParentBlock()->unsetChild($blockName);
            $this->getLayout()->unsetBlock($blockName);
        }
        return $this;
    }
}
