<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition License
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magentocommerce.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_CatalogSearch
 * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://www.magentocommerce.com/license/enterprise-edition
 */

/**
 * Advanced search result
 *
 * @category   Mage
 * @package    Mage_CatalogSearch
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_CatalogSearch_Block_Advanced_Result extends Mage_Core_Block_Template
{
    protected function _prepareLayout()
    {
        if ($breadcrumbs = $this->getLayout()->getBlock('breadcrumbs')) {
            $breadcrumbs->addCrumb('home', array(
                'label'=>Mage::helper('catalogsearch')->__('Home'),
                'title'=>Mage::helper('catalogsearch')->__('Go to Home Page'),
                'link'=>Mage::getBaseUrl()
            ))->addCrumb('search_result', array(
                'label'=>Mage::helper('catalogsearch')->__('Results')
            ));
        }
        return parent::_prepareLayout();
    }

    public function setListOrders() {
        $category = Mage::getSingleton('catalog/layer')
            ->getCurrentCategory();
        /* @var $category Mage_Catalog_Model_Category */

        $availableOrders = $category->getAvailableSortByOptions();
        unset($availableOrders['position']);

        $this->getChild('search_result_list')
            ->setAvailableOrders($availableOrders);
    }

    public function setListModes() {
        $this->getChild('search_result_list')
            ->setModes(array(
                'grid' => Mage::helper('catalogsearch')->__('Grid'),
                'list' => Mage::helper('catalogsearch')->__('List'))
            );
    }

    public function setListCollection() {
        $this->getChild('search_result_list')
           ->setCollection($this->_getProductCollection());
    }

    protected function _getProductCollection(){

		// For Auto sort by zone
		$zoneCookie = Mage::getModel('core/cookie')->get('zone');
		$actual_zone = substr($zoneCookie,0,-1);	
		$zone = 'zone'.$actual_zone;
		if(!$zoneCookie ) {
			$results =$this->getSearchModel()->getProductCollection()
							//->joinField('inventory_in_stock', 'cataloginventory_stock_item', 'is_in_stock', 'product_id=entity_id','is_in_stock>=0', 'left')
							->addAttributeToFilter("price", array('gt'=> 0))
							->addAttributeToSort('inventory_in_stock','desc')
							->addAttributeToSort('name');

		}else{
			$order = $this->getRequest()->getParam('order');
			$sortByAttr = '';
			if( $order == "ordered_qty" )
				$sortByAttr = 'ordered_qty';
			elseif ( $order == "name" )
				$sortByAttr = 'name';
			elseif ($order == "price" )
				$sortByAttr = 'price';
            elseif ($order == "till_date_revenue")
                $sortByAttr = 'last_tendays_sale_price';
			else
				$sortByAttr = $zone;

            if($sortByAttr == 'last_tendays_sale_price'){
//echo $sortByAttr;
                $results =$this->getSearchModel()->getProductCollection()
                //->joinField('inventory_in_stock', 'cataloginventory_stock_item', 'is_in_stock', 'product_id=entity_id','is_in_stock>=0', 'left')
                    ->addAttributeToFilter("price", array('gt'=> 0))
		    ->addAttributeToFilter("visibility", array('in',array(4)))
                    ->addAttributeToSort('inventory_in_stock','desc')
                    ->addAttributeToSort($sortByAttr, 'desc')
                    ->addAttributeToSort('name');
            }else{
                $results =$this->getSearchModel()->getProductCollection()
                //->joinField('inventory_in_stock', 'cataloginventory_stock_item', 'is_in_stock', 'product_id=entity_id','is_in_stock>=0', 'left')
                    ->addAttributeToFilter("price", array('gt'=> 0))
		    ->addAttributeToFilter("visibility", array('in',array(4)))
                    ->addAttributeToSort('inventory_in_stock','desc')
                    ->addAttributeToSort($sortByAttr)
                    ->addAttributeToSort('name');
            }

		}
		return $results;
    }

    public function getSearchModel()
    {
        return Mage::getSingleton('catalogsearch/advanced');
    }

    public function getResultCount()
    {
        if (!$this->getData('result_count')) {
            $size = $this->getSearchModel()->getProductCollection()->getSize();
            $this->setResultCount($size);
        }
        return $this->getData('result_count');
    }

    public function getProductListHtml()
    {
        return $this->getChildHtml('search_result_list');
    }

    public function getFormUrl()
    {
        return Mage::getModel('core/url')
            ->setQueryParams($this->getRequest()->getQuery())
            ->getUrl('*/*/', array('_escape' => true));
    }

    public function getSearchCriterias()
    {
        $searchCriterias = $this->getSearchModel()->getSearchCriterias();
        $middle = ceil(count($searchCriterias) / 2);
        $left = array_slice($searchCriterias, 0, $middle);
        $right = array_slice($searchCriterias, $middle);

        return array('left'=>$left, 'right'=>$right);
    }
}
