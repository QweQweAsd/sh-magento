<?php

//$log_array = array();
//require_once '../app/Mage.php';
//umask(0);
//Mage::app('default');
function couponOfferCron($start=0) {
    $log_array = array();
    $argv[1] = $start;
    $write = Mage::getSingleton('core/resource')->getConnection('core_write');
    $import = 0;


//die();
//$coupon = Mage::getModel('freegift/salesrule');

    /* To check whether any other cron/update process is running or not 
     */
    $Status = $write->fetchAll("SELECT status FROM import_notification_history WHERE 
    import_type='inventoryFeed' or import_type='inventory' or import_type='price' or import_type='productDescription' or  import_type='deleteKeycodes' or import_type='deletePrice' or import_type='deleteShipping' ");

    if ($Status[0]['status'] != "inProgress" &&
            $Status[1]['status'] != "inProgress" &&
            $Status[2]['status'] != "inProgress" &&
            $Status[3]['status'] != "inProgress" && $Status[4]['status'] !="inProgress" && $Status[5]['status'] !="inProgress" && $Status[6]['status'] !="inProgress") {
//Mail to be sent for
        //$arrTo[] = array("toEmail" => "amitblues123@gmail.com", "toName" => "imsupport");
        //$arrTo[] = array("toEmail" => "mohansaip@kensium.com", "toName" => "Janesh");
//This URL IS USED FOR BATCH PROCESSING
//Semaphore Flag
        $txtFlagFile = '/home/springhill/UploadComplete.txt';

//Directory path where import files are located
        $dir_import = "/home/springhill/";

//Directory Path where Import files to be moved after successful import
        $dir_Archive = $GLOBALS["global_dir_Archive"];
        $keycodeFilePath = $dir_import . 'KeyCodes.txt';
        $catalogFilePath = $dir_import . 'CatalogOffers.txt';
        $ShippingFilePath = $dir_import . 'ShippingTables.txt';
        /*         * ************************************* */
        $InventoryId = $write->fetchOne("SELECT id FROM import_notification_history WHERE import_type='catalogOffers' and status='notStarted'");

//Checking flag file and then proceeding for Inventory Update

        if (file_exists($txtFlagFile) && file_exists($keycodeFilePath) && file_exists($catalogFilePath) && file_exists($ShippingFilePath)) {


            $OfferFeedId = $write->fetchOne("SELECT id FROM import_notification_history WHERE import_type='catalogOffers'");
            $write->query("UPDATE import_notification_history SET status='inProgress',result_count='" . $argv[1] . "' WHERE id=" . $OfferFeedId);

            if (file_exists($ShippingFilePath)) {
                try {
                    //massDelete();
                    /* To delete old Data */
                    $deleteOldData = $write->query("delete from shippingtables_temp");
                    $result = $write->query("LOAD DATA LOCAL INFILE '" . $ShippingFilePath . "' INTO TABLE shippingtables_temp FIELDS TERMINATED BY '|' ENCLOSED BY ''  LINES TERMINATED BY '\n' STARTING BY ''");
                    $import = 1;
                    $write->exec("call spCompareShippingTables()");
                } catch (Exception $e) {
                    echo 'Import Failed : ' . $e;
                }
                /* To Delete the file headers Row */
                if ($import == 1) {
                    $deleteResult = $write->query("delete from shippingtables_temp where Offer_Code='Offer_Code'");
                    $deleteResultSH = $write->query("delete from shippingtables where Offer_Code='Offer_Code'");
                    //$execute = shell_exec('rm -f ' . $filePath);
                }
            } else {
                echo 'Import Failed : File ' . $ShippingFilePath . " not exists";
            }

            /* For Keycodes */

            try {
                /* To delete old Data */
                $deleteOldData = $write->query("delete from keycodes_temp");
                $result = $write->query("LOAD DATA LOCAL INFILE '" . $keycodeFilePath . "' INTO TABLE keycodes_temp FIELDS TERMINATED BY '|' ENCLOSED BY ''  LINES TERMINATED BY '\n' STARTING BY ''");
                $import = 1;

                $write->exec("call spCompareKeyCodes()");

                if ($import == 1) {
                    $deleteResult = $write->query("delete from keycodes_temp where Keycode='Keycode'");
                    $deleteResultK = $write->query("delete from keycodes where Keycode='Keycode'");
                    echo 'Keycodes Created' . "\n";
                }
            } catch (Exception $e) {
                //echo 'Import of ' . $keycodeFilePath . ' Failed : ' . $e->getMessage();

                $log_array[]['Message'] = $e->getMessage();
                $log_array[]['Trace'] = $e->getTraceAsString();
                $log_array[]['Info'] = $keycodeFilePath . " file not exists.";
            }



            /* For Catalog Offers */

            try {
                /* To delete old Data */

                $deleteOldData = $write->query("delete from catalog_offers_temp");
                $result = $write->query("LOAD DATA LOCAL INFILE '" . $catalogFilePath . "' INTO TABLE catalog_offers_temp FIELDS TERMINATED BY '|' ENCLOSED BY ''  LINES TERMINATED BY '\n' STARTING BY ''");
                $import = 1;

                $write->exec("call spCompareCatalogOffers()");

                if ($import == 1) {
                    $deleteResult = $write->query("delete from catalog_offers_temp where Offer='Offer'");
                    $deleteResultCO = $write->query("delete from catalog_offers where Offer='Offer'");
                    echo 'CatalogOffers Created';
                }
            } catch (Exception $e) {
                //echo 'Import Failed : ' . $e;
                $log_array[]['Message'] = $e->getMessage();
                $log_array[]['Trace'] = $e->getTraceAsString();
                $log_array[]['Info'] = $catalogFilePath . " file not exists.";
            }
            $write->query("UPDATE import_notification_history SET start_date=NOW(),email_notification='No',result_count='" . $argv[1] . "' WHERE id=" . $InventoryId);
            $sql = "SELECT Keycode,Offer_Code FROM keycodes_temp WHERE Offer_Code<>4";
            try {
                $info = $write->fetchAll($sql);
                $iter = 0;
		$code ='';
		$existOfferCodeInKeyCode='';
                foreach ($info as $row) {
                    $keyCode = array();
                    $offerSql = "SELECT * FROM catalog_offers_temp where  Offer_Code='" . $row['Offer_Code'] . "'   ";
                    try {
                        $offerInfo = $write->fetchAll($offerSql);
                        $i = 0;
                        if (!empty($offerInfo[0]['Coupon_Amt_1']) && !empty($offerInfo[0]['Coupon_Min_Order_1'])) {
                            $keyCode[$i]['keycode'] = $row['Keycode'];
                            $keyCode[$i]['amount'] = $offerInfo[0]['Coupon_Amt_1'];
                            $keyCode[$i]['min_order'] = $offerInfo[0]['Coupon_Min_Order_1'];
                            $keyCode[$i]['type'] = 'general';
                            $i++;
                        } if (!empty($offerInfo[0]['Coupon_Amt_2']) && !empty($offerInfo[0]['Coupon_Min_Order_2'])) {
                            $keyCode[$i]['keycode'] = $row['Keycode'] . "_" . $offerInfo[0]['Coupon_Min_Order_2'];
                            $keyCode[$i]['amount'] = $offerInfo[0]['Coupon_Amt_2'];
                            $keyCode[$i]['min_order'] = $offerInfo[0]['Coupon_Min_Order_2'];
                            $keyCode[$i]['type'] = 'general';
                            $i++;
                        } if (!empty($offerInfo[0]['Coupon_Amt_3']) && !empty($offerInfo[0]['Coupon_Min_Order_3'])) {
                            $keyCode[$i]['keycode'] = $row['Keycode'] . "_" . $offerInfo[0]['Coupon_Min_Order_3'];
                            $keyCode[$i]['amount'] = $offerInfo[0]['Coupon_Amt_3'];
                            $keyCode[$i]['min_order'] = $offerInfo[0]['Coupon_Min_Order_3'];
                            $keyCode[$i]['type'] = 'general';
                            $i++;
                        }if (!empty($offerInfo[0]['Coupon_Amt_4']) && !empty($offerInfo[0]['Coupon_Min_Order_4'])) {
                            $keyCode[$i]['keycode'] = $row['Keycode'] . "_" . $offerInfo[0]['Coupon_Min_Order_4'];
                            $keyCode[$i]['amount'] = $offerInfo[0]['Coupon_Amt_4'];
                            $keyCode[$i]['min_order'] = $offerInfo[0]['Coupon_Min_Order_4'];
                            $keyCode[$i]['type'] = 'general';
                            $i++;
                        }if (!empty($offerInfo[0]['Coupon_Amt_4']) && !empty($offerInfo[0]['Coupon_Min_Order_4'])) {
                            $keyCode[$i]['keycode'] = $row['Keycode'] . "_" . $offerInfo[0]['Coupon_Min_Order_4'];
                            $keyCode[$i]['amount'] = $offerInfo[0]['Coupon_Amt_4'];
                            $keyCode[$i]['min_order'] = $offerInfo[0]['Coupon_Min_Order_4'];
                            $keyCode[$i]['type'] = 'general';
                            $i++;
                        }
                        if (!empty($offerInfo[0]['Bonus_Item_1']) && !empty($offerInfo[0]['Bonus_Amount_1'])) {
                            $keyCode[$i]['keycode'] = $row['Keycode'];
                            $keyCode[$i]['product'] = $offerInfo[0]['Bonus_Item_1'];
                            $keyCode[$i]['min_order'] = $offerInfo[0]['Bonus_Amount_1'];
                            $keyCode[$i]['type'] = 'freegift';
                            $i++;
                        }if ((empty($offerInfo[0]['Coupon_Amt_1']) && empty($offerInfo[0]['Coupon_Min_Order_1'])) && (empty($offerInfo[0]['Bonus_Item_1']) && empty($offerInfo[0]['Bonus_Amount_1']))) {
                            $keyCode[$i]['keycode'] = $row['Keycode'];
                            $keyCode[$i]['amount'] = $offerInfo[0]['Coupon_Amt_1'];
                            $keyCode[$i]['min_order'] = $offerInfo[0]['Coupon_Min_Order_1'];
                            $keyCode[$i]['type'] = 'general';
                        }
                        $usesPerCoupon = "";
                        $usesPerCustomer = "";
                        if ($offerInfo[0]['Custnbr_Required'] == "True" && $offerInfo[0]['One_Use'] == 'True') {
                            $usesPerCoupon = 1;
                            $usesPerCustomer = 1;
                        } else if ($offerInfo[0]['Custnbr_Required'] == "True" && ($offerInfo[0]['One_Use'] == 'False' || empty($offerInfo[0]['One_Use']))) {
                            $usesPerCoupon = "";
                            $usesPerCustomer = "1";
                        } else if (($offerInfo[0]['Custnbr_Required'] == "False" || empty($offerInfo[0]['Custnbr_Required'])) && $offerInfo[0]['One_Use'] == 'True') {
                            $usesPerCoupon = 1;
                            $usesPerCustomer = "";
                        } else if (($offerInfo[0]['Custnbr_Required'] == "False" || empty($offerInfo[0]['Custnbr_Required'])) &&
                                ($offerInfo[0]['One_Use'] == 'False' || empty($offerInfo[0]['One_Use']))) {
                            $usesPerCoupon = "";
                            $usesPerCustomer = "";
                        }

                        if ($offerInfo[0]['One_Use'] == 'True')
                            $onetime = 1;
                        else
                            $onetime=0;
                        $j = 0;

                        foreach ($keyCode as $rec) {

                            if ($keyCode[$j]['type'] == 'general') {
								$createflag=0;
								if(!in_array($row['Offer_Code'], $code)){
									$code[] = $row['Offer_Code'];
							        $existOfferCodeInKeyCode[] = "'".$row['Offer_Code']."'"; // for comparing with the offer that are not present in the keycode_temp table
									$createflag=1;
								}
                                $conditionsArray[0] = array("type" => 'salesrule/rule_condition_address',
                                    "attribute" => 'base_subtotal',
                                    "operator" => '>=',
                                    "value" => $keyCode[$j]['min_order'],
                                    "is_value_processed" => 0);
                                $array = array("type" => 'salesrule/rule_condition_combine',
                                    "attribute" => '',
                                    "operator" => '',
                                    "value" => 1,
                                    "is_value_processed" => '',
                                    "aggregator" => 'all',
                                    "conditions" => $conditionsArray);
                                $conditionSerialized = serialize($array);
                                $ruleDetails = array("name" => $offerInfo[0]['Offer'],
                                    "description" => $offerInfo[0]['Offer_Text'],
                                    "Offer_Date" => $offerInfo[0]['Offer_Date'],
                                    "Offer_Exp_Date" => $offerInfo[0]['Offer_Exp_Date'],
                                    "couponType" => 2,
                                    "usesPerCoupon" => $usesPerCoupon,
                                    "couponcode" => $keyCode[$j]['keycode'],
                                    "usesPerCustomer" => $usesPerCustomer,
                                    "Offer_Code" => $row['Offer_Code'],
                                    "discountAmount" => $keyCode[$j]['amount']);
                                echo generalCouponCode($conditionSerialized, $ruleDetails, $write,$createflag);
                            } elseif ($keyCode[$j]['type'] == 'freegift') {

                                $conditionsArray[0] = array("type" => 'salesrule/rule_condition_address',
                                    "attribute" => 'base_subtotal',
                                    "operator" => '>=',
                                    "value" => $keyCode[$j]['min_order'],
                                    "is_value_processed" => 0);
                                $array = array("type" => 'salesrule/rule_condition_combine',
                                    "attribute" => '',
                                    "operator" => '',
                                    "value" => 1,
                                    "is_value_processed" => '',
                                    "usesPerCoupon" => $onetime,
                                    "aggregator" => 'all',
                                    "conditions" => $conditionsArray);
                                $conditionSerialized = serialize($array);
                                $sku = $keyCode[$j]['product'];
                                $_Pdetails = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);
                                if ($_Pdetails) {
                                    $productid = $_Pdetails->getId();
                                    $ruleDetails = array("name" => $offerInfo[0]['Offer'],
                                        "description" => $offerInfo[0]['Offer_Text'],
                                        "Offer_Date" => $offerInfo[0]['Offer_Date'],
                                        "Offer_Exp_Date" => $offerInfo[0]['Offer_Exp_Date'],
                                        "couponType" => 2,
                                        "couponcode" => $keyCode[$j]['keycode'],
                                        "usesPerCustomer" => $onetime,
                                        "Offer_Code" => $row['Offer_Code'],
                                        "free-product" => $productid,
                                        "minOrder" => $keyCode[$j]['min_order']);
                                    echo freeGift($conditionSerialized, $ruleDetails, $write);
                                } else {
                                    echo 'Creation Failed' . $sku . ' doesnot exists' . "\n";
                                }
                            }
                            $j++;
                        }
                    } catch (Exception $e) {
                        echo $e->getMessage();
                        $log_array[]['Message'] = $e->getMessage();
                        $log_array[]['Trace'] = $e->getTraceAsString();
                        $log_array[]['Info'] = $row['Offer_Code'] . "  not exists.";
                    }
                }

                /************ Offer_Code that are are not present in the keycode_temp table, then for restOffer we are looking into the keycode table to update the existing offer_code ****/
		//print_r($existOfferCodeInKeyCode);

                try{

                    $offerNotIn = implode(",",$existOfferCodeInKeyCode);

			        if(count($existOfferCodeInKeyCode) > 0){
                        $restOffers = $write->fetchAll("SELECT * FROM catalog_offers_temp where Offer_Code not in (".$offerNotIn.")   ");
                    }else{
                        $restOffers = $write->fetchAll("SELECT * FROM catalog_offers_temp where 1 ");
                    }
                   // echo "SELECT * FROM catalog_offers_temp where Offer_Code not in (".$offerNotIn.")   ";
		//die;

                    foreach($restOffers as $restOffer){
                        //echo $restOffer['Offer_Code'];
                        //if(in_array($restOffer['Offer_Code'],$existOfferCodeInKeyCode)) continue;
                        //echo "SELECT Keycode,Offer_Code FROM keycodes WHERE Offer_Code='".$restOffer['Offer_Code']."' limit 1 ";
                        $rows = $write->fetchAll("SELECT Keycode,Offer_Code FROM keycodes WHERE Offer_Code='".$restOffer['Offer_Code']."' limit 1 ");
                        foreach( $rows as $rw ){
                            $i = 0;
                            $keyCode = array();
                            if (!empty($restOffer['Coupon_Amt_1']) && !empty($restOffer['Coupon_Min_Order_1'])) {
                                $keyCode[$i]['keycode'] = $rw['Keycode'];
                                $keyCode[$i]['amount'] = $restOffer['Coupon_Amt_1'];
                                $keyCode[$i]['min_order'] = $restOffer['Coupon_Min_Order_1'];
                                $keyCode[$i]['type'] = 'general';
                                $i++;
                            } if (!empty($restOffer['Coupon_Amt_2']) && !empty($restOffer['Coupon_Min_Order_2'])) {
                                $keyCode[$i]['keycode'] = $rw['Keycode'] . "_" . $restOffer['Coupon_Min_Order_2'];
                                $keyCode[$i]['amount'] = $restOffer['Coupon_Amt_2'];
                                $keyCode[$i]['min_order'] = $restOffer['Coupon_Min_Order_2'];
                                $keyCode[$i]['type'] = 'general';
                                $i++;
                            } if (!empty($restOffer['Coupon_Amt_3']) && !empty($restOffer['Coupon_Min_Order_3'])) {
                                $keyCode[$i]['keycode'] = $rw['Keycode'] . "_" . $restOffer['Coupon_Min_Order_3'];
                                $keyCode[$i]['amount'] = $restOffer['Coupon_Amt_3'];
                                $keyCode[$i]['min_order'] = $restOffer['Coupon_Min_Order_3'];
                                $keyCode[$i]['type'] = 'general';
                                $i++;
                            }if (!empty($restOffer['Coupon_Amt_4']) && !empty($restOffer['Coupon_Min_Order_4'])) {
                                $keyCode[$i]['keycode'] = $rw['Keycode'] . "_" . $offerInfo[0]['Coupon_Min_Order_4'];
                                $keyCode[$i]['amount'] = $restOffer['Coupon_Amt_4'];
                                $keyCode[$i]['min_order'] = $restOffer['Coupon_Min_Order_4'];
                                $keyCode[$i]['type'] = 'general';
                                $i++;
                            }if (!empty($restOffer['Coupon_Amt_4']) && !empty($restOffer['Coupon_Min_Order_4'])) {
                                $keyCode[$i]['keycode'] = $rw['Keycode'] . "_" . $restOffer['Coupon_Min_Order_4'];
                                $keyCode[$i]['amount'] = $restOffer['Coupon_Amt_4'];
                                $keyCode[$i]['min_order'] = $restOffer['Coupon_Min_Order_4'];
                                $keyCode[$i]['type'] = 'general';
                                $i++;
                            }
                            if (!empty($restOffer['Bonus_Item_1']) && !empty($restOffer['Bonus_Amount_1'])) {
                                $keyCode[$i]['keycode'] = $rw['Keycode'];
                                $keyCode[$i]['product'] = $restOffer['Bonus_Item_1'];
                                $keyCode[$i]['min_order'] = $restOffer['Bonus_Amount_1'];
                                $keyCode[$i]['type'] = 'freegift';
                                $i++;
                            }if ((empty($restOffer['Coupon_Amt_1']) && empty($restOffer['Coupon_Min_Order_1'])) && (empty($restOffer['Bonus_Item_1']) && empty($restOffer['Bonus_Amount_1']))) {
                                $keyCode[$i]['keycode'] = $rw['Keycode'];
                                $keyCode[$i]['amount'] = $restOffer['Coupon_Amt_1'];
                                $keyCode[$i]['min_order'] = $restOffer['Coupon_Min_Order_1'];
                                $keyCode[$i]['type'] = 'general';
                            }
                            $usesPerCoupon = "";
                            $usesPerCustomer = "";
                            if ($restOffer['Custnbr_Required'] == "True" && $restOffer['One_Use'] == 'True') {
                                $usesPerCoupon = 1;
                                $usesPerCustomer = 1;
                            } else if ($restOffer['Custnbr_Required'] == "True" && ($restOffer['One_Use'] == 'False' || empty($restOffer['One_Use']))) {
                                $usesPerCoupon = "";
                                $usesPerCustomer = "1";
                            } else if (($restOffer['Custnbr_Required'] == "False" || empty($restOffer['Custnbr_Required'])) && $restOffer['One_Use'] == 'True') {
                                $usesPerCoupon = 1;
                                $usesPerCustomer = "";
                            } else if (($restOffer['Custnbr_Required'] == "False" || empty($restOffer['Custnbr_Required'])) &&
                                ($restOffer['One_Use'] == 'False' || empty($restOffer['One_Use']))) {
                                $usesPerCoupon = "";
                                $usesPerCustomer = "";
                            }

                            if ($restOffer['One_Use'] == 'True')
                                $onetime = 1;
                            else
                                $onetime=0;
                            $j = 0;
                            foreach ($keyCode as $rec) {

                                if ($keyCode[$j]['type'] == 'general') {
                                    $conditionsArray[0] = array("type" => 'salesrule/rule_condition_address',
                                        "attribute" => 'base_subtotal',
                                        "operator" => '>=',
                                        "value" => $keyCode[$j]['min_order'],
                                        "is_value_processed" => 0);
                                    $array = array("type" => 'salesrule/rule_condition_combine',
                                        "attribute" => '',
                                        "operator" => '',
                                        "value" => 1,
                                        "is_value_processed" => '',
                                        "aggregator" => 'all',
                                        "conditions" => $conditionsArray);
                                    $conditionSerialized = serialize($array);
                                    $ruleDetails = array("name" => $restOffer['Offer'],
                                        "description" => $restOffer['Offer_Text'],
                                        "Offer_Date" => $restOffer['Offer_Date'],
                                        "Offer_Exp_Date" => $restOffer['Offer_Exp_Date'],
                                        "couponType" => 2,
                                        "usesPerCoupon" => $usesPerCoupon,
                                        "couponcode" => $keyCode[$j]['keycode'],
                                        "usesPerCustomer" => $usesPerCustomer,
                                        "Offer_Code" => $rw['Offer_Code'],
                                        "discountAmount" => $keyCode[$j]['amount']);
                                    echo generalCouponCode($conditionSerialized, $ruleDetails, $write , 1);
                                } elseif ($keyCode[$j]['type'] == 'freegift' && 0) {

                                    $conditionsArray[0] = array("type" => 'salesrule/rule_condition_address',
                                        "attribute" => 'base_subtotal',
                                        "operator" => '>=',
                                        "value" => $keyCode[$j]['min_order'],
                                        "is_value_processed" => 0);
                                    $array = array("type" => 'salesrule/rule_condition_combine',
                                        "attribute" => '',
                                        "operator" => '',
                                        "value" => 1,
                                        "is_value_processed" => '',
                                        "usesPerCoupon" => $onetime,
                                        "aggregator" => 'all',
                                        "conditions" => $conditionsArray);
                                    $conditionSerialized = serialize($array);
                                    $sku = $keyCode[$j]['product'];
                                    $_Pdetails = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);
                                    if ($_Pdetails) {
                                        $productid = $_Pdetails->getId();
                                        $ruleDetails = array("name" => $offerInfo[0]['Offer'],
                                            "description" => $offerInfo[0]['Offer_Text'],
                                            "Offer_Date" => $offerInfo[0]['Offer_Date'],
                                            "Offer_Exp_Date" => $offerInfo[0]['Offer_Exp_Date'],
                                            "couponType" => 2,
                                            "couponcode" => $keyCode[$j]['keycode'],
                                            "usesPerCustomer" => $onetime,
                                            "Offer_Code" => $row['Offer_Code'],
                                            "free-product" => $productid,
                                            "minOrder" => $keyCode[$j]['min_order']);
                                        echo freeGift($conditionSerialized, $ruleDetails, $write);
                                    } else {
                                        echo 'Creation Failed' . $sku . ' doesnot exists' . "\n";
                                    }
                                }
                                $j++;
                            }
                        }

                    }
                }catch (Exception $e) {
                    echo $e->getMessage();
                    $log_array[]['Message'] = $e->getMessage();
                    $log_array[]['Trace'] = $e->getTraceAsString();
                    $log_array[]['Info'] = $rw['Offer_Code'] . "  not exists.";
                }
                /*                 * *************************** */
                try {

                    $txFileName = "KeyCodes.txt";
                    $timestamp = date("YmdHis") . "_";
                    $txtArchiveKeycodes = $dir_Archive . $timestamp . $txFileName;
                    if (copy($keycodeFilePath, $txtArchiveKeycodes)) {
                        unlink($keycodeFilePath);
                        $write->query("UPDATE import_notification_history SET email_notification='No' WHERE import_type='catalogOffers'");
                    }
                } catch (Exception $e) {
                    $log_array[]['Message'] = $e->getMessage();
                    $log_array[]['Trace'] = $e->getTraceAsString();
                    $log_array[]['Info'] = "Something went wrong while archive the " . $txFileName . " file.";
                }
                try {
                    /* To Delete the file headers Row */
                    $txFileName1 = "CatalogOffers.txt";
                    $txtArchiveCatalogOffers = $dir_Archive . $timestamp . $txFileName1;
                    if (copy($catalogFilePath, $txtArchiveCatalogOffers)) {

                        unlink($catalogFilePath);
                    }
                    /* To Delete the file headers Row */
                } catch (Exception $e) {
                    $log_array[]['Message'] = $e->getMessage();
                    $log_array[]['Trace'] = $e->getTraceAsString();
                    $log_array[]['Info'] = "Something went wrong while archive the " . $txFileName1 . " file.";
                }

                try {
                    $txFileName2 = "ShippingTables.txt";
                    $txtArchiveShippingtable = $dir_Archive . $timestamp . $txFileName2;
                    if (copy($ShippingFilePath, $txtArchiveShippingtable)) {
                        unlink($ShippingFilePath);
                    }
                } catch (Exception $e) {
                    $log_array[]['Message'] = $e->getMessage();
                    $log_array[]['Trace'] = $e->getTraceAsString();
                    $log_array[]['Info'] = "Something went wrong while archive the " . $txFileName2 . " file.";
                }

                try {
                    $produ_desc_file = $dir_import . "ProductDescription.txt";
                    $inventory_file = $dir_import . "Inventory.txt";
                    $inventory_feed_file = $dir_import . "InventoryFeed.txt";
                    $price_file = $dir_import . "Price.txt";
                    $catalog_offer_file = $dir_import . "CatalogOffers.txt";
                    $keycode_file = $dir_import . "KeyCodes.txt";
                    $shipping_table_file = $dir_import . "ShippingTables.txt";
                    $delete_Price = $dir_import . "DeletePrice.txt";
                    $delete_keycodes = $dir_import . "DeleteKeyCodes.txt";
                    $delete_Shipping = $dir_import . "DeleteShippingDetails.txt";

                    if (!file_exists($produ_desc_file) &&
                            !file_exists($inventory_file) &&
                            !file_exists($inventory_feed_file) &&
                            !file_exists($price_file) &&
                            !file_exists($catalog_offer_file) &&
                            !file_exists($keycode_file) &&
                            !file_exists($shipping_table_file) &&
                            !file_exists($delete_Price)&&
                            !file_exists($delete_keycodes)&&
                            !file_exists($delete_Shipping)) {
                        unlink($txtFlagFile);
                        /* for ($i = 1; $i < 9; $i++) {
                          $process = Mage::getModel('index/process')->load($i);
                          $process->reindexAll();
                          } */
                    }
                } catch (Exception $e) {
                    $log_array[]['Message'] = $e->getMessage();
                    $log_array[]['Trace'] = $e->getTraceAsString();
                    $log_array[]['Info'] = "Something went wrong while removing flag file.";
                }
                try {
                    $deleteResult = $write->query("delete from catalog_offers_temp");
                    $deleteResult1 = $write->query("delete from keycodes_temp");
                    $deleteResult2 = $write->query("delete from shippingtables_temp");
                    $write->query("UPDATE import_notification_history SET status='notStarted', end_date=NOW() WHERE import_type='catalogOffers' and status='inProgress'");
                } catch (Exception $e) {
                    $log_array[]['Message'] = $e->getMessage();
                    $log_array[]['Trace'] = $e->getTraceAsString();
                    $log_array[]['Info'] = "Something went wrong while deleting the records from " . $tableName . " table.";
                }
                /*                 * ************************************* */
            } catch (Exception $e) {
                echo $e->getMessage();
                $log_array[]['Message'] = $e->getMessage();
                $log_array[]['Trace'] = $e->getTraceAsString();
                $log_array[]['Info'] = "Keycodes are not exists";
            }

            //DELETE THE ARCHIVE FILES OLDER THAN 30 Days
            if ($handle = opendir($dir_Archive)) {
                while (false !== ($entry = readdir($handle))) {
                    if ($entry != "." && $entry != "..") {
                        $arrFiles = explode("_", $entry);

                        $fileTimeStamp = $arrFiles[0];
                        $date1 = date('YmdHis');
                        $date2 = $fileTimeStamp;

                        $ts1 = strtotime($date1);
                        $ts2 = strtotime($date2);
                        $seconds_diff = $ts1 - $ts2;
                        $days = floor($seconds_diff / 3600 / 24);
                        try {
                            //Delete the files older than 30 days
                            if ($days > 30) {
                                unlink($dir_Archive . $entry);
                            }
                        } catch (Exception $e) {
                            $log_array[]['Message'] = $e->getMessage();
                            $log_array[]['Trace'] = $e->getTraceAsString();
                            $log_array[]['Info'] = "Something went wrong while deleting 30 days old archive files.";
                        }
                    }
                }
                closedir($handle);
            }

            if (count($log_array) > 0) {
                $errorMsg = '';
                foreach ($log_array as $log) {
                    $errorMsg .= '
          - - - - - - - -
          ERROR
          SUBJECT: ' . $log['Info'] . '
          Message: ' . $log['Message'] . '
          Trace: ' . $log['Trace'] . '
          ProductId: ' . $log['sku'] . '
          Time: ' . date('Y m d H:i:s') . '
          ';
                    echo "\r\n" . $errorMsg;
                }
                file_put_contents($dir_Archive . '/logs/log_' . time() . '.txt', $errorMsg);
                //Getting admin Information
                $adminInfo = $write->fetchAll("SELECT * from admin_user");
                $userFirstname = $adminInfo[0]['firstname'];
                $userLastname = $adminInfo[0]['lastname'];
                $userEmail = $adminInfo[0]['email'];
                //$arrTo[] = array("toEmail" => $userEmail, "toName" => $userFirstname . ' ' . $userLastname);

                $mail = new Zend_Mail();
                $mail->setType(Zend_Mime::MULTIPART_RELATED);
                $mail->setBodyHtml('There were errors updating the catalogOffers, keycodes, shippingTables for Spring Hill. Please review the log files at: ' . $_SERVER['HTTP_HOST'] . '/dailyfeed/archive/logs/log_' . time() . '.txt');
                //$mail->setFrom($userEmail, $userFirstname . ' ' . $userLastname);
                //SENDING MAIL TO Janesh,imsupport@gardensalive.com and Magento Admin
                /* foreach ($arrTo as $to) {
                  $mail->addTo($to['toEmail'], $to['toName']);
                  } */
                $mail->setFrom('mohansaip@kensium.com', 'Mohan sai');
                $mail->addTo('mohansaip@kensium.com', 'Mohan sai');
                $mail->addTo('sudhakark@kensium.com', 'Sudhakar');
                $mail->setSubject('Error updating SH product database');
//            $fileContents = file_get_contents($dir_Archive . '/logs/log_' . time() . '.txt');
//            $file = $mail->createAttachment($fileContents);
//            $file->filename = "log_" . time() . ".txt";
                try {
                    if ($mail->send()) {
                        Mage::getSingleton('core/session')->addSuccess('Log Information has been mailed');
                    }
                } catch (Exception $ex) {
                    Mage::getSingleton('core/session')->addSuccess('Unable to send mail');
                }
            }
        } else {
            echo $txtFlagFile . "\n" . $keycodeFilePath . "\n" . $catalogFilePath . "\n Not Exists\n";
            $log_array[]['Message'] = $txtFlagFile . "\n" . $keycodeFilePath . "\n" . $catalogFilePath . "\n Not Exists\n";
            $log_array[]['Trace'] = 'Files not exists';
            $log_array[]['Info'] = $txtFlagFile . "\n" . $keycodeFilePath . "\n" . $catalogFilePath . "\n Not Exists\n";
        }
    } else {
        echo 'Process not initiated';
        $log_array[]['Message'] = "Process not initiated";
        $log_array[]['Trace'] = "Process not initiated";
        $log_array[]['Info'] = "Process not initiated";
    }
    return TRUE;
}

function massDelete() {
    $oRuleCollection = Mage::getModel('salesrule/rule')->getCollection();
    foreach ($oRuleCollection as $oRule) {
        if ($oRule->getId() == 149)
            continue;
        $rulesIds[] = $oRule->getId();
    }
    //print_r($rulesIds); 
    if (count($rulesIds) > 0) {
        try {
            $model = Mage::getModel('salesrule/rule');
            foreach ($rulesIds as $rulesId) {
                $model->load($rulesId);
                if ($model->delete()) {
                    $write = Mage::getSingleton('core/resource')->getConnection('core_write');
                    $write->query("DELETE from salesrule_coupon where rule_id = '" . $rulesId . "' ");
                    echo $rulesId . " Deleted" . "\n";
                }
                //die();
            }
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }
    }
}

function freeGift($conditionSerialized, $ruleDetails, $write ) {

    $checkCode = $write->fetchAll("SELECT *  FROM `mw_freegift_salesrule` WHERE `coupon_code` ='" . $ruleDetails['couponcode'] . "'");

    $ruleId = $checkCode[0]['rule_id'];
    if (!empty($ruleId)) {
        $coupon = Mage::getModel('freegift/salesrule')->load($ruleId);
        $mode = 'update';
    } else {
        $coupon = Mage::getModel('freegift/salesrule');
        $mode = 'insert';
    }
    /* $coupon->setName($ruleDetails['name'])
      ->setDescription($ruleDetails['description'])
      ->setIsActive(1)
      ->setCustomerGroupIds('0, 1, 2, 3') //an array of customer grou pids
      ->setCouponCode($ruleDetails['couponcode'])
      ->setUsesPerCoupon($ruleDetails['usesPerCoupon'])
      ->setDiscountQty(null)
      ->setFromDate($ruleDetails['Offer_Date'])
      ->setToDate($ruleDetails['Offer_Exp_Date'])
      ->setSortOrder(0)
      ->setOfferCode($ruleDetails['Offer_Code'])
      ->setConditionsSerialized($conditionSerialized)
      ->setGiftProductIds($ruleDetails['free-product'])
      ->setWebsiteIds(array(1)); */

    try {
        //$coupon->save();
        //$updateCatalogOffer = $write->query("update catalog_offers set Flag=1 where Offer_Code='" . $ruleDetails['Offer_Code'] . "'");
        // $updateKeyCode = $write->query("update Keycodes set Flag=1 where Keycode='" . $ruleDetails['couponcode'] . "'");
        if ($mode == 'insert') {
            //echo 'Created FreeGift:: OfferCode:' . $ruleDetails['Offer_Code'] . '---Keycode:' . $ruleDetails['couponcode'] . "\n";
        }
        if ($mode == 'update') {
            //echo 'Updated FreeGift:: OfferCode:' . $ruleDetails['Offer_Code'] . '---Keycode:' . $ruleDetails['couponcode'] . "\n";
        }
    } catch (Except $e) {
        $result = 'Failed:-KeyCode creation:' . $ruleDetails['keycode'] . "\n" . $e->getMessage . "\n";
        $log_array[]['Message'] = $e->getMessage();
        $log_array[]['Trace'] = $e->getTraceAsString();
        $log_array[]['Info'] = "Unable to create couponcode:" . $ruleDetails['keycode'];
    }
    return $result;
}

function generalCouponCode($conditionSerialized, $ruleDetails, $write , $createflag) {
    
    $records = $write->fetchAll("SELECT rule_id FROM salesrule where offer_code= '".$ruleDetails['Offer_Code']."' ");
    //echo count($records);
	//echo "SELECT sr.rule_id FROM salesrule as sr, salesrule_coupon as sc where  sc.rule_id = sr.rule_id and sc.code= '".$ruleDetails['couponcode']."' ";
	$keycodes = $write->fetchAll("SELECT sr.rule_id FROM salesrule as sr, salesrule_coupon as sc where  sc.rule_id = sr.rule_id and sc.code= '".$ruleDetails['couponcode']."' ");
	
    if(count($records) > 0  && $createflag && count($keycodes) ){
		foreach($records as $record){
			$ruleId = $record['rule_id'];
			//$checkCode = Mage::getModel('salesrule/coupon')->load($ruleDetails['couponcode'], 'code');
			//$ruleId = $checkCode->getRuleId();

			$coupon = Mage::getModel('salesrule/rule')->load($ruleId);
			$mode = 'update';

			$coupon->setName($ruleDetails['name'])
			    ->setDescription($ruleDetails['description'])
			    ->setFromDate($ruleDetails['Offer_Date'])
			    ->setToDate($ruleDetails['Offer_Exp_Date'])
			    ->setCouponType(2)
			    ->setUsesPerCoupon($ruleDetails['usesPerCoupon'])
			    ->setUsesPerCustomer($ruleDetails['usesPerCustomer'])
			    ->setCouponCode($coupon->getCouponCode())
			    ->setConditionsSerialized($conditionSerialized)
			    ->setCustomerGroupIds(array(0, 1, 2, 3)) //an array of customer grou pids
			    ->setIsActive(1)
			    ->setActionsSerialized('a:6:{s:4:"type";s:40:"salesrule/rule_condition_product_combine";s:9:"attribute";N;s:8:"operator";N;s:5:"value";s:1:"1";s:18:"is_value_processed";N;s:10:"aggregator";s:3:"all";}')
			    ->setStopRulesProcessing(0)
			    ->setIsAdvanced(1)
			    ->setProductIds('')
			    ->setSortOrder(0)
			    ->setSimpleAction('cart_fixed')
			    ->setDiscountQty(null)
			    ->setDiscountStep('0')
			    ->setSimpleFreeShipping('0')
			    ->setApplyToShipping('0')
			    ->setIsRss(0)
			    //->setOfferCode($ruleDetails['Offer_Code'])
			    ->setWebsiteIds(array(1))
			    ->setDiscountAmount($ruleDetails['discountAmount']);
			try {
			    $coupon->save();
			    //$updateCatalogOffer = $write->query("update catalog_offers set Flag=1 where Offer_Code='" . $ruleDetails['Offer_Code'] . "'");
			    //$updateKeyCode = $write->query("update Keycodes set Flag=1 where Keycode='" . $ruleDetails['couponcode'] . "'");
			    if ($mode == 'insert')
			        echo '---Created:: OfferCode:' . $coupon->getOfferCode() . '---Keycode:' . $coupon->getCouponCode() . "\n";
			    if ($mode == 'update')
			        echo '---Updated:: OfferCode:' .$coupon->getOfferCode(). '---Keycode:' .  $coupon->getCouponCode()  . "\n";
			} catch (Except $e) {
			    $result = 'Failed:-KeyCode creation:' . $ruleDetails['keycode'] . "\n" . $e->getMessage . "\n";

			    $log_array[]['Message'] = $e->getMessage();
			    $log_array[]['Trace'] = $e->getTraceAsString();
			    $log_array[]['Info'] = "Unable to create couponcode:" . $ruleDetails['keycode'];
			}
		}
    } else {
		
		if(count($keycodes) == 0 ) {
			$coupon = Mage::getModel('salesrule/rule');
			$mode = 'insert';
			$coupon->setName($ruleDetails['name'])
				->setDescription($ruleDetails['description'])
				->setFromDate($ruleDetails['Offer_Date'])
				->setToDate($ruleDetails['Offer_Exp_Date'])
				->setCouponType(2)
				->setUsesPerCoupon($ruleDetails['usesPerCoupon'])
				->setUsesPerCustomer($ruleDetails['usesPerCustomer'])
				->setCouponCode($ruleDetails['couponcode'])
				->setConditionsSerialized($conditionSerialized)
				->setCustomerGroupIds(array(0, 1, 2, 3)) //an array of customer grou pids
				->setIsActive(1)
				->setActionsSerialized('a:6:{s:4:"type";s:40:"salesrule/rule_condition_product_combine";s:9:"attribute";N;s:8:"operator";N;s:5:"value";s:1:"1";s:18:"is_value_processed";N;s:10:"aggregator";s:3:"all";}')
				->setStopRulesProcessing(0)
				->setIsAdvanced(1)
				->setProductIds('')
				->setSortOrder(0)
				->setSimpleAction('cart_fixed')
				->setDiscountQty(null)
				->setDiscountStep('0')
				->setSimpleFreeShipping('0')
				->setApplyToShipping('0')
				->setIsRss(0)
				->setOfferCode($ruleDetails['Offer_Code'])
				->setWebsiteIds(array(1))
				->setDiscountAmount($ruleDetails['discountAmount']);
			try {
				$coupon->save();
				echo '---Created:: OfferCode:' . $ruleDetails['Offer_Code'] . '---Keycode:' . $ruleDetails['couponcode'] . "\n";
				$records = $write->fetchAll("SELECT rule_id FROM salesrule where offer_code= '".$ruleDetails['Offer_Code']."' ");
				if(count($records) > 0){
					foreach($records as $record){
						$ruleId = $record['rule_id'];
						//$checkCode = Mage::getModel('salesrule/coupon')->load($ruleDetails['couponcode'], 'code');
						//$ruleId = $checkCode->getRuleId();

						$coupon = Mage::getModel('salesrule/rule')->load($ruleId);
						$mode = 'update';

						$coupon->setName($ruleDetails['name'])
							->setDescription($ruleDetails['description'])
							->setFromDate($ruleDetails['Offer_Date'])
							->setToDate($ruleDetails['Offer_Exp_Date'])
							->setCouponType(2)
							->setUsesPerCoupon($ruleDetails['usesPerCoupon'])
							->setUsesPerCustomer($ruleDetails['usesPerCustomer'])
							->setCouponCode($coupon->getCouponCode())
							->setConditionsSerialized($conditionSerialized)
							->setCustomerGroupIds(array(0, 1, 2, 3)) //an array of customer grou pids
							->setIsActive(1)
							->setActionsSerialized('a:6:{s:4:"type";s:40:"salesrule/rule_condition_product_combine";s:9:"attribute";N;s:8:"operator";N;s:5:"value";s:1:"1";s:18:"is_value_processed";N;s:10:"aggregator";s:3:"all";}')
							->setStopRulesProcessing(0)
							->setIsAdvanced(1)
							->setProductIds('')
							->setSortOrder(0)
							->setSimpleAction('cart_fixed')
							->setDiscountQty(null)
							->setDiscountStep('0')
							->setSimpleFreeShipping('0')
							->setApplyToShipping('0')
							->setIsRss(0)
							//->setOfferCode($ruleDetails['Offer_Code'])
							->setWebsiteIds(array(1))
							->setDiscountAmount($ruleDetails['discountAmount']);
						try {
							$coupon->save();
							echo '---Updated:: OfferCode:' .$coupon->getOfferCode(). '---Keycode:' .  $coupon->getCouponCode()  . "\n";
						} catch (Except $e) {
							$result = 'Failed:-KeyCode creation:' . $ruleDetails['keycode'] . "\n" . $e->getMessage . "\n";

							$log_array[]['Message'] = $e->getMessage();
							$log_array[]['Trace'] = $e->getTraceAsString();
							$log_array[]['Info'] = "Unable to create couponcode:" . $ruleDetails['keycode'];
						}
					}
				}
				
				//echo "\n";

			} catch (Except $e) {
				$result = 'Failed:-KeyCode creation:' . $ruleDetails['keycode'] . "\n" . $e->getMessage . "\n";
				$log_array[]['Message'] = $e->getMessage();
				$log_array[]['Trace'] = $e->getTraceAsString();
				$log_array[]['Info'] = "Unable to create couponcode:" . $ruleDetails['keycode'];
			}
		}
    }

    return $result;
}

