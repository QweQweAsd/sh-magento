<?php
/*require_once '../app/Mage.php';
umask(0);
Mage::app('default');
global $global_dir_Archive;
$global_dir_Archive = "/var/www/springhill/dailyfeed/archive/";
////include 'web_conf.php';
KeycodeDeleteCron();*/
function KeycodeDeleteCron($start=0) {
    $log_array = array();
    $argv[1] = $start;
    $write = Mage::getSingleton('core/resource')->getConnection('core_write');

    $InventoryStatus = $write->fetchAll("SELECT status FROM import_notification_history 
    WHERE import_type='price' or import_type='inventoryFeed' or import_type='inventory' or import_type='catalogOffers' or import_type='productDescription' or  import_type='deleteShipping' or import_type='deletePrice' ");
    if ($InventoryStatus[0]['status'] != "inProgress" &&
            $InventoryStatus[1]['status'] != "inProgress" &&
            $InventoryStatus[2]['status'] != "inProgress" &&
            $InventoryStatus[3]['status'] != "inProgress" && $InventoryStatus[4]['status'] !="inProgress" && $InventoryStatus[5]['status'] !="inProgress" && $InventoryStatus[6]['status'] !="inProgress")  {
//Mail to be sent for
        //$arrTo[] = array("toEmail" => "amitblues123@gmail.com", "toName" => "imsupport");
        //$arrTo[] = array("toEmail" => "mohansaip@kensium.com", "toName" => "Janesh");
//Semaphore Flag
        $txtFlagFile = '/home/springhill/UploadComplete.txt';

//Directory path where import files are located
        $dir_import = "/home/springhill/";

//Directory Path where Import files to be moved after successful import
        $dir_Archive = $GLOBALS["global_dir_Archive"];

        $txFileName = "DeleteKeyCodes.txt";

        $timestamp = date("YmdHis") . "_";

        $txtArchivePrice = $dir_Archive . $timestamp . $txFileName;

        $tableName = "delete_keycodes";

        $txtPriceFile = $dir_import . $txFileName;

        $write = Mage::getSingleton('core/resource')->getConnection('core_write');

        $InventoryId = $write->fetchOne("SELECT id FROM import_notification_history WHERE import_type='deleteKeycodes' and status='notStarted'");

//Checking flag file and then proceeding for Inventory Update

        if (file_exists($txtFlagFile) && file_exists($txtPriceFile) && ($argv[1] == 0 && !empty($InventoryId) || ($argv[1] != 0 && empty($InventoryId)))) {
            $limit = 100;
            //Saving the lastupdated time in database for 36 hours email notification
            $InventoryFeedId = $write->fetchOne("SELECT id FROM import_notification_history WHERE import_type='deleteKeycodes'");
            if (empty($InventoryFeedId)) {
                $write->query("INSERT INTO import_notification_history(import_type,status) VALUES('deleteKeycodes','inProgress')");
            } else {
                $write->query("UPDATE import_notification_history SET status='inProgress',result_count='" . $argv[1] . "' WHERE id=" . $InventoryFeedId);
            }

//Get the limit to be imported by default 100        

            $connection = Mage::getSingleton('core/resource')->getConnection('core_read');

            if (file_exists($txtPriceFile) && empty($argv[1])) {
                try {
                    $write = Mage::getSingleton('core/resource')->getConnection('core_write');
                    $write->delete($tableName);

                    $write->query("LOAD DATA LOCAL INFILE '" . $txtPriceFile . "' INTO TABLE " . $tableName . "
FIELDS TERMINATED BY '|' ENCLOSED BY '' 
LINES TERMINATED BY '\n' STARTING BY ''");
                    $write->query("DELETE FROM " . $tableName . " where Offer_Code='Offer_Code'");
                    $write->query("UPDATE import_notification_history SET start_date=NOW(),email_notification='No',result_count='" . $argv[1] . "' WHERE id=" . $InventoryFeedId);
                } catch (Exception $e) {
                    $log_array[0]['Message'] = $e->getMessage();
                    $log_array[0]['Trace'] = $e->getTraceAsString();
                    $log_array[0]['Info'] = "Something went wrong while importing from " . $txFileName . " file.";
                }
            }

            $totalResults1 = "";

            $totalResults1 = $connection->select()
                    ->from('delete_keycodes as P', array('count(*) as count','P.Offer_Code'));

            $totalResultsCount1 = $connection->fetchAll($totalResults1);

            if ($start < count($totalResultsCount1)) {
                if ($start == 0) {
                    $priceSelect = $connection->select()
                        ->from('delete_keycodes as P', array( 'P.Offer_Code','P.keycode'))
                        ->limit($limit);
                } else {
                    $priceSelect = $connection->select()
                        ->from('delete_keycodes as P', array('P.Offer_Code','P.keycode'))
                        ->limit($limit, $start);
                }
                $priceRowsArray = $connection->fetchAll($priceSelect);
                $error_inc = 1;
                $res_cnt = count($priceRowsArray);
                $inc_val = 0;
                foreach($priceRowsArray as $deleterow){
                      //if($deleterow['Offer_Code']!='4'){
                           $delOfferCode = trim($deleterow['Offer_Code']);
                           $delKeyCode = trim($deleterow['keycode']);
                           $qry =" SELECT COUNT(*) FROM  keycodes WHERE Offer_Code ='".$delOfferCode."' AND Keycode='".$delKeyCode."'";
                           $priceResult = $write->fetchOne($qry);
                            try {
                                $resource = Mage::getSingleton('core/resource');
                                $keycodetable = $resource->getTableName('salesrule/rule') . '_coupon';
                                $magentoKeycodes = $write->fetchAll("SELECT SRC.coupon_id,SR.rule_id FROM salesrule SR INNER JOIN ".$keycodetable." SRC ON SRC.rule_id=SR.rule_id WHERE SR.offer_code='".$delOfferCode."' AND SRC.code='".$delKeyCode."' ");                                    //Deleting the keycodes from Magento table$delKeyCode
                                foreach($magentoKeycodes as $mKeycode){
                                    //Deleting keycode from magento table
                                    $couponId= $mKeycode['coupon_id'];
                                    $ruleId= trim($mKeycode['rule_id']);
                                    if(!empty($couponId)){
                                        //$write->query("DELETE FROM ".$keycodetable." WHERE coupon_id='".$couponId."'");
                                        //delete rule w.r.t keycode and offer_code combination
                                        $objRule = Mage::getModel('salesrule/rule')->load($ruleId);
                                        $objRule->delete();
                                    }
                                }
                                if($priceResult > 0){
                                    //Delete custom table keycode
                                    $write->query("DELETE FROM keycodes WHERE Offer_Code='".$delOfferCode."' AND Keycode='".$delKeyCode."'");
                                }
                                echo "Deleted Keycode".$delKeyCode." Offer_Code===>".$delOfferCode.'\n';
                            }catch (Exception $e) {
                                $log_array[$error_inc]['Message'] = $e->getMessage();
                                $log_array[$error_inc]['Trace'] = $e->getTraceAsString();
                                $log_array[$error_inc]['Info'] = "Something went wrong while deleting price.";
                            }
                      //}
                    $error_inc++;
                    $inc_val++;
                }
                $start = $start + $limit;
                KeycodeDeleteCron($start);
            } else {
                $error_inc1 = $res_cnt + 1;
                try {
                    if (copy($txtPriceFile, $txtArchivePrice)) {
                        unlink($txtPriceFile);
                    }
                } catch (Exception $e) {
                    $log_array[$error_inc1 + 1]['Message'] = $e->getMessage();
                    $log_array[$error_inc1 + 1]['Trace'] = $e->getTraceAsString();
                    $log_array[$error_inc1 + 1]['Info'] = "Something went wrong while archive the " . $tableName . " file.";
                }

                try {
                    $produ_desc_file = $dir_import . "ProductDescription.txt";
                    $inventory_file = $dir_import . "Inventory.txt";
                    $inventory_feed_file = $dir_import . "InventoryFeed.txt";
                    $price_file = $dir_import . "Price.txt";
                    $catalog_offer_file = $dir_import . "CatalogOffers.txt";
                    $keycode_file = $dir_import . "KeyCodes.txt";
                    $shipping_table_file = $dir_import . "ShippingTables.txt";
                    $delete_Price = $dir_import . "DeletePrice.txt";
                    $delete_keycodes = $dir_import . "DeleteKeyCodes.txt";
                    $delete_Shipping = $dir_import . "DeleteShippingDetails.txt";

                    if (!file_exists($produ_desc_file) &&
                            !file_exists($inventory_file) &&
                            !file_exists($inventory_feed_file) &&
                            !file_exists($price_file) &&
                            !file_exists($catalog_offer_file) &&
                            !file_exists($keycode_file) &&
                            !file_exists($shipping_table_file) &&
                            !file_exists($delete_Price)&&
                            !file_exists($delete_keycodes)&&
                            !file_exists($delete_Shipping)) {
                        unlink($txtFlagFile);
                    }
                } catch (Exception $e) {
                    $log_array[$error_inc1 + 2]['Message'] = $e->getMessage();
                    $log_array[$error_inc1 + 2]['Trace'] = $e->getTraceAsString();
                    $log_array[$error_inc1 + 2]['Info'] = "Something went wrong while removing flag file.";
                }
                try {
                    /* $write = Mage::getSingleton('core/resource')->getConnection('core_write');
                      $write->delete($tableName); */
                    if (empty($start))
                        $a = "empty";
                    else
                        $a=$start;
                    $write = Mage::getSingleton('core/resource')->getConnection('core_write');
                    $write->delete("delete_keycodes");

                    $write->query("UPDATE import_notification_history SET status='notStarted', end_date=NOW(),result_count='" . $argv[1] . "' WHERE import_type='deleteKeycodes' and status='inProgress'");
                } catch (Exception $e) {
                    $log_array[$error_inc1 + 3]['Message'] = $e->getMessage();
                    $log_array[$error_inc1 + 3]['Trace'] = $e->getTraceAsString();
                    $log_array[$error_inc1 + 3]['Info'] = "Something went wrong while deleting the records from " . $tableName . " table.";
                }
            }
//DELETE THE ARCHIVE FILES OLDER THAN 30 Days
            if ($handle = opendir($dir_Archive)) {
                while (false !== ($entry = readdir($handle))) {
                    if ($entry != "." && $entry != "..") {
                        $arrFiles = explode("_", $entry);

                        $fileTimeStamp = $arrFiles[0];
                        $date1 = date('YmdHis');
                        $date2 = $fileTimeStamp;

                        $ts1 = strtotime($date1);
                        $ts2 = strtotime($date2);
                        $seconds_diff = $ts1 - $ts2;
                        $days = floor($seconds_diff / 3600 / 24);
                        try {
//Delete the files older than 30 days
                            if ($days > 30) {
                                unlink($dir_Archive . $entry);
                            }
                        } catch (Exception $e) {
                            $log_array[$error_inc1 + 4]['Message'] = $e->getMessage();
                            $log_array[$error_inc1 + 4]['Trace'] = $e->getTraceAsString();
                            $log_array[$error_inc1 + 4]['Info'] = "Something went wrong while deleting 30 days old archive files.";
                        }
                    }
                }
                closedir($handle);
            }
            //ERROR LOG
            if (count($log_array) > 0) {
                $errorMsg = '';
                foreach ($log_array as $log) {
                    $errorMsg .= '
          - - - - - - - -
          ERROR
          SUBJECT: ' . $log['Info'] . '
          Message: ' . $log['Message'] . '
          Trace: ' . $log['Trace'] . '
          ProductId: ' . $log['sku'] . '
          Time: ' . date('Y m d H:i:s') . '
          ';
                    echo "\r\n" . $errorMsg;
                }
                file_put_contents($dir_Archive . '/logs/log_' . time() . '.txt', $errorMsg);
                //Getting admin Information
                $adminInfo = $write->fetchAll("SELECT * from admin_user");
                $userFirstname = $adminInfo[0]['firstname'];
                $userLastname = $adminInfo[0]['lastname'];
                $userEmail = $adminInfo[0]['email'];
                //$arrTo[] = array("toEmail" => $userEmail, "toName" => $userFirstname . ' ' . $userLastname);

                $mail = new Zend_Mail();
                $mail->setType(Zend_Mime::MULTIPART_RELATED);
                $mail->setBodyHtml('There were errors updating the Price for Spring Hill. Please review the log files at: ' . $_SERVER['REMOTE_ADDR'] . '/dailyfeed/archive/logs/log_' . time() . '.txt');
                //$mail->setFrom($userEmail, $userFirstname . ' ' . $userLastname);
                //SENDING MAIL TO Janesh,imsupport@gardensalive.com and Magento Admin
                /* foreach ($arrTo as $to) {
                  $mail->addTo($to['toEmail'], $to['toName']);
                  } */
                $mail->setFrom('mohansaip@kensium.com', 'Mohan sai');
                $mail->addTo('mohansaip@kensium.com', 'Mohan sai');
                $mail->addTo('sudhakark@kensium.com', 'Sudhakar');
                $mail->setSubject('Error updating SH product database');
//            $fileContents = file_get_contents($dir_Archive . '/logs/log_' . time() . '.txt');
//            $file = $mail->createAttachment($fileContents);
//            $file->filename = "log_" . time() . ".txt";
                try {
                    if ($mail->send()) {
                        Mage::getSingleton('core/session')->addSuccess('Log Information has been mailed');
                    }
                } catch (Exception $ex) {
                    Mage::getSingleton('core/session')->addSuccess('Unable to send mail');
                }
            }
        }
        return TRUE;
    } else {
        return FALSE;
    }
}

