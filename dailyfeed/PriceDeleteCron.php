<?php
/*require_once '../app/Mage.php';
umask(0);
Mage::app('default');
global $global_dir_Archive;
$global_dir_Archive = "/var/www/springhill/dailyfeed/archive/";
////include 'web_conf.php';
priceDeleteCron();*/
function priceDeleteCron($start=0) {
    $log_array = array();
    $argv[1] = $start;
    $write = Mage::getSingleton('core/resource')->getConnection('core_write');

    $InventoryStatus = $write->fetchAll("SELECT status FROM import_notification_history 
    WHERE import_type='price' or import_type='inventoryFeed' or import_type='inventory' or import_type='catalogOffers' or import_type='productDescription' or  import_type='deleteKeycodes' or import_type='deleteShipping' ");
    if ($InventoryStatus[0]['status'] != "inProgress" &&
            $InventoryStatus[1]['status'] != "inProgress" &&
            $InventoryStatus[2]['status'] != "inProgress" &&
            $InventoryStatus[3]['status'] != "inProgress" && $InventoryStatus[4]['status'] !="inProgress" && $InventoryStatus[5]['status'] !="inProgress" && $InventoryStatus[6]['status'] !="inProgress")  {
//Mail to be sent for
        //$arrTo[] = array("toEmail" => "amitblues123@gmail.com", "toName" => "imsupport");
        //$arrTo[] = array("toEmail" => "mohansaip@kensium.com", "toName" => "Janesh");
//Semaphore Flag
        $txtFlagFile = '/home/springhill/UploadComplete.txt';

//Directory path where import files are located
        $dir_import = "/home/springhill/";

//Directory Path where Import files to be moved after successful import
        $dir_Archive = $GLOBALS["global_dir_Archive"];

        $txFileName = "DeletePrice.txt";

        $timestamp = date("YmdHis") . "_";

        $txtArchivePrice = $dir_Archive . $timestamp . $txFileName;

        $tableName = "delete_price";

        $txtPriceFile = $dir_import . $txFileName;

        $write = Mage::getSingleton('core/resource')->getConnection('core_write');

        $InventoryId = $write->fetchOne("SELECT id FROM import_notification_history WHERE import_type='deletePrice' and status='notStarted'");

//Checking flag file and then proceeding for Inventory Update

        if (file_exists($txtFlagFile) && file_exists($txtPriceFile) && ($argv[1] == 0 && !empty($InventoryId) || ($argv[1] != 0 && empty($InventoryId)))) {
            $limit = 100;
            //Saving the lastupdated time in database for 36 hours email notification
            $InventoryFeedId = $write->fetchOne("SELECT id FROM import_notification_history WHERE import_type='deletePrice'");
            if (empty($InventoryFeedId)) {
                $write->query("INSERT INTO import_notification_history(import_type,status) VALUES('deletePrice','inProgress')");
            } else {
                $write->query("UPDATE import_notification_history SET status='inProgress',result_count='" . $argv[1] . "' WHERE id=" . $InventoryFeedId);
            }

//Get the limit to be imported by default 100        

            $connection = Mage::getSingleton('core/resource')->getConnection('core_read');

            if (file_exists($txtPriceFile) && empty($argv[1])) {
                try {
                    $write = Mage::getSingleton('core/resource')->getConnection('core_write');
                    $write->delete($tableName);

                    $write->query("LOAD DATA LOCAL INFILE '" . $txtPriceFile . "' INTO TABLE " . $tableName . "
FIELDS TERMINATED BY '|' ENCLOSED BY '' 
LINES TERMINATED BY '\n' STARTING BY ''");
                    $write->query("DELETE FROM " . $tableName . " where Product_Number='Product_Number'");
                    $write->query("UPDATE import_notification_history SET start_date=NOW(),email_notification='No',result_count='" . $argv[1] . "' WHERE id=" . $InventoryFeedId);
                } catch (Exception $e) {
                    $log_array[0]['Message'] = $e->getMessage();
                    $log_array[0]['Trace'] = $e->getTraceAsString();
                    $log_array[0]['Info'] = "Something went wrong while importing from " . $txFileName . " file.";
                }
            }

            $totalResults1 = "";

            $totalResults1 = $connection->select()
                    ->from('delete_price as P', array('count(*) as count', 'P.Product_Number', 'P.Offer_Code'));

            $totalResultsCount1 = $connection->fetchAll($totalResults1);

            if ($start < count($totalResultsCount1)) {
                if ($start == 0) {
                    $priceSelect = $connection->select()
                        ->from('delete_price as P', array( 'P.Product_Number', 'P.Offer_Code'))
                        ->limit($limit);
                } else {
                    $priceSelect = $connection->select()
                        ->from('delete_price as P', array('P.Product_Number', 'P.Offer_Code'))
                        ->limit($limit, $start);
                }
                $priceRowsArray = $connection->fetchAll($priceSelect);
                $error_inc = 1;
                $res_cnt = count($priceRowsArray);
                $inc_val = 0;
                foreach($priceRowsArray as $deleterow){
                      if(trim($deleterow['Offer_Code'])!='4'){
                           $delProductNumber = trim($deleterow['Product_Number']);
                           $delOfferCode = trim($deleterow['Offer_Code']);
                           $qry =" SELECT COUNT(*) FROM  price WHERE Product_Number='".$delProductNumber."' AND  Offer_Code ='".$delOfferCode."'";
                           $priceResult = $write->fetchOne($qry);
                            if($priceResult > 0){
                                try {
                                    //Deleting the Price from price table
                                    $write->query("DELETE FROM price WHERE Product_Number='".$delProductNumber."' AND Offer_Code='".$delOfferCode."'");
                                    echo "SKU====>".$delProductNumber."Offer_Code===>".$delOfferCode.'\n';
                                }catch (Exception $e) {
                                    $log_array[$error_inc]['Message'] = $e->getMessage();
                                    $log_array[$error_inc]['Trace'] = $e->getTraceAsString();
                                    $log_array[$error_inc]['Info'] = "Something went wrong while deleting price.";
                                }
                            }
                      }
                    $error_inc++;
                    $inc_val++;
                }
                $start = $start + $limit;
                priceDeleteCron($start);
            } else {
                $error_inc1 = $res_cnt + 1;
                try {
                    if (copy($txtPriceFile, $txtArchivePrice)) {
                        unlink($txtPriceFile);
                    }
                } catch (Exception $e) {
                    $log_array[$error_inc1 + 1]['Message'] = $e->getMessage();
                    $log_array[$error_inc1 + 1]['Trace'] = $e->getTraceAsString();
                    $log_array[$error_inc1 + 1]['Info'] = "Something went wrong while archive the " . $tableName . " file.";
                }

                try {
                    $produ_desc_file = $dir_import . "ProductDescription.txt";
                    $inventory_file = $dir_import . "Inventory.txt";
                    $inventory_feed_file = $dir_import . "InventoryFeed.txt";
                    $price_file = $dir_import . "Price.txt";
                    $catalog_offer_file = $dir_import . "CatalogOffers.txt";
                    $keycode_file = $dir_import . "KeyCodes.txt";
                    $shipping_table_file = $dir_import . "ShippingTables.txt";
                    $delete_price = $dir_import . "DeletePrice.txt";
                    $delete_keycodes = $dir_import . "DeleteKeyCodes.txt";
                    $delete_shipping = $dir_import . "DeleteShippingDetails.txt";

                    if (!file_exists($produ_desc_file) &&
                            !file_exists($inventory_file) &&
                            !file_exists($inventory_feed_file) &&
                            !file_exists($price_file) &&
                            !file_exists($catalog_offer_file) &&
                            !file_exists($keycode_file) &&
                            !file_exists($shipping_table_file) &&
                            !file_exists($delete_price)&&
                            !file_exists($delete_keycodes)&&
                            !file_exists($delete_shipping)) {
                        unlink($txtFlagFile);
                    }
                } catch (Exception $e) {
                    $log_array[$error_inc1 + 2]['Message'] = $e->getMessage();
                    $log_array[$error_inc1 + 2]['Trace'] = $e->getTraceAsString();
                    $log_array[$error_inc1 + 2]['Info'] = "Something went wrong while removing flag file.";
                }
                try {
                    /* $write = Mage::getSingleton('core/resource')->getConnection('core_write');
                      $write->delete($tableName); */
                    if (empty($start))
                        $a = "empty";
                    else
                        $a=$start;
                    $write = Mage::getSingleton('core/resource')->getConnection('core_write');
                    $write->delete("delete_price");

                    $write->query("UPDATE import_notification_history SET status='notStarted', end_date=NOW(),result_count='" . $argv[1] . "' WHERE import_type='deletePrice' and status='inProgress'");
                } catch (Exception $e) {
                    $log_array[$error_inc1 + 3]['Message'] = $e->getMessage();
                    $log_array[$error_inc1 + 3]['Trace'] = $e->getTraceAsString();
                    $log_array[$error_inc1 + 3]['Info'] = "Something went wrong while deleting the records from " . $tableName . " table.";
                }
            }
//DELETE THE ARCHIVE FILES OLDER THAN 30 Days
            if ($handle = opendir($dir_Archive)) {
                while (false !== ($entry = readdir($handle))) {
                    if ($entry != "." && $entry != "..") {
                        $arrFiles = explode("_", $entry);

                        $fileTimeStamp = $arrFiles[0];
                        $date1 = date('YmdHis');
                        $date2 = $fileTimeStamp;

                        $ts1 = strtotime($date1);
                        $ts2 = strtotime($date2);
                        $seconds_diff = $ts1 - $ts2;
                        $days = floor($seconds_diff / 3600 / 24);
                        try {
//Delete the files older than 30 days
                            if ($days > 30) {
                                unlink($dir_Archive . $entry);
                            }
                        } catch (Exception $e) {
                            $log_array[$error_inc1 + 4]['Message'] = $e->getMessage();
                            $log_array[$error_inc1 + 4]['Trace'] = $e->getTraceAsString();
                            $log_array[$error_inc1 + 4]['Info'] = "Something went wrong while deleting 30 days old archive files.";
                        }
                    }
                }
                closedir($handle);
            }
            //ERROR LOG
            if (count($log_array) > 0) {
                $errorMsg = '';
                foreach ($log_array as $log) {
                    $errorMsg .= '
          - - - - - - - -
          ERROR
          SUBJECT: ' . $log['Info'] . '
          Message: ' . $log['Message'] . '
          Trace: ' . $log['Trace'] . '
          ProductId: ' . $log['sku'] . '
          Time: ' . date('Y m d H:i:s') . '
          ';
                    echo "\r\n" . $errorMsg;
                }
                file_put_contents($dir_Archive . '/logs/log_' . time() . '.txt', $errorMsg);
                //Getting admin Information
                $adminInfo = $write->fetchAll("SELECT * from admin_user");
                $userFirstname = $adminInfo[0]['firstname'];
                $userLastname = $adminInfo[0]['lastname'];
                $userEmail = $adminInfo[0]['email'];
                //$arrTo[] = array("toEmail" => $userEmail, "toName" => $userFirstname . ' ' . $userLastname);

                $mail = new Zend_Mail();
                $mail->setType(Zend_Mime::MULTIPART_RELATED);
                $mail->setBodyHtml('There were errors updating the Price for Spring Hill. Please review the log files at: ' . $_SERVER['REMOTE_ADDR'] . '/dailyfeed/archive/logs/log_' . time() . '.txt');
                //$mail->setFrom($userEmail, $userFirstname . ' ' . $userLastname);
                //SENDING MAIL TO Janesh,imsupport@gardensalive.com and Magento Admin
                /* foreach ($arrTo as $to) {
                  $mail->addTo($to['toEmail'], $to['toName']);
                  } */
                $mail->setFrom('mohansaip@kensium.com', 'Mohan sai');
                $mail->addTo('mohansaip@kensium.com', 'Mohan sai');
                $mail->addTo('sudhakark@kensium.com', 'Sudhakar');
                $mail->setSubject('Error updating SH product database');
//            $fileContents = file_get_contents($dir_Archive . '/logs/log_' . time() . '.txt');
//            $file = $mail->createAttachment($fileContents);
//            $file->filename = "log_" . time() . ".txt";
                try {
                    if ($mail->send()) {
                        Mage::getSingleton('core/session')->addSuccess('Log Information has been mailed');
                    }
                } catch (Exception $ex) {
                    Mage::getSingleton('core/session')->addSuccess('Unable to send mail');
                }
            }
        }
        return TRUE;
    } else {
        return FALSE;
    }
}

