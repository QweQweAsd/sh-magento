<?php

require_once '/var/www/magento/app/Mage.php';
umask(0);
Mage::app('default');
global $global_server_path, $global_server_image_path;
$global_server_path = "http://www.springhillnursery.com/";
$global_server_image_path = $global_server_path."media/catalog/product"; ///B/l/Black_Eyed_Susan_Daylily.jpg";

$connection = Mage::getSingleton('core/resource')->getConnection('core_read');

$limit = 100;
$start = 0;
if (!empty($argv[1]))
    $start = $argv[1];
else
    $start=0;
if (empty($start)) {
    $totalResults = $connection->select()
            ->from('catalog_product_entity as cpe', array('cpe.entity_id', 'cpe.sku'))
            ->join('catalog_product_entity_int as cpei', 'cpe.entity_id=cpei.entity_id', array())
            ->where("cpe.sku!=''")
            ->where("cpei.attribute_id=96")
            ->where("cpei.value=1")
            ->where("cpe.sku NOT LIKE '%_VSPMAAC%'")
            ->limit($limit);
} else {
    $totalResults = $connection->select()
            ->from('catalog_product_entity as cpe', array('cpe.entity_id', 'cpe.sku'))
            ->join('catalog_product_entity_int as cpei', 'cpe.entity_id=cpei.entity_id', array())
            ->where("cpe.sku!=''")
            ->where("cpei.attribute_id=96")
            ->where("cpei.value=1")
            ->where("cpe.sku NOT LIKE '%_VSPMAAC%'")
            ->limit($limit, $start);
}
//print $totalResults;exit;
$totalResultsSet = $connection->fetchAll($totalResults);
$i = 0;
$totalData = array();
if (count($totalResultsSet) > 0) {
    foreach ($totalResultsSet as $key => $value) {
        try {
            //$product_id = Mage::getModel('catalog/product')->getIdBySku("$sku");
            if (!empty($value['entity_id']) && !strstr($value['sku'])) {// && is_numeric($value['sku'])) {
		
                $product_id = $value['entity_id'];
                $_Pdetails = Mage::getModel('catalog/product')->load($product_id);
                if($_Pdetails->getPrice()=="0.00")
		continue;
		if($_Pdetails->getDescription()=="")
		continue;
		$categories = $_Pdetails->getCategoryIds();
		if(empty($categories))
		continue;
		if($_Pdetails->getName()=="")
		continue;
                $totalData[$i]['unique_internal_code'] = trim($value['sku']);
                $totalData[$i]['product_name'] = str_replace("\t", " ", htmlspecialchars_decode($_Pdetails->getName()));
                $desc = str_replace("\n", "", $_Pdetails->getDescription());
                $desc1 = str_replace("\t", "", $desc);
                $desc2 = str_replace("\n\t", "", $desc1);
                $desc3 = str_replace("\t\n", "", $desc2);
                $desc4 = str_replace("\r", "", $desc3);
                $totalData[$i]['product_description'] = substr(trim($desc4),0,900);
		$productPrice=$_Pdetails->getPrice();               
		$totalData[$i]['product_price'] = number_format($productPrice, 2, '.', '');
                $totalData[$i]['product_url'] = $GLOBALS["global_server_path"] . $_Pdetails->getUrlPath();
                $totalData[$i]['image_url'] = $GLOBALS["global_server_image_path"] . $_Pdetails->getImage();

                $categories_list = getCategoriesNames($_Pdetails->getCategoryIds());
                /*print $value['sku'] . "--->" . $product_id;
                if (count($categories_list) > 0 && $value['sku']==2503) {
                    print_r($_Pdetails->getCategoryIds());
                    print_r($categories_list);
                    exit;
                }*/
                $totalData[$i]['category'] = implode(",", $categories_list);
                $totalData[$i]['manufacturer'] = 'Spring Hill Nurseries';

                $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product_id);
                $is_stock = ($stockItem->getIsInStock() == 0) ? "N" : "Y";

                $totalData[$i]['stock_status'] = $is_stock;
                $totalData[$i]['condition'] = "New";
                $totalData[$i]['mpn'] = $value['sku'];
                $totalData[$i]['isbn'] = "";
                $totalData[$i]['upc'] = "";
                $totalData[$i]['muze_id'] = "";
                $totalData[$i]['payment_type'] = "";
                $totalData[$i]['keywords'] = htmlspecialchars($_Pdetails->getSearchWords());
	        $msrp=number_format($productPrice, 2, '.', '');
                $totalData[$i]['msrp'] = $msrp;
                $totalData[$i]['sale_price'] = "";
                $totalData[$i]['shipping_cost'] = "7.95";
                $totalData[$i]['shipping_weight'] = "";
                $totalData[$i]['promotional_messagev'] = "";
                $totalData[$i]['format'] = "";
                $totalData[$i]['gender'] = "";
                $totalData[$i]['department'] = "";
                $totalData[$i]['size'] = $_Pdetails->getSpacing();
                $totalData[$i]['like_com_size'] = "";
                $totalData[$i]['color'] = htmlspecialchars($_Pdetails->getWebSearchFlowerColor());
                $totalData[$i]['age_range'] = "";
                $totalData[$i]['age_group'] = "";
                $totalData[$i]['availability'] = "";
                $totalData[$i]['stock_description'] = "";
                $totalData[$i]['nrf_size'] = "";
                $totalData[$i]['nrf_color'] = "";
                $totalData[$i]['distributor_id'] = "";
                $totalData[$i]['ingram_part'] = "";
                $totalData[$i]['asin'] = "";
                $totalData[$i]['zip_code'] = "45371";
                $totalData[$i]['outlet'] = "";
                $totalData[$i]['currency'] = "";
                $totalData[$i]['author'] = "";
                $totalData[$i]['publisher'] = "";
                $totalData[$i]['movie_director'] = "";
                $totalData[$i]['actor'] = "";
                $totalData[$i]['artist'] = "";
                $totalData[$i]['pickup'] = "";
                $totalData[$i]['price_type'] = "";
                $totalData[$i]['made_in'] = "";
                $totalData[$i]['material'] = "";
                $totalData[$i]['style'] = "";
                $totalData[$i]['binding'] = "";
                $totalData[$i]['edition'] = "";
                $totalData[$i]['genre'] = "";
                $totalData[$i]['pages'] = "";
                $totalData[$i]['focus_type'] = "";
                $totalData[$i]['resolution'] = "";
                $totalData[$i]['zoom'] = "";
                $totalData[$i]['megapixels'] = "";
                $totalData[$i]['memory'] = "";
                $totalData[$i]['processor_speed'] = "";
                $totalData[$i]['wireless_interface'] = "";
                $totalData[$i]['battery_life'] = "";
                $totalData[$i]['capacity'] = "";
                $totalData[$i]['operating_system'] = "";
                $totalData[$i]['optical_drive'] = "";
                $totalData[$i]['recommended_usage'] = "";
                $totalData[$i]['screen_size'] = "";
                $totalData[$i]['aspect_ratio'] = "";
                $totalData[$i]['display_type'] = "";
                $totalData[$i]['color_output'] = "";
                $totalData[$i]['memory_card_slot'] = "";
                $totalData[$i]['load_type'] = "";
                $totalData[$i]['feature'] = "";
                $totalData[$i]['functions'] = "";
                $totalData[$i]['tech_specification_link'] = "";
                $totalData[$i]['height'] = htmlspecialchars($_Pdetails->getHeightHabit());
                $totalData[$i]['length'] = "";
                $totalData[$i]['width'] = "";
                $totalData[$i]['installation'] = "";
                $totalData[$i]['occasion'] = "";
                $totalData[$i]['heel_height'] = "";
                $totalData[$i]['shoe_width'] = "";
                $totalData[$i]['game_platform'] = "";
                $totalData[$i]['google_shipping'] = "";
                $totalData[$i]['google_tax'] = "";
                $totalData[$i]['compatible_with'] = "";
                $totalData[$i]['location'] = "";
                $totalData[$i]['year'] = "";
                $totalData[$i]['product_type'] = "";
                $totalData[$i]['quantity'] = "";
                $totalData[$i]['google_expiration_date'] = "";
                $totalData[$i]['google_expiration_date_auto_renew'] = "";
                $totalData[$i]['subscription_duration'] = "";
                $totalData[$i]['frequency'] = "";
                $totalData[$i]['issues'] = "";
                $totalData[$i]['ean'] = "";
                $totalData[$i]['become_category'] = "";
                $totalData[$i]['nextag_category'] = "";
                $totalData[$i]['pricegrabber_category'] = "";
                $totalData[$i]['pronto_category'] = "";
                $totalData[$i]['shopping_com_category'] = "";
                $totalData[$i]['shopzilla_category'] = "";
                $totalData[$i]['smarter_category'] = "";
                $totalData[$i]['sortprice_category'] = "";
                $totalData[$i]['like_com_category'] = "";
                $totalData[$i]['shop_com_category'] = "";
                $totalData[$i]['become_url_replacement'] = $GLOBALS["global_server_path"] . $_Pdetails->getUrlPath() . "?sid=0433135";
                $totalData[$i]['buysafe_url_replacement'] = "";
                $totalData[$i]['google_base_url_replacement'] = "";
                $totalData[$i]['nextag_url_replacement'] = $GLOBALS["global_server_path"] . $_Pdetails->getUrlPath() . "?sid=0433132";
                $totalData[$i]['pricegrabber_url_replacement'] = $GLOBALS["global_server_path"] . $_Pdetails->getUrlPath() . "?sid=0433137";
                $totalData[$i]['pronto_url_replacement'] = "";
                $totalData[$i]['shopping_com_url_replacement'] = $GLOBALS["global_server_path"] . $_Pdetails->getUrlPath() . "?sid=0433134";
                $totalData[$i]['shopzilla_url_replacement'] = $GLOBALS["global_server_path"] . $_Pdetails->getUrlPath() . "?sid=0433136";
                $totalData[$i]['smarter_url_replacement'] = "";
                $totalData[$i]['thefind_url_replacement'] = $GLOBALS["global_server_path"] . $_Pdetails->getUrlPath() . "?sid=0433133";
                $totalData[$i]['yahoo_shopping_url_replacement'] = "";
                $totalData[$i]['sortprice_url_replacement'] = "";
                $totalData[$i]['like_com_url_replacement'] = "";
                $totalData[$i]['shop_com_url_replacement'] = "";
                $totalData[$i]['google_base_product_name_replacement'] = "";
                $totalData[$i]['google_base_product_description_replacement'] = "";
                $totalData[$i]['include_google_base'] = "Y";
                $totalData[$i]['include_amazon'] = "Y";
                $totalData[$i]['include_become'] = "Y";
                $totalData[$i]['include_buysafe'] = "Y";
                $totalData[$i]['include_cashback'] = "Y";
                $totalData[$i]['include_like_com'] = "Y";
                $totalData[$i]['include_nextag'] = "Y";
                $totalData[$i]['include_pricegrabber'] = "Y";
                $totalData[$i]['include_pronto'] = "Y";
                $totalData[$i]['include_powerreview'] = "Y";
                $totalData[$i]['include_shop_com'] = "Y";
                $totalData[$i]['include_shopping_com'] = "Y";
                $totalData[$i]['include_shopzilla'] = "Y";
                $totalData[$i]['include_smarter'] = "Y";
                $totalData[$i]['include_sortprice'] = "Y";
                $totalData[$i]['include_thefind'] = "Y";
                $totalData[$i]['include_yahoo_shopping'] = "Y";
                $totalData[$i]['brand'] = "SH";
                $totalData[$i]['adwords_queryparam'] = "CAWELAID=1246254500";
                $totalData[$i]['adwords_publish'] = "true";
            }
        } catch (Exception $e) {
            $log_array[0]['Message'] = $e->getMessage();
            $log_array[0]['Trace'] = $e->getTraceAsString();
            $log_array[0]['Info'] = "Something went wrong while exporting data form magento.";
        }
        $i++;
    }
    try {
        $myFile = "/var/www/magento/dailyfeed/singlefeed/SingleFeed--SH.txt";
        if (empty($start)) {
            unlink($myFile);
        }
        $fh = fopen($myFile, 'a+') or die("can't open file");
        if (empty($start)) {

            $heading = "UNIQUE INTERNAL CODE	PRODUCT NAME	PRODUCT DESCRIPTION	PRODUCT PRICE	PRODUCT URL	IMAGE URL	CATEGORY	MANUFACTURER	STOCK STATUS	CONDITION (IF NOT NEW)	MPN (MANUFACTURER PART NUMBER)	ISBN (IF BOOK)	UPC (UNIVERSAL PRODUCT CODE)	MUZE ID	PAYMENT TYPE	KEYWORDS	MSRP	SALE PRICE	SHIPPING COST	SHIPPING WEIGHT	PROMOTIONAL MESSAGE	FORMAT	GENDER	DEPARTMENT	SIZE	LIKE.COM SIZE	COLOR	AGE RANGE	AGE GROUP	AVAILABILITY	STOCK DESCRIPTION (IF BOOK/MUSIC/MOVIE)	NRF SIZE	NRF COLOR	DISTRIBUTOR ID	INGRAM PART #	ASIN	ZIP CODE	OUTLET	CURRENCY	AUTHOR	PUBLISHER	MOVIE DIRECTOR	ACTOR	ARTIST	PICKUP	PRICE TYPE	MADE_IN	MATERIAL	STYLE	BINDING	EDITION	GENRE	PAGES	FOCUS TYPE	RESOLUTION	ZOOM	MEGAPIXELS	MEMORY	PROCESSOR SPEED	WIRELESS INTERFACE	BATTERY LIFE	CAPACITY	OPERATING SYSTEM	OPTICAL DRIVE	RECOMMENDED USAGE	SCREEN SIZE	ASPECT RATIO	DISPLAY TYPE	COLOR OUTPUT	MEMORY CARD SLOT	LOAD TYPE	FEATURE	FUNCTIONS	TECH SPECIFICATION LINK	HEIGHT	LENGTH	WIDTH	INSTALLATION	OCCASION	HEEL HEIGHT	SHOE WIDTH	GAME PLATFORM	GOOGLE SHIPPING	GOOGLE TAX	COMPATIBLE WITH	LOCATION	YEAR	PRODUCT TYPE	QUANTITY	GOOGLE EXPIRATION DATE	GOOGLE EXPIRATION DATE AUTO RENEW	SUBSCRIPTION DURATION	FREQUENCY	ISSUES	EAN	BECOME CATEGORY	NEXTAG CATEGORY	PRICEGRABBER CATEGORY	PRONTO CATEGORY	SHOPPING.COM CATEGORY	SHOPZILLA CATEGORY	SMARTER CATEGORY	SORTPRICE CATEGORY	LIKE.COM CATEGORY	SHOP.COM CATEGORY	BECOME URL REPLACEMENT	BUYSAFE URL REPLACEMENT	GOOGLE BASE URL REPLACEMENT	NEXTAG URL REPLACEMENT	PRICEGRABBER URL REPLACEMENT	PRONTO URL REPLACEMENT	SHOPPING.COM URL REPLACEMENT	SHOPZILLA URL REPLACEMENT	SMARTER URL REPLACEMENT	THEFIND URL REPLACEMENT	YAHOO SHOPPING URL REPLACEMENT	SORTPRICE URL REPLACEMENT	LIKE.COM URL REPLACEMENT	SHOP.COM URL REPLACEMENT	GOOGLE BASE PRODUCT NAME REPLACEMENT	GOOGLE BASE PRODUCT DESCRIPTION REPLACEMENT	INCLUDE GOOGLE BASE	INCLUDE AMAZON	INCLUDE BECOME	INCLUDE BUYSAFE	INCLUDE CASHBACK	INCLUDE LIKE.COM	INCLUDE NEXTAG	INCLUDE PRICEGRABBER	INCLUDE PRONTO	INCLUDE POWERREVIEW	INCLUDE SHOP.COM	INCLUDE SHOPPING.COM	INCLUDE SHOPZILLA	INCLUDE SMARTER	INCLUDE SORTPRICE	INCLUDE THEFIND	INCLUDE YAHOO! SHOPPING	Brand	adwords_queryparam	adwords_publish\n";
            fwrite($fh, $heading);
        }
        foreach ($totalData as $id => $info) {
            $stringData = implode("\t", $info) . "\n";
            fwrite($fh, $stringData);
        }
        fclose($fh);
    } catch (Exception $e) {
        $log_array[0]['Message'] = $e->getMessage();
        $log_array[0]['Trace'] = $e->getTraceAsString();
        $log_array[0]['Info'] = "Something went wrong while exporting data form magento.";
    }
    $start = $start + $limit;
    exec('php -f /var/www/magento/dailyfeed/productExportCron.php ' . $start . ' ' . $limit);
} else {
    
}

//print "<pre>";
//print_r($totalData);

function getCategoriesNames($idsList) {
    $catagory_model = Mage::getModel('catalog/category');
    $list = array();
    if (count($idsList) > 0) {
        foreach ($idsList as $key => $value) {
            $categories = $catagory_model->load($value); // where $id will be the known category id
            if (!empty($categories)) {
                /* echo 'category name->' . $categories->getName(); //get category name
                  echo 'category image url->' . $categories->getImageUrl(); //get category image url
                  echo 'category url path->' . $categories->getUrlPath(); //get category url path */
                $list[] = $categories->getName();
            }
        }
    }
    return $list;
}

?>
