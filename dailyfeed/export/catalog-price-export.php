<?php
require_once '/var/www/html/springhill/app/Mage.php';
umask(0);
Mage::app('default');
global $global_dir_Archive;
global $global_server_path, $global_server_image_path;
$global_server_path = "http://www.springhillnursery.com/";
$global_dir_Archive = "/var/www/html/springhill/dailyfeed/singlefeed/";
$global_dir_ArchiveDir = "/var/www/html/springhill/dailyfeed/archive/";
$txFileName = "CatalogPriceExport.txt";
$timestamp = date("Ymd") . "_";
$dir_Archive = $GLOBALS["global_dir_Archive"];
$ProductFile = $dir_Archive . $timestamp . $txFileName;
$file =$timestamp . $txFileName;
$pricefile='';
$prevTime='';
$write = Mage::getSingleton('core/resource')->getConnection('core_write');
$query = "SELECT catalogprice_id, Product_Number, Offer_Code, Qty_Low, Qty_High, Price, Whlsl_Price, Sale_Price, Last_Update, Catalog_Type, FreeItm, FreeQty FROM price";
$fetchPrice = $write->fetchAll($query);
$i=0;
$catalogPrice =array();
foreach($fetchPrice as $price){

    $catalogPrice[$i]['catalogprice_id'] = $price['catalogprice_id'];
    $catalogPrice[$i]['Product_Number'] = $price['Product_Number'];
    $catalogPrice[$i]['Offer_Code'] = $price['Offer_Code'];
    $catalogPrice[$i]['Qty_Low'] = $price['Qty_Low'];
    $catalogPrice[$i]['Qty_High'] = $price['Qty_High'];
    $catalogPrice[$i]['Price'] = $price['Price'];
    $catalogPrice[$i]['Whlsl_Price'] = $price['Whlsl_Price'];
    $catalogPrice[$i]['Sale_Price'] = $price['Sale_Price'];
    $catalogPrice[$i]['Last_Update'] = $price['Last_Update'];
    $catalogPrice[$i]['Catalog_Type'] = $price['Catalog_Type'];
    $catalogPrice[$i]['FreeItm'] = $price['FreeItm'];
    $catalogPrice[$i]['FreeQty'] = $price['FreeQty'];
    $i++;
}

    try {
        $txFileName = "CatalogPriceExport.txt";
        $timestamp = date("Ymd") . "_";
        $dir_Archive = $GLOBALS["global_dir_Archive"];
        echo $CatalogFile = $dir_Archive . $timestamp . $txFileName;
        $file = $timestamp . $txFileName;
        if(file_exists($CatalogFile)){
            unlink($CatalogFile);
        }
        $fh = fopen($CatalogFile, 'a+') or die("can't open file");
        $header ="catalogprice_id|Product_Number|Offer_Code|Qty_Low|Qty_High|Price|Whlsl_Price|Sale_Price|Last_Update|Catalog_Type|FreeItm|FreeQty\r\n";
        fwrite($fh, $header);
        foreach ($catalogPrice as $id => $priceInfo) {
            $stringData = implode("|", $priceInfo) . "\n";
            fwrite($fh, $stringData);
        }
        fclose($fh);
        removeOldFiles($dir_Archive);

        //echo $CategoryFile."file is exported with all category details";
        echo '<a href="'.$global_server_path.'dailyfeed/singlefeed/'.$file.'">'.$file."</a>file is exported with all category details";
    } catch (Exception $e) {
        $log_array[0]['Message'] = $e->getMessage();
        $log_array[0]['Trace'] = $e->getTraceAsString();
        $log_array[0]['Info'] = "Something went wrong while exporting data form magento.";
    }


function removeOldFiles($dir_Archive){
    //Reading all the files from the archive directory for finding latest price file
    if ($handle = opendir($dir_Archive)) {
        while (false !== ($entry = readdir($handle))) {
            if ($entry != "." && $entry != "..") {
                //Check for price.txt extension
                if(strpos($entry, 'CatalogPriceExport.txt')){
                        $timestamp = str_replace('_CatalogPriceExport.txt','',$entry);
                        // get today and last 1 days time
                        $today = time();
                        $OneMonth = $today - (60*60*24*30);
                        $from = date("Ymd", $OneMonth);
                        if($from > $timestamp){
                            unlink($dir_Archive.$entry);
                        }

                }
            }
        }
        closedir($handle);
    }
}
?>
