<?php
require_once '/var/www/springhill/app/Mage.php';
umask(0);
Mage::app('default');

global $global_dir_Archive;
echo $global_dir_Archive = "/var/www/magento/dailyfeed/archive/";
priceCron(0);
function priceCron($start=0) {
    $log_array = array();
    $argv[1] = $start;
    $write = Mage::getSingleton('core/resource')->getConnection('core_write');

    $InventoryStatus = $write->fetchAll("SELECT status FROM import_notification_history 
    WHERE import_type='inventoryFeed' or import_type='inventory' or import_type='catalogOffers' or import_type='productDescription'  or  import_type='deleteKeycodes' or import_type='deletePrice' or import_type='deleteShipping' ");

    if ($InventoryStatus[0]['status'] != "inProgress" &&
            $InventoryStatus[1]['status'] != "inProgress" &&
            $InventoryStatus[2]['status'] != "inProgress" &&
            $InventoryStatus[3]['status'] != "inProgress" && $InventoryStatus[4]['status'] !="inProgress" && $InventoryStatus[5]['status'] !="inProgress" && $InventoryStatus[6]['status'] !="inProgress") {
//Mail to be sent for
        //$arrTo[] = array("toEmail" => "amitblues123@gmail.com", "toName" => "imsupport");
        //$arrTo[] = array("toEmail" => "mohansaip@kensium.com", "toName" => "Janesh");
//This URL IS USED FOR BATCH PROCESSING

        $CronURL = '../dailyfeed/updateCron.php';

//Semaphore Flag
        $txtFlagFile = '/home/vikasp/springhill/UploadComplete.txt';

//Directory path where import files are located
        $dir_import = "/home/vikasp/springhill/";

//Directory Path where Import files to be moved after successful import
        $dir_Archive = $GLOBALS["global_dir_Archive"];

        $txFileName = "Price.txt";

        $timestamp = date("YmdHis") . "_";

        $txtArchivePrice = $dir_Archive . $timestamp . $txFileName;

        $tableName = "price_temp";

        $txtPriceFile = $dir_import . $txFileName;

        $write = Mage::getSingleton('core/resource')->getConnection('core_write');

        $InventoryId = $write->fetchOne("SELECT id FROM import_notification_history WHERE import_type='price' and status='notStarted'");

//Checking flag file and then proceeding for Inventory Update

        if (file_exists($txtFlagFile) && file_exists($txtPriceFile) && ($argv[1] == 0 && !empty($InventoryId) || ($argv[1] != 0 && empty($InventoryId)))) {
            $limit = 100;
//            $start = 0;
//            //Get the Start Index for Import
//            if (!empty($argv[1]))
//                $start = $argv[1];
//            else {
//                $start = 0;
//            }
//Saving the lastupdated time in database for 36 hours email notification
            $InventoryFeedId = $write->fetchOne("SELECT id FROM import_notification_history WHERE import_type='price'");
            if (empty($InventoryFeedId)) {
                $write->query("INSERT INTO import_notification_history(import_type,status) VALUES('price','inProgress')");
            } else {
                $write->query("UPDATE import_notification_history SET status='inProgress',result_count='" . $argv[1] . "' WHERE id=" . $InventoryFeedId);
            }

//Get the limit to be imported by default 100        

            /*
             * Update Inventory values with Product info
             */

            $connection = Mage::getSingleton('core/resource')->getConnection('core_read');

            $totalResults = $connection->select()
                    ->from('price_temp', array('count(*) as count'));
            $totalResultsCount = $connection->fetchAll($totalResults);

            /*if (file_exists($txtPriceFile) && empty($argv[1])) {
                try {
                    $write = Mage::getSingleton('core/resource')->getConnection('core_write');
                    $write->delete($tableName);

                    $write->query("LOAD DATA LOCAL INFILE '" . $txtPriceFile . "' INTO TABLE " . $tableName . "
FIELDS TERMINATED BY '|' ENCLOSED BY '' 
LINES TERMINATED BY '\n' STARTING BY ''");
                    $write->exec("call spComparePrice()");
                    $write->query("DELETE FROM " . $tableName . " where Price='Price'");
                    $write->query("UPDATE import_notification_history SET start_date=NOW(),email_notification='No',result_count='" . $argv[1] . "' WHERE id=" . $InventoryFeedId);
                } catch (Exception $e) {
                    $log_array[0]['Message'] = $e->getMessage();
                    $log_array[0]['Trace'] = $e->getTraceAsString();
                    $log_array[0]['Info'] = "Something went wrong while importing from " . $txFileName . " file.";
                }
            }*/
            $totalResults1 = "";

            $totalResults1 = $connection->select()
                    ->from('price_temp as P', array('count(*) as count', 'P.Product_Number', 'P.Price','P.Sale_Price'))
                    ->join("catalog_product_entity AS cpe", "cpe.sku = P.Product_Number", array())
                    ->where('P.Offer_Code=4')
                    //->where('P.Product_Number IN (80357,80480,80411,80482,80401,80485,80432,80491,84415,64975,82517,80412,80418,80395,66738,21501)')
                    ->group('P.Product_Number');

           echo $totalResultsCount1 = $connection->fetchAll($totalResults1);


            if ($start < count($totalResultsCount1)) {
                if ($start == 0) {
                    $priceSelect = $connection->select()
                            ->from('price_temp as P', array('count(*) as count', 'P.Product_Number', 'P.Price','P.Sale_Price'))
                            ->join("catalog_product_entity AS cpe", "cpe.sku = P.Product_Number", array())
                            ->where('P.Offer_Code=4')
                            // ->where('P.Product_Number IN (80357,80480,80411,80482,80401,80485,80432,80491,84415,64975,82517,80412,80418,80395,66738,21501)')
                            ->group('P.Product_Number')
                            ->limit($limit);
                } else {
                    $priceSelect = $connection->select()
                            ->from('price_temp as P', array('count(*) as count', 'P.Product_Number', 'P.Price','P.Sale_Price'))
                            ->join("catalog_product_entity AS cpe", "cpe.sku = P.Product_Number", array())
                            ->where('P.Offer_Code=4')
                            // ->where('P.Product_Number IN (80357,80480,80411,80482,80401,80485,80432,80491,84415,64975,82517,80412,80418,80395,66738,21501)')
                            ->group('P.Product_Number')
                            ->limit($limit, $start);
                }

                $priceRowsArray = $connection->fetchAll($priceSelect);
                $UomQty = 0;
                $error_inc = 1;
                $res_cnt = count($priceRowsArray);
                $inc_val = 0;

                foreach ($priceRowsArray as $pKey => $pValue) {

                    $sku = $pValue['Product_Number'];
                    $salePrice = $pValue['Sale_Price'];
                    $mainPrice = $pValue['Price'];

                    if($salePrice){
                        $mainPrice = $salePrice;
                    }

                    //$_Pdetails = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);
                    $product_id = Mage::getModel('catalog/product')->getIdBySku("$sku");
                    $_Pdetails = Mage::getModel('catalog/product')->load($product_id);
                    $offersku = $sku . "_VSPMAAC";

                    //$product_id = $_Pdetails->getId();
                    //echo $psta = $_Pdetails->isSaleable()."\n";
                    if (1) {

                        $product_offer_id = Mage::getModel('catalog/product')->getIdBySku("$offersku");
                        $_Pdetails_offer = Mage::getModel('catalog/product')->load($product_offer_id);
                        if ($_Pdetails == null)
                            continue;

                        if ($_Pdetails_offer == null)
                            continue;

                        $UomQty = $_Pdetails->getUnitOfMeasure();

                        if (!empty($_Pdetails_offer)) {
                            $offer_product_id = $_Pdetails_offer->getId();
                            $offer_productdataVal = Mage::getModel('catalog/product')->load($offer_product_id);
                            $offer_UomQty = $offer_productdataVal->getUnitOfMeasure();
                        }

                        if ($pValue['count'] == 1) {
                            try {
                                $priceCalculated = 0;
                                $pOld = 0;
                                $pNew = 0;

                                if ($UomQty > 1)
                                    $priceCalculated = ($mainPrice / $UomQty); //number_format(($pValue['Price'] / $UomQty), 4, '.', '');
                                else
                                    $priceCalculated = $mainPrice;

                                $oldPrice = $_Pdetails->getPrice();
                                $oldOfferPrice = $offer_productdataVal->getPrice();

                                $pOld = number_format($oldPrice, 4, '.', '');
                                $pOfferOld = number_format($oldOfferPrice, 4, '.', '');
                                $pNew = number_format($priceCalculated, 4, '.', '');
                                //print "\n" . $inc_val . "===>" . $sku . "===>" . $pValue['Price'] . "==>" . $pOld . "===>" . $pNew . "===>" . $UomQty . "\n";

                                if ($pOld != $pNew) {
                                    $_Pdetails->setPrice($pNew);
                                    $_Pdetails->setSortPrice($mainPrice);
                                    $_Pdetails->save();
                                }
                                if ($pOfferOld != $pNew && $offer_product_id) {
                                    $offer_productdataVal->setPrice($pNew);
                                    $offer_productdataVal->setSortPrice($mainPrice);
                                    $offer_productdataVal->save();
                                }
                                //echo $_Pdetails['sku'] . "Updated1*********";
                            } catch (Exception $e) {
                                $log_array[$error_inc]['Message'] = $e->getMessage();
                                $log_array[$error_inc]['Trace'] = $e->getTraceAsString();
                                $log_array[$error_inc]['Info'] = "Something went wrong while updating price1.";
                                //print "\nSomething went wrong while updating price1.\n";
                            }
                        } else if ($pValue['count'] > 1) {

                            $pSubSelect = $connection->select()
                                    ->from('price_temp', array('Price','Sale_Price','Qty_Low'))
                                    ->where("Product_Number =" . $sku)
                                    ->where("Offer_Code=4");
                            $psubRowsArray = $connection->fetchAll($pSubSelect);

                            $j = 1;
                            $tierPrices = array();
                            foreach ($psubRowsArray as $id => $info) {
                                $tierMainPrice = $info['Price'];
                                $tierSalePrice = $info['Sale_Price'];
                                if($tierSalePrice){
                                    $tierMainPrice = $tierSalePrice;
                                }
                                if ($j == 1) {
                                    $priceCalculated1 = 0;
                                    $qtylow = 0;
                                    $orgi_qty = 0;
                                    $pOld = 0;
                                    $pNew = 0;


                                    if ($UomQty > 1)
                                        $priceCalculated1 = ($tierMainPrice / $UomQty);
                                    else
                                        $priceCalculated1 = $tierMainPrice;

                                    try {


                                        $oldPrice = $_Pdetails->getPrice();
                                        $oldOfferPrice = $offer_productdataVal->getPrice();
                                        $pOfferOld = number_format($oldOfferPrice, 4, '.', '');
                                        $pOld = number_format($oldPrice, 4, '.', '');
                                        $pNew = number_format($priceCalculated1, 4, '.', '');
                                        //print "\nsecond--->" . $UomQty1 . "===>" . $info['Price'] . "==>" . $pOld . "===>" . $pNew . "===>" . $UomQty1 . "===>" . $priceCalculated1 . "\n";
                                        //print "\n second--->" . $inc_val . "===>" . $sku . "===>" . $oldPrice . "====>" . $priceCalculated1 . "===>" . $pOld . "===>" . $pNew . "===>" . $UomQty;
                                        //echo $pOld ."!=". $pNew;
                                        if ($pOld != $pNew) {
                                            $_Pdetails->setPrice($pNew);
                                            $_Pdetails->save();
                                        }
                                        if ($pOfferOld != $pNew && $offer_product_id) {
                                            $offer_productdataVal->setPrice($pNew);
                                            $offer_productdataVal->save();
                                        }

                                        //echo $_Pdetails['sku'] . "Updated2*********";
                                    } catch (Exception $e) {
                                        $log_array[$error_inc]['Message'] = $e->getMessage();
                                        $log_array[$error_inc]['Trace'] = $e->getTraceAsString();
                                        $log_array[$error_inc]['Info'] = "Something went wrong while updating price2.";
                                        //print "\nSomething went wrong while updating price2.\n";
                                    }
                                } else {

                                    $qtyLow = 0;
                                    $priceCalculated2 = 0;

                                    if ($UomQty > 1) {
                                        $qtyLow = $info['Qty_Low'] * $UomQty;
                                        $priceCalculated2 = $tierMainPrice / $UomQty;
                                    } else {
                                        $qtyLow = $info['Qty_Low'];
                                        $priceCalculated2 = $tierMainPrice;
                                    }
                                    $priceCalculated2 = number_format($priceCalculated2, 4);
                                    $tierPrices[] = array(
                                        'website_id' => 'all',
                                        'cust_group' => 'all',
                                        'price_qty' => $qtyLow,
                                        'price' => $priceCalculated2
                                    );
                                }
                                $j++;
                            }

                            if (!empty($tierPrices) && count($tierPrices) > 0) {
                                try {
                                    $dbc = Mage::getSingleton('core/resource')->getConnection('core_write');
                                    $resource = Mage::getSingleton('core/resource');
                                    $table = $resource->getTableName('catalog/product') . '_tier_price';

                                    $dbc->query("DELETE FROM $table WHERE entity_id = $product_id");
                                    if ($_Pdetails_offer && $offer_product_id) {
                                        $dbc->query("DELETE FROM $table WHERE entity_id = $offer_product_id");
                                    }
                                    foreach ($tierPrices as $k => $v) {
                                        $dbc->query("INSERT INTO $table (entity_id,all_groups,customer_group_id,qty,value,website_id)
    VALUES ('" . $product_id . "',32000,0," . $v['price_qty'] . ",'" . $v['price'] . "',0)");
                                        if ($_Pdetails_offer && $offer_product_id) {
                                            $dbc->query("INSERT INTO $table (entity_id,all_groups,customer_group_id,qty,value,website_id)
    VALUES ('" . $offer_product_id . "',32000,0," . $v['price_qty'] . ",'" . $v['price'] . "',0)");
                                        }
                                    }
                                } catch (Exception $e) {
                                    $log_array[$error_inc]['Message'] = $e->getMessage();
                                    $log_array[$error_inc]['Trace'] = $e->getTraceAsString();
                                    $log_array[$error_inc]['Info'] = "Something went wrong while updating tier price.";
                                    //print "\nSomething went wrong while updating tier price.\n";
                                }
                            }
                        }
                        print $sku . "===>" . $inc_val . "\n";
                        $error_inc++;
                        $inc_val++;
                    }
                }
                //print_r($log_array);
                $start = $start + $limit;
                priceCron($start);
                //exec('php PriceUpdateCron.php ' . $start . ' ' . $limit);
            } else {
                $error_inc1 = $res_cnt + 1;
                try {
                    if (copy($txtPriceFile, $txtArchivePrice)) {
                        unlink($txtPriceFile);
                    }
                } catch (Exception $e) {
                    $log_array[$error_inc1 + 1]['Message'] = $e->getMessage();
                    $log_array[$error_inc1 + 1]['Trace'] = $e->getTraceAsString();
                    $log_array[$error_inc1 + 1]['Info'] = "Something went wrong while archive the " . $tableName . " file.";
                }

                try {
                    $produ_desc_file = $dir_import . "ProductDescription.txt";
                    $inventory_file = $dir_import . "Inventory.txt";
                    $inventory_feed_file = $dir_import . "InventoryFeed.txt";
                    $price_file = $dir_import . "Price.txt";
                    $catalog_offer_file = $dir_import . "CatalogOffers.txt";
                    $keycode_file = $dir_import . "KeyCodes.txt";
                    $shipping_table_file = $dir_import . "ShippingTables.txt";
                    $delete_Price = $dir_import . "DeletePrice.txt";
                    $delete_keycodes = $dir_import . "DeleteKeyCodes.txt";
                    $delete_Shipping = $dir_import . "DeleteShippingDetails.txt";

                    if (!file_exists($produ_desc_file) &&
                            !file_exists($inventory_file) &&
                            !file_exists($inventory_feed_file) &&
                            !file_exists($price_file) &&
                            !file_exists($catalog_offer_file) &&
                            !file_exists($keycode_file) &&
                            !file_exists($shipping_table_file) &&
                            !file_exists($delete_Price)&&
                            !file_exists($delete_keycodes)&&
                            !file_exists($delete_Shipping)) {
                        unlink($txtFlagFile);
                        /* for ($i = 1; $i < 9; $i++) {
                          $process = Mage::getModel('index/process')->load($i);
                          $process->reindexAll();
                          } */
                    }
                } catch (Exception $e) {
                    $log_array[$error_inc1 + 2]['Message'] = $e->getMessage();
                    $log_array[$error_inc1 + 2]['Trace'] = $e->getTraceAsString();
                    $log_array[$error_inc1 + 2]['Info'] = "Something went wrong while removing flag file.";
                }
                try {
                    /* $write = Mage::getSingleton('core/resource')->getConnection('core_write');
                      $write->delete($tableName); */
                    if (empty($start))
                        $a = "empty";
                    else
                        $a=$start;
                    $write = Mage::getSingleton('core/resource')->getConnection('core_write');
                    $write->delete("price_temp");

                    $write->query("UPDATE import_notification_history SET status='notStarted', end_date=NOW(),result_count='" . $argv[1] . "' WHERE import_type='price' and status='inProgress'");
                } catch (Exception $e) {
                    $log_array[$error_inc1 + 3]['Message'] = $e->getMessage();
                    $log_array[$error_inc1 + 3]['Trace'] = $e->getTraceAsString();
                    $log_array[$error_inc1 + 3]['Info'] = "Something went wrong while deleting the records from " . $tableName . " table.";
                }
            }
//DELETE THE ARCHIVE FILES OLDER THAN 30 Days
            if ($handle = opendir($dir_Archive)) {
                while (false !== ($entry = readdir($handle))) {
                    if ($entry != "." && $entry != "..") {
                        $arrFiles = explode("_", $entry);

                        $fileTimeStamp = $arrFiles[0];
                        $date1 = date('YmdHis');
                        $date2 = $fileTimeStamp;

                        $ts1 = strtotime($date1);
                        $ts2 = strtotime($date2);
                        $seconds_diff = $ts1 - $ts2;
                        $days = floor($seconds_diff / 3600 / 24);
                        try {
//Delete the files older than 30 days
                            if ($days > 30) {
                                unlink($dir_Archive . $entry);
                            }
                        } catch (Exception $e) {
                            $log_array[$error_inc1 + 4]['Message'] = $e->getMessage();
                            $log_array[$error_inc1 + 4]['Trace'] = $e->getTraceAsString();
                            $log_array[$error_inc1 + 4]['Info'] = "Something went wrong while deleting 30 days old archive files.";
                        }
                    }
                }
                closedir($handle);
            }
//ERROR LOG
//ERROR LOG
//        print "<pre>";
//        print_r($log_array);
            if (count($log_array) > 0) {
                $errorMsg = '';
                foreach ($log_array as $log) {
                    $errorMsg .= '
          - - - - - - - -
          ERROR
          SUBJECT: ' . $log['Info'] . '
          Message: ' . $log['Message'] . '
          Trace: ' . $log['Trace'] . '
          ProductId: ' . $log['sku'] . '
          Time: ' . date('Y m d H:i:s') . '
          ';
                    echo "\r\n" . $errorMsg;
                }
                file_put_contents($dir_Archive . '/logs/log_' . time() . '.txt', $errorMsg);
                //Getting admin Information
                $adminInfo = $write->fetchAll("SELECT * from admin_user");
                $userFirstname = $adminInfo[0]['firstname'];
                $userLastname = $adminInfo[0]['lastname'];
                $userEmail = $adminInfo[0]['email'];
                //$arrTo[] = array("toEmail" => $userEmail, "toName" => $userFirstname . ' ' . $userLastname);

                $mail = new Zend_Mail();
                $mail->setType(Zend_Mime::MULTIPART_RELATED);
                $mail->setBodyHtml('There were errors updating the Price for Spring Hill. Please review the log files at: ' . $_SERVER['REMOTE_ADDR'] . '/dailyfeed/archive/logs/log_' . time() . '.txt');
                //$mail->setFrom($userEmail, $userFirstname . ' ' . $userLastname);
                //SENDING MAIL TO Janesh,imsupport@gardensalive.com and Magento Admin
                /* foreach ($arrTo as $to) {
                  $mail->addTo($to['toEmail'], $to['toName']);
                  } */
                $mail->setFrom('mohansaip@kensium.com', 'Mohan sai');
                $mail->addTo('mohansaip@kensium.com', 'Mohan sai');
                $mail->addTo('sudhakark@kensium.com', 'Sudhakar');
                $mail->setSubject('Error updating SH product database');
//            $fileContents = file_get_contents($dir_Archive . '/logs/log_' . time() . '.txt');
//            $file = $mail->createAttachment($fileContents);
//            $file->filename = "log_" . time() . ".txt";
                try {
                    if ($mail->send()) {
                        Mage::getSingleton('core/session')->addSuccess('Log Information has been mailed');
                    }
                } catch (Exception $ex) {
                    Mage::getSingleton('core/session')->addSuccess('Unable to send mail');
                }
            }
        }
        return TRUE;
    } else {
        return FALSE;
    }
   // exit;
}


