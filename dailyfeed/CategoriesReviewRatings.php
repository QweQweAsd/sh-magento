<?php
/**
 * Created by JetBrains PhpStorm.
 * User: amits
 * Date: 7/25/13
 * Time: 3:18 PM
 * To change this template use File | Settings | File Templates.
 */
require_once '/var/www/html/springhill/app/Mage.php';
include_once '/var/www/html/springhill/apiclient/web_conf.php';
umask(0);
Mage::app('default');
$_topcategories = Mage::getModel('catalog/category') 
    ->getCollection()
    ->addAttributeToSelect('*')
    ->addIsActiveFilter();
foreach($_topcategories as $_tcategory){
    $category = Mage::getModel('catalog/category')->load($_tcategory->getId());
    $_productCollection = Mage::getModel('catalog/product')->getCollection()
        ->addAttributeToSelect('entity_id')
        ->addCategoryFilter($category)
        ->addAttributeToFilter('status', array('eq' => 1))
        ->addAttributeToFilter('default_category', array('eq' => 1))
        ->setVisibility(Mage::getSingleton('catalog/product_visibility')->getVisibleInCatalogIds())
        ->addAttributeToSort('last_tendays_sale_price', 'desc');
    Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($_productCollection);
    $totalReviewCount=0;
    $totalRating =0;
    $noOfProducts = 0;
    foreach ($_productCollection as $product){
        $storeId    = Mage::app()->getStore('default')->getId();
        $summaryData = Mage::getModel('review/review_summary')
            ->setStoreId($storeId)
            ->load($product->getId());
        $mainProductReview= $summaryData->getReviewsCount();
        $mainProductRating = $summaryData->getRatingSummary()/20;
        if($mainProductReview > 0){
            $totalReviewCount =$totalReviewCount+$mainProductReview;
            $totalRating = $totalRating+$mainProductRating;
            $noOfProducts++;
        }
    }

    /***Get reviews of subcategories products ***/
    $getSubCategories = $category->getAllChildren();
    $arrSubCategories = explode(',',$getSubCategories);
    foreach($arrSubCategories as $subcat){
        if($subcat!=$category->getId()){
            $objSubCategory =  Mage::getModel('catalog/category')->load($subcat);
            $_subproductCollection = Mage::getModel('catalog/product')->getCollection()
                ->addAttributeToSelect('entity_id')
                ->addCategoryFilter($objSubCategory)
                ->addAttributeToFilter('status', array('eq' => 1))
                ->addAttributeToFilter('default_category', array('eq' => 1))
                ->setVisibility(Mage::getSingleton('catalog/product_visibility')->getVisibleInCatalogIds())
                ->addAttributeToSort('last_tendays_sale_price', 'desc');
            Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($_subproductCollection);
            foreach ($_subproductCollection as $subproduct){
                $storeId    = Mage::app()->getStore('default')->getId();
                $subsummaryData = Mage::getModel('review/review_summary')
                    ->setStoreId($storeId)
                    ->load($subproduct->getId());
                $subProductReview = $subsummaryData->getReviewsCount();
                $subProductRating = $subsummaryData->getRatingSummary()/20;
                if($subProductReview > 0 ){
                    $totalReviewCount =$totalReviewCount+$subProductReview;
                    $totalRating = $totalRating+$subProductRating;
                    $noOfProducts++;
                }
            }
        }
    }
    $default_sort_by = $category->getDefaultSortBy();
    $avgRating = round($totalRating/$noOfProducts,2);
    if($default_sort_by){
        $result = $client->call($session_id, 'catalog_category.update', array($category->getId(), array(
            'default_sort_by' => $default_sort_by,
            'available_sort_by' => 'position,name,price,till_date_revenue',
            'total_review_count' => $totalReviewCount,
            'avg_rating' => $avgRating) ) );
    }else{
        $result = $client->call($session_id, 'catalog_category.update', array($category->getId(), array(
            'available_sort_by' => 'position,name,price,till_date_revenue',
            'default_sort_by' => null,
            'total_review_count' => $totalReviewCount,
            'avg_rating' => $avgRating ) ) );
    }

    echo  $category->getName().'===='.$totalReviewCount.'===='.$avgRating;
    echo "\n";
}
/***END Get reviews of subcategories products ***/
