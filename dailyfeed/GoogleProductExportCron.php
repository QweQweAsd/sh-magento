<?php

require_once '/var/www/html/springhill/app/Mage.php';
umask(0);
Mage::app('default');
global $global_server_path, $global_server_image_path;
$global_server_path = "http://www.springhillnursery.com/";
$global_server_image_path = $global_server_path."media/catalog/product"; ///B/l/Black_Eyed_Susan_Daylily.jpg";

$connection = Mage::getSingleton('core/resource')->getConnection('core_read');
$write = Mage::getSingleton('core/resource')->getConnection('core_write');
$limit = 100;
$start = 0;
if (!empty($argv[1]))
    $start = $argv[1];
else
    $start=0;
if (empty($start)) {
    $totalResults = $connection->select()
        ->from('catalog_product_entity as cpe', array('cpe.entity_id', 'cpe.sku'))
        ->join('catalog_product_entity_int as cpei', 'cpe.entity_id=cpei.entity_id', array())
        ->where("cpe.sku!=''")
        ->where("cpei.attribute_id=96")
        ->where("cpei.value=1")
        ->where("cpe.sku NOT LIKE '%_VSPMAAC%'")
        ->limit($limit);
} else {
    $totalResults = $connection->select()
        ->from('catalog_product_entity as cpe', array('cpe.entity_id', 'cpe.sku'))
        ->join('catalog_product_entity_int as cpei', 'cpe.entity_id=cpei.entity_id', array())
        ->where("cpe.sku!=''")
        ->where("cpei.attribute_id=96")
        ->where("cpei.value=1")
        ->where("cpe.sku NOT LIKE '%_VSPMAAC%'")
        ->limit($limit, $start);
}
//print $totalResults;exit;
$totalResultsSet = $connection->fetchAll($totalResults);
$i = 0;
$totalData = array();

//Get the Offer Code for the Keycode 0425238

$strOfferCode = $write->fetchOne("SELECT Offer_Code FROM keycodes WHERE keycode = '0425238'");

if (count($totalResultsSet) > 0) {
    foreach ($totalResultsSet as $key => $value) {
        try {
            //$product_id = Mage::getModel('catalog/product')->getIdBySku("$sku");
            if (!empty($value['entity_id']) && !strstr($value['sku'])) { // && is_numeric($value['sku'])) {

                $product_id = $value['entity_id'];
                $_Pdetails = Mage::getModel('catalog/product')->load($product_id);
				/*if($_Pdetails->getPrice()=="0.00")
                    continue;*/
		if(!$_Pdetails->isSaleable())
                    continue;
                if($_Pdetails->getDescription()=="")
                    continue;
                $categories = $_Pdetails->getCategoryIds();
                if(empty($categories))
                    continue;
                if($_Pdetails->getName()=="")
                    continue;
                //If Image is empty dont include in feed
                $prodImage= $_Pdetails->getImage();
                if($prodImage=="/" || $prodImage=="no_selection" || $prodImage=="")
                    continue;

                $categories_list = $_Pdetails->getCategoryIds();
				//Array Mapping for the Google product category
				$arrSunPerennials = array ('Sun Perennials','Anemones','Aster Plants','Carnation Plants','Columbine Plants','Coneflower Plants','Coreopsis Plants','Daisy Plants','Daylily Plants','Foxglove Plants','Gaillardia Plants','Geraniums','Hollyhocks','Iris','Lavender Plants','Lilies','Monarda Plants','Mum Plants','Other Sun','Perennials','Peony Plants','Phlox Plants','Poppy Plants','Scabiosa');
				$arrShadePerennials = array('Shade Perennials','Astilbe','Begonia','Bleeding Hearts','Ferns','Hellebore Plants','Heuchera (Coral Bells)','Heucherella','Hostas','Lily-of-the-Valley','Other Shade Perennials','Primrose');
				$arrAnnualsTropicals = array('Annuals & Tropicals','Annual Plants','Edibles','Flowering','Pansies','Succulents','Vegetables');
				$arrFloweringBulbs =array('Flowering Bulbs','Lilies','Other Hardy Bulbs','Tender Bulbs');
				$arrCollections =array('Collections');
				$arrDeerResistant =array('Deer Resistant');
				$arrDroughtTolerant =array('Drought Tolerant');
				$arrFruitBerries  =array('Fruit & Berries','Berries','Citrus Plants','Fruit Trees','Small Fruits',);
				$arrGardeningSupplies =array('Gardening Supplies');
				$arrGrabBags =array('Grab Bags');
				$arrGrasses=array('Grasses');
				$arrGroundCover =array('Ground Cover','Carpet Phlox','Other Ground Cover','Sedum');
				$arrRoses = array('Roses','Climbing Roses','Floribunda Roses','Grandiflora Roses','Ground Cover Roses','Knock Out Roses','Mini Roses','Shrub Roses');
				$arrShrubsHedges =array('Shrubs & Hedges','Azaleas','Butterfly Bush','Crape Myrtles','Evergreen Shrubs','Hibiscus','Hydrangea','Lilac','Mountain Laurel','Other Shrubs & Hedges','Rhododendron','Weigela');
				$arrTrees =array('Trees','Cherry Trees','Dogwood Trees','Flowering Trees','Japanese Maple','Magnolia Trees','Ornamental Trees','Shade Trees',
'Weeping Trees');
				$arrFloweringVines = array('Flowering Vines','Clematis Vines','Honeysuckle Vines','Other Vines','Trumpet Vines','Wisteria Vines');
				$productPriceInfo =$write->fetchAll("SELECT Sale_Price,Price from price WHERE Offer_Code='".$strOfferCode."' AND Product_Number='".trim($value['sku'])."' Order By Qty_Low asc LIMIT 1 ");
				if(empty($productPriceInfo[0]['Price'])){
                    $productDefaultPriceInfo =$write->fetchAll("SELECT Sale_Price,Price from price WHERE Offer_Code='4' AND Product_Number='".trim($value['sku'])."' Order By Qty_Low asc LIMIT 1 ");
                    //If Price is not found in Price table then we are not adding in the feed
                    if(empty($productDefaultPriceInfo[0]['Price']))
                        continue;
                    else{
                        $productPrice = $productDefaultPriceInfo[0]['Price'];
                        $productSalePrice = $productDefaultPriceInfo[0]['Sale_Price'];
                    }
                }else{
                    $productPrice = $productPriceInfo[0]['Price'];
                    $productSalePrice = $productPriceInfo[0]['Sale_Price'];
                }
                $ShippingPrice = $write->fetchOne("SELECT Shipping FROM shippingtables WHERE Offer_Code='".$strOfferCode."' ORDER BY Order_Low ASC LIMIT 1");
				$categoryName ='';
				$chkCategoryFlag = False;
				$levelOfCategory ='';
                $arrParentIds =array();
                foreach($categories_list as $catvalue) {
                    $catagory_model = Mage::getModel('catalog/category');
                    $categories = $catagory_model->load($catvalue); // where $id will be the known category id
                    //Only Active categories && $categories->getIncludeInSinglefeed()==1
					
                    if($categories->getIsActive()==1 && $categories->getIncludeInSinglefeed()==1){
							//Check for the parent category and sub level category
                            $retlevel = getCategoryLevel($catvalue,0);
                            //Check for same parent
                            $cparentId = $categories->getParentId();
                            //logic enter for first time else if level is greater than previous else parent id is not same
                            if(empty($levelOfCategory) || $retlevel > $levelOfCategory || !in_array($cparentId,$arrParentIds)){
                                //echo '$retlevel===>'.$retlevel;
                                $levelOfCategory = $retlevel;
                                $arrParentIds[] =$cparentId;
                                $categoryName = $categories->getName();
                                $chkCategoryFlag = True;
                            }
					}
				}

						if($chkCategoryFlag){

                            if(in_array($categoryName,$arrSunPerennials)){
                                $strGoogleProductCategory = 'Home & Garden > Plants > Flowers';
                            }elseif(in_array($categoryName,$arrShadePerennials)){
                                $strGoogleProductCategory = 'Home & Garden > Plants > Flowers';
                            }elseif(in_array($categoryName,$arrAnnualsTropicals)){
                                $strGoogleProductCategory = 'Home & Garden > Plants > Outdoor Plants';
                            }elseif(in_array($categoryName,$arrFloweringBulbs)){
                                $strGoogleProductCategory = 'Home & Garden > Plants > Flowers';
                            }elseif(in_array($categoryName,$arrCollections)){
                                $strGoogleProductCategory = 'Home & Garden > Plants > Outdoor Plants';
                            }elseif(in_array($categoryName,$arrDeerResistant)){
                                $strGoogleProductCategory = 'Home & Garden > Plants > Outdoor Plants';
                            }elseif(in_array($categoryName,$arrDroughtTolerant)){
                                $strGoogleProductCategory = 'Home & Garden > Plants > Outdoor Plants';
                            }elseif(in_array($categoryName,$arrFruitBerries)){
                                $strGoogleProductCategory = 'Home & Garden > Plants > Outdoor Plants';
                            }elseif(in_array($categoryName,$arrGardeningSupplies)){
                                $strGoogleProductCategory = 'Home & Garden > Lawn & Garden > Gardening';
                            }elseif(in_array($categoryName,$arrGrabBags)){
                                $strGoogleProductCategory = 'Home & Garden > Lawn & Garden > Gardening';
                            }elseif(in_array($categoryName,$arrGrasses)){
                                $strGoogleProductCategory = 'Home & Garden > Plants > Outdoor Plants';
                            }elseif(in_array($categoryName,$arrGroundCover)){
                                $strGoogleProductCategory = 'Home & Garden > Plants > Outdoor Plants';
                            }elseif(in_array($categoryName,$arrRoses)){
                                $strGoogleProductCategory = 'Home & Garden > Plants > Flowers';
                            }elseif(in_array($categoryName,$arrShrubsHedges)){
                                $strGoogleProductCategory = 'Home & Garden > Plants > Outdoor Plants';
                            }elseif(in_array($categoryName,$arrTrees)){
                                $strGoogleProductCategory = 'Home & Garden > Plants > Trees';
                            }elseif(in_array($categoryName,$arrFloweringVines)){
                                $strGoogleProductCategory = 'Home & Garden > Plants > Outdoor Plants';
                            }else{
                                $strGoogleProductCategory = 'Home & Garden > Lawn & Garden > Gardening';
                            }

							$totalData[$i]['id'] =trim($value['sku']);
							//Remove the trademark,registered snipets from Product Name
							$productName =str_replace(array('®', '™','’','‘'), array('', ''), $_Pdetails->getName());    
		                	$prodName=stripslashes(utf8_decode(str_replace("\t", " ", htmlspecialchars_decode($productName))));
		                    $title= $prodName ."  By Spring Hill Nurseries ".trim($value['sku']);
		                    $totalData[$i]['title'] = strip_tags($title);
		                    $desc = str_replace("\n", "", $_Pdetails->getDescription());
		                    $desc1 = str_replace("\t", "", $desc);
		                    $desc2 = str_replace("\n\t", "", $desc1);
		                    $desc3 = str_replace("\t\n", "", $desc2);
		                    $desc4 = str_replace("\r", "", $desc3);
		                    //$totalData[$i]['description'] = substr(utf8_decode(trim(stripslashes(strip_tags(htmlspecialchars_decode($desc4))))),0,900);
		                    $strDescription = substr(utf8_decode(trim(stripslashes(strip_tags(htmlspecialchars_decode($desc4))))),0,900);
		                    //$strDescription = str_replace("'"," feet",$strDescription);

		                    $totalData[$i]['description'] = str_replace(">","",$strDescription);
		                    //$totalData[$i]['stock_status'] = 'Y';
		                    $totalData[$i]['google_product_category'] =$strGoogleProductCategory;
		                    $totalData[$i]['product_type'] = $categoryName;
		                    $totalData[$i]['mpn'] =$value['sku'];
		                    //removed category from the URL
		                    $totalData[$i]['link'] =$GLOBALS["global_server_path"] .$_Pdetails->getUrlPath().'?sid=0425238&utm_medium=shopping_engine';
		                    $totalData[$i]['image_link'] =$GLOBALS["global_server_image_path"] . $_Pdetails->getImage();
		                    //$productPrice=$_Pdetails->getPrice();
		                    $totalData[$i]['price'] =number_format($productPrice, 2, '.', '');
		                    $totalData[$i]['shipping'] =$ShippingPrice;
		                    $totalData[$i]['tax'] = "";
		                    $totalData[$i]['shipping_weight'] ="";
		                    $totalData[$i]['gender'] ="";
		                    $totalData[$i]['age_group'] ="";
		                    $totalData[$i]['color'] =strip_tags($_Pdetails->getWebSearchFlowerColor());
		                    $totalData[$i]['size'] = stripslashes(utf8_decode($_Pdetails->getSpacing()));
		                    $totalData[$i]['availability'] ="Y";
		                    $totalData[$i]['brand'] = "SH";
		                    $totalData[$i]['online_only'] = "";
		                    $totalData[$i]['additional_image_link'] ="";
							$totalData[$i]['adwords_redirect'] ="";
		                    $totalData[$i]['adwords_grouping'] ="";
		                    $totalData[$i]['adwords_labels'] ="1kQVCM-QQBCT757-Aw";
		                    //$totalData[$i]['adwords_publish'] ="true";
							$totalData[$i]['condition'] ="New";
		                    $totalData[$i]['format'] ="";
		                    $totalData[$i]['item_group_id'] ="";
		                    $totalData[$i]['material'] ="";
		                    $totalData[$i]['pattern'] = "";
		                    $totalData[$i]['excluded_destination'] ="";
		                    $totalData[$i]['payment_accepted'] ="";
		                    $totalData[$i]['payment_notes'] ="";
		                    $totalData[$i]['sale_price_effective_date'] ="";
		                    $totalData[$i]['sale_price'] =$productSalePrice;
		                    $totalData[$i]['product_type_old'] ="";
		                    $i++;
						}
						/*if($i==20)
							break;*/
            }
        } catch (Exception $e) {
            $log_array[0]['Message'] = $e->getMessage();
            $log_array[0]['Trace'] = $e->getTraceAsString();
            $log_array[0]['Info'] = "Something went wrong while exporting data form magento.";
        }

    }

    try {
        $myFile = "/var/www/html/springhill/dailyfeed/singlefeed/SingleFeed--SH.txt";
        if (empty($start)) {
            unlink($myFile);
        }
        // Let's make sure the file exists and is writable first.
       
        $fh = fopen($myFile, 'a+') or die("can't open file");
		chmod($myFile,0777);
        if (empty($start)) {
			
            $heading = "id	title	description	google_product_category	product_type	mpn	link	image_link	price	shipping	tax	shipping_weight	gender	age_group	color	size	availability	brand	online_only	additional_image_link	adwords_redirect	adwords_grouping	adwords_labels	condition	format	item_group_id	material	pattern	excluded_destination	payment_accepted	payment_notes	sale_price_effective_date	sale_price	product_type_old\n";
            fwrite($fh, $heading);
        }
        foreach ($totalData as $id => $info) {
            $stringData = implode("\t", $info) . "\n";
            fwrite($fh, $stringData);
        }
        fclose($fh);
       /* }else{
	 echo 'no permissions';
	}*/
    } catch (Exception $e) {
        $log_array[0]['Message'] = $e->getMessage();
        $log_array[0]['Trace'] = $e->getTraceAsString();
        $log_array[0]['Info'] = "Something went wrong while exporting data form magento.";
    }
    $start = $start + $limit;
    exec('php -f /var/www/html/springhill/dailyfeed/GoogleProductExportCron.php ' . $start . ' ' . $limit);
} else {

}

function getCategoryLevel($categoryId,$level){
    $catagory_model = Mage::getModel('catalog/category');
    $categories = $catagory_model->load($categoryId);
    $parentId =$categories->getParentId();
    if($parentId ==2 || $parentId==247){
        return $level;
    }else{
        $level++;
        return getCategoryLevel($parentId,$level);
    }
}

?>
